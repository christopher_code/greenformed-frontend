import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import MainBar from "../components/BuildingBlocks/templateMainBar";
import ProfileData from "../components/pageSpecific/User/CRUD/Profile/profileData";

class UserProfile extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>
                <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                    >
                    <div className="topBarFlex">
                        <div>
                            <h1 className="topBarHeadline">Profile Page</h1>
                            <h3 className="topBarSubheadline">See all public information of that profile on the Greenformed platform.</h3>
                        </div>
                    </div>
                </MainBar>

                <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                >
                    <ProfileData/>
                </MainBar>

            </Layout>
        )
    }
}

export default UserProfile;