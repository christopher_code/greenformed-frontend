import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import MainBar from "../components/BuildingBlocks/templateMainBar";
import LoginForm from "../components/pageSpecific/User/CRUD/Login/loginForm";

class NewProduct extends Component {
    
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Login to the Greenformed - the plattform for green house gas data</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                    <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                    >
                        <div className="topBarFlex">
                            <div>
                                <h1 className="topBarHeadline">Login to Greenformed to get the full experience</h1>
                                <h3 className="topBarSubheadline">As a user of the Greenfomed platform you have full rights for adding data, approving data, and thereby contributing to create a global database of Greenhouse Gas data. Feel free to create an account.</h3>
                            </div>
                        </div>
                    </MainBar>

                    <MainBar
                        marginTop={'0'}
                        marginBottom={'20px'}
                    >
                         Login Page
                        <LoginForm></LoginForm>
                    </MainBar>

            </Layout>
        )
    }
}

export default NewProduct;
