import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import FullProduct from "../components/pageSpecific/Product/CRUD/GET/fullProduct";
import { urlBE } from "../App";

class Product extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed Product Page</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                <FullProduct
                    url={urlBE + '/api/o/product/get/full/'}
                    type={'product'}
                ></FullProduct>

            </Layout>
        )
    }
}

export default Product;