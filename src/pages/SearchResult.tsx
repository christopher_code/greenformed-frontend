import  { Component } from 'react';
import { Helmet } from 'react-helmet';
import Layout from "../components/HOC/Layout";
import SearchCard from "../components/pageSpecific/Product/CRUD/GET/fetchSearchResult";

class SearchResult extends Component {
    render() {
        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed - Search Result</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                <SearchCard/>

            </Layout>
        )
    }
}

export default SearchResult;