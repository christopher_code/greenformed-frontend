import  { Component } from 'react';
import { Helmet } from 'react-helmet';
import Layout from "../components/HOC/Layout";
import MainBar from "../components/BuildingBlocks/templateMainBar";
import Button from "../components/BuildingBlocks/templateButton";
import ProductCards from "../components/pageSpecific/Product/CRUD/GET/fetchProductCards";
import Upper from "../components/pageSpecific/Home/UI/upperBar";
import goTo from "../helper/goTo";
import { urlBE } from "../App";

class Index extends Component {
    render() {
        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed - Home Page with fully approved products</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                    <Upper/>

                    <ProductCards
                        url={urlBE + '/api/o/product/get/random'}
                    ></ProductCards>

                    <MainBar
                        marginTop={'0'}
                        marginBottom={'0'}
                    >
                        <div className="bottomBarFlex">
                            <h3 className="bottomBarHeadline">Do you have data about Greenhouse Gases?</h3>
                            <Button 
                                event={() => goTo('/new/product')}
                                bg={"#A3A9C3"}
                                border={'#A3A9C3'}
                                width={"215px"}
                                color={"#36354A"}
                                margin={"20px 0 0 0"}
                            >
                                Add Data
                            </Button>
                        </div>
                    </MainBar>

            </Layout>
        )
    }
}

export default Index;