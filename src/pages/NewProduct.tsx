import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import NewProductFull from "../components/pageSpecific/Product/UI/FullComponents/newProduct";

class NewProduct extends Component {
    
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed Add Product With Green House Gas Data</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>
                
                <NewProductFull></NewProductFull>
                    
            </Layout>
        )
    }
}

export default NewProduct;