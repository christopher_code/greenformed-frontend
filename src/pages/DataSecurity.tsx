import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";

class DataSecurity extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Data Protection Agreement of Greenformed</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                <p>The data protection agreement of Greenformed is created here.</p>
               
            </Layout>
        )
    }
}

export default DataSecurity; 