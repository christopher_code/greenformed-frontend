import  { Component } from 'react';
import { Helmet } from 'react-helmet';
import Layout from "../components/HOC/Layout";
import ApprovalCards from "../components/pageSpecific/Product/CRUD/GET/fetchApprovalCards";

class Index extends Component {
    render() {
        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed - Home Page with fully approved products</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                    <ApprovalCards
                        url={'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/o/product/fetch/random'}
                    ></ApprovalCards>

            </Layout>
        )
    }
}

export default Index;