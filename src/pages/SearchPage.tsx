import  { Component } from 'react';
import { Helmet } from 'react-helmet';
import Layout from "../components/HOC/Layout";
import SetSearch from "../components/pageSpecific/Product/CRUD/ADD/setSearch";

class SearchPage extends Component {
    render() {
        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed - Search the Greenformed Databse</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                <SetSearch/>

            </Layout>
        )
    }
}

export default SearchPage;