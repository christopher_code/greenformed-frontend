import  { Component } from 'react';
import { Helmet } from 'react-helmet';

import Layout from "../components/HOC/Layout";
import MainBar from "../components/BuildingBlocks/templateMainBar";
import Search from "../components/BuildingBlocks/tempalteSearchBar";
import ApprovedFullProduct from "../components/pageSpecific/Product/CRUD/GET/fullAprovedProduct";

class Product extends Component {
    render() {

        return (
            <Layout>
                <Helmet>
                     <meta charSet="utf-8" />
                     <title>Greenformed Approved Product Page</title>
                     <meta 
                         name="description"
                         content="The public Green House Gas Database managed by the public. From the public, for the public." />
                </Helmet>

                <ApprovedFullProduct
                    url={'https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/o/product/get/'}
                    type={'approvedProduct'}
                ></ApprovedFullProduct>

                <MainBar
                    marginTop={'20px'}
                    marginBottom={'0'}
                >
                    <div className="bottomBarFlex">
                        <h3 className="bottomBarHeadlineProductPage">Want to get more information about the GHG Emissions of a product or service?</h3>
                        <Search 
                            event={console.log("test")} 
                            bg={"white"}
                            border={"#1E3D0E"}
                            icon={"red"}
                            placeholder={"Search"}
                            fontColor={"#1E3D0E"}
                            borderColor={"#1E3D0E"}
                            width="300px"
                            changeEvent={() => console.log("test")}
                        >
                        Search
                        </Search>
                    </div>
                </MainBar>

            </Layout>
        )
    }
}

export default Product;