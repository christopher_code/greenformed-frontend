import puppeteer from "puppeteer";

describe("Home Container offline", () => {
  let browser: any;
  let page : any;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  it("expect error timeout", async () => {
    await page.goto("http://localhost:3000/3/3/3/3/3/3/3");

    await page.waitForSelector(".topBarHeadline");

    const text = await page.$eval(".topBarHeadline", (e: any) => e.textContent);
    expect(text).toContain("Login to Greenformed to get the full experience");
  });

  afterAll(() => browser.close());
});