import puppeteer from "puppeteer";

describe("Home Container", () => {
  let browser: any;
  let page : any;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  it("contains the welcome text", async () => {
    await page.goto("http://localhost:3000");
    await page.waitForSelector(".textDivNavHeadline");
    const text = await page.$eval(".textDivNavHeadline", (e: any) => e.textContent);
    expect(text).toContain("Greenformed");
  });

  afterAll(() => browser.close());
});