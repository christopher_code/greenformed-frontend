import puppeteer from "puppeteer";

describe("Search Container", () => {
  let browser: any;
  let page : any;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  it("contains the welcome text", async () => {
    await page.goto("http://localhost:3000/search");
    await page.waitForSelector("#inputSearchBar");
    await page.click("#inputSearchBar");
    await page.type("#inputSearchBar", "12341234");

    await page.waitForSelector(".buttonTemplate");

    const text = await page.$eval(
        ".buttonTemplate",
        (e: any) => e.textContent
      );

    expect(text).toContain("Add Product");
  });

  afterAll(() => browser.close());
});