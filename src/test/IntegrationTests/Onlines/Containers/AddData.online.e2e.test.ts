import puppeteer from "puppeteer";

describe("Add Data Container", () => {
  let browser: any;
  let page : any;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  it("Test if add data is a private route", async () => {
    await page.goto("http://localhost:3000");
    await page.waitForSelector(".textDivNavHeadline");

    await page.goto("http://localhost:3000/new/product");
    await page.waitForSelector(".containerSignUp");

    const text = await page.$eval(".topBarHeadline", (e: any) => e.textContent);
    expect(text).toContain("Login to Greenformed to get the full experience");
  });

  afterAll(() => browser.close());
});