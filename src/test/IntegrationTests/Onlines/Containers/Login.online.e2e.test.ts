import puppeteer from "puppeteer";

describe('Login Container', () => {
  let browser: any;
  let page: any;

  beforeEach(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  afterEach(() => browser.close());

  it("Shows error messafe after failed login", async () => {
    await page.goto("http://localhost:3000/user/login");
    await page.waitForSelector(".containerSignUp");

    await page.click("#loginEmail");
    await page.type("#loginEmail", "beste@frau.de");

    await page.click("#loginPassword");
    await page.type("#loginPassword", "passworrrr");

    await page.click(".formButtonGeneral");

    await page.waitForSelector(".messageComponentText");

    const text = await page.$eval(
      ".messageComponentText",
      (e: any) => e.textContent
    );
    expect(text).toContain("401 - Please provide the correct email and password");
  });

  it("Successfull login", async () => {
    await page.goto("http://localhost:3000/");
    await page.goto("http://localhost:3000/user/login");
    await page.waitForSelector(".containerSignUp");

    await page.click("#loginEmail");
    await page.type("#loginEmail", "beste@frau.de");

    await page.click("#loginPassword");
    await page.type("#loginPassword", "password");

    await page.click(".formButtonGeneral");


    expect(location.href).not.toContain('/user');
  });

  afterAll(() => browser.close());
  
});