import puppeteer from "puppeteer";

describe("Home Container offline", () => {
  let browser: any;
  let page : any;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  it("expect error timeout", async () => {
    await page.goto("http://localhost:3000/user/login");
    await expect(page).rejects
  });

  afterAll(() => browser.close());
});