import puppeteer from "puppeteer";

describe("Home Container offline", () => {
  let browser: any;
  let page : any;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  it("expect error timeout", async () => {
    await page.goto("http://localhost:3000/approved/data");
    await expect(page).rejects;

    await page.waitForSelector(".messageComponentText");

    const text = await page.$eval(".messageComponentText", (e: any) => e.textContent);
    expect(text).toContain("Something went wrong - please also check your connection");
  });

  afterAll(() => browser.close());
});