import { shallow } from 'enzyme'
import App from '../../App'

describe('Test if <App /> renders', () => {
  let component

  beforeEach(() => {
    component = shallow(<App />)
  })
  test('It should mount', () => {
    expect(component.length).toBe(1)
  })
})