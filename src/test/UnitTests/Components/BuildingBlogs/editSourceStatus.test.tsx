import { shallow } from 'enzyme'
import EditSourceStatus from "../../../../components/BuildingBlocks/templateSourceEdit";

it('should render the editSourceStatus template: finalApproved', () => {
  const wrapper = shallow
    (<EditSourceStatus
        eventSource={()=> console.log("test")}
        eventEdit={()=> console.log("test 1")}
        status={'approvedFinal'}
    />)
  const text = wrapper.find('.sourceStatusApproved')
  const result = text.text()

  expect(result).toBe('Source Approved')
  expect(result).not.toBe('')
})

it('should render the editSourceStatus template: notFinalApproved', () => {
    const wrapper = shallow
      (<EditSourceStatus
          eventSource={()=> console.log("test")}
          eventEdit={()=> console.log("test 1")}
          status={'other'}
      />)
    const text = wrapper.find('.sourceStatusApproved')
    const result = text.text()
  
    expect(result).toBe('Not fully approved yet')
    expect(result).not.toBe('')
  })