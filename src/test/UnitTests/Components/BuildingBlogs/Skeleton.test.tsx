import { shallow } from 'enzyme'
import Skeleton from "../../../../components/BuildingBlocks/templateSkeleton";

describe('Skeleton Snapshot Test', () => {
    it('should render our Snapshots correctly', () => {
      const wrapper = shallow(
        <Skeleton numberOfLines={1} />)
      expect(wrapper).toMatchSnapshot()
    })
})
