import { shallow } from 'enzyme'
import Button from "../../../../components/BuildingBlocks/templateButton";

it('should render the button template', () => {
  const wrapper = shallow
    (<Button 
        event={()=> console.log('test')}
        bg={'white'}
        children={'any'}
        border={'transparent'}
        width={'120px'}
        color={'black'}
        margin={'0'}
    />)
  const text = wrapper.find('Button')
  const result = text.text()

  expect(result).toBe('any')
  expect(result).not.toBe('')
})

describe('Button Snapshot Test', () => {
    it('should render our Snapshots correctly', () => {
      const wrapper = shallow(
        <Button 
        event={()=> console.log('test')}
        bg={'white'}
        children={'any'}
        border={'transparent'}
        width={'120px'}
        color={'black'}
        margin={'0'}
    />)
      expect(wrapper).toMatchSnapshot()
    })
})

test('Testing clicking the button', () => {
  const wrapper = shallow(
    <Button 
        event={()=> console.log('test')}
        bg={'white'}
        children={'any'}
        border={'transparent'}
        width={'120px'}
        color={'black'}
        margin={'0'}
    />)
  const btn = wrapper.find('.buttonTemplate')
  btn.simulate('click')

  const output: string = wrapper.find('.buttonTemplate').text()
})
