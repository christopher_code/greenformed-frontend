import convertDate from "../../../helper/convertDate";

const date1 = 1234567890;

const date2 = 1234;

describe('Check if helper function for converting dates work', () => {

    test('Expect converteDate function to output array of strings', () => {
        let date = convertDate(date1);
        expect(typeof date).toBe("object");
        expect(typeof date[0]).toBe("string");
        expect(typeof date[1]).toBe("string");
    });

    test('Expect two dates not to equal', () => {
        let date = convertDate(date1);
        let dateSame = convertDate(date1)
        let dateCompare = convertDate(date2);
        expect(date[0]).not.toEqual(dateCompare[0]);
        expect(date[0]).toEqual(dateSame[0]);
    });
});


