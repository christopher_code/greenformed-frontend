import React, { Suspense } from 'react';
import './App.scss';
import { Route, Switch, withRouter, Router, Redirect } from 'react-router-dom';
import history from './helper/history';
import reducer from './store/reducer/auth';

import Home from "./pages/Home";
import ApprovedData from "./pages/ApprovedData";
import ProductPage from "./pages/Product";
import ApprovedProductPage from "./pages/ApprovedProduct";
import UpdateProduct from "./pages/UpdateProduct";
import UpdateSeller from "./pages/UpdateSeller";
import UpdateSingle from "./pages/UpdateSingle";
import UpdateReason from "./pages/UpdateReason";
import DeleteTotal from "./pages/DeleteTotalGHG";
import Source from "./pages/Source";
import SourceApproval from './pages/SourceApproval';
import NewProductPage from "./pages/NewProduct";
import AddToExistingProduct from "./pages/AddToExistingProduct";
import Login from "./pages/Login";
import SignUp from "./pages/SignUp";
import Profile from "./pages/UserProfile";
import GetApprovalData from "./pages/ApprovalStart";
import ChangeHistory from "./pages/ChangeHistory";
import SearchPage from "./pages/SearchPage";
import SearchResult from "./pages/SearchResult";
import Imprint from "./pages/Imprint";
import LegalNotice from "./pages/LegalNotice";
import DataSecurity from './pages/DataSecurity';

export const urlBE: string = 'https://greenformedapprovalbe-jnzm36w3ca-ew.a.run.app';

const initialState = {
  isAuthenticated: false,
  token: null,
  id: null
};

export const AuthContext = React.createContext<any>(initialState);

function App() {
  let isAuthenticated: any = localStorage.getItem('isAuthenticated');

  const [state, dispatch] = React.useReducer(reducer, initialState);

  let routes = (
    <Switch>
      <Route path="/" exact component={Home}/>
      <Route path="/approved/data" exact component={ApprovedData}/>
      <Route path="/product/:name" exact component={ProductPage}/>
      <Route path="/approved/product/:name" exact component={ApprovedProductPage}/>
      <Route path="/source/:id/:type/:name" exact component={Source}/>
      <Route path="/user/login" exact component={Login}/>
      <Route path="/user/signup" exact component={SignUp}/>
      <Route path="/o/user/profile/:id" exact component={Profile}/>
      <Route path="/user/profile/" exact component={Login}/>
      <Route path="/change/history/:type/:id" exact component={ChangeHistory}/>
      <Route path="/search/:name/:approved" exact component={SearchResult}/>
      <Route path="/search" exact component={SearchPage}/>
      <Route path="/imprint" exact component={Imprint}/>
      <Route path="/legal/notice" exact component={LegalNotice}/>
      <Route path="/data/security" exact component={DataSecurity}/>
      <Route path="*" exact component={Login}/>
      <Suspense fallback={<div>Loading...</div>}>
        <Redirect to="/user/login"/>
      </Suspense>
    </Switch>
  );

  if (isAuthenticated) {
    routes = (
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/approved/data" exact component={ApprovedData}/>
        <Route path="/approved/product/:name" exact component={ApprovedProductPage}/>
        <Route path="/search" exact component={SearchPage}/>
        <Route path="/imprint" exact component={Imprint}/>
        <Route path="/legal/notice" exact component={LegalNotice}/>
        <Route path="/user/signup" exact component={Home}/>
        <Route path="/user/login" exact component={Home}/>
        <Route path="/data/security" exact component={DataSecurity}/>
        <Route path="/o/user/profile/:id" exact component={Home}/>
        <Suspense fallback={<div>Loading...</div>}>
          <Route path="/product/:name" exact component={ProductPage}/>
          <Route path="/edit/product/:id" exact component={UpdateProduct}/>
          <Route path="/edit/seller/:id" exact component={UpdateSeller}/>
          <Route path="/edit/ghg/single/:id" exact component={UpdateSingle}/>
          <Route path="/edit/ghg/reason/:id" exact component={UpdateReason}/>
          <Route path="/edit/ghg/total/:id" exact component={DeleteTotal}/>
          <Route path="/source/:id/:type/:name" exact component={Source}/>
          <Route path="/new/product" exact component={NewProductPage}/>
          <Route path="/add/:type/to/product/:productId" exact component={AddToExistingProduct}/>
          <Route path="/user/profile/:id" exact component={Profile}/>
          <Route path="/verify/source/:id/:product/:type/:index/:typeSource/:modelId" exact component={SourceApproval}/>
          <Route path="/approval/init/data" exact component={GetApprovalData}/>
          <Route path="/change/history/:type/:id" exact component={ChangeHistory}/>
          <Route path="/search/:name/:approved" exact component={SearchResult}/>
        </Suspense>
      </Switch>
    );
  }

  return (
    <div className="App">
    <AuthContext.Provider
        value={{
          state,
          dispatch
        }}
      >
          <Router history={history}>
              <Suspense fallback={<p>Loading...</p>}>{routes}</Suspense>
          </Router>
        </AuthContext.Provider>
    </div>
  );
}

export default withRouter(App);