import Navigation from "../pageSpecific/Navigation/UI/navbar";
import Footer from "../pageSpecific/Navigation/UI/footer";
import Sidebar from "../BuildingBlocks/templateSideBar";

const Layout = (props: any) => {

  return (
    <>
      <section className="sectionNavigation">
        <Navigation></Navigation>
      </section>

      <main className="Content">
        <div className="mainContent">
          <Sidebar></Sidebar>
          <div className="mainbars">
            {props.children}
          </div>
        </div>
      </main>

      <section className="sectionFooter">
          <Footer></Footer>
      </section>
    </>
  );
};

export default Layout;