import React, {useState, useEffect} from 'react';
import Search from "../../../BuildingBlocks/tempalteSearchBar";
import { Navbar, Nav } from 'react-bootstrap';
import logo from "../../../../assets/LogoMock.png";
import user from "../../../../assets/user.png";
import { AuthContext, urlBE } from "../../../../App";
import goTo from "../../../../helper/goTo";

const Navigation: React.FC = () => {
    const { dispatch } = React.useContext(AuthContext);
    const [search, setSearch] = useState<string>('');
    const [userId, setUserId] = useState<string>('');
    const [token, setToken] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        let dataT: any = await localStorage.getItem('token');
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
        }

    let isAuthenticated: any = localStorage.getItem('isAuthenticated');

    const addTokenToBlacklist = (e: any) => {
        e.preventDefault();

        fetch(`${urlBE}/api/blacklist/add/${token}`, {
                method: "POST",
                headers: {
                    "X-Content-Type-Options": "nosniff"
                },
            }).then(response => {
                console.log("response")
                    if (response.ok) {
                        dispatch({type: "LOGOUT"})
                        return response.json();
                    }
                    throw response;
                })
              .catch(error => {
                  console.log(error)
              }
            );
      }

    return (
    <header className="navContainer">
    <div className="navBarDiv">
        <Navbar bg="transparent" expand="lg" className="navbar">
            <Navbar.Brand href="/">
                <div className="navLogoDiv">
                    <img src={logo} alt="GreenFormed Logo" className="logoNav" />
                </div>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse className="navbar-justify" id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link className="navLink" onClick={()=> goTo('/')}>Start</Nav.Link>
                    <Nav.Link className="navLink" onClick={()=> goTo('/approved/data')}>Approved Data</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/new/product')} id="addData">Add Data</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/approval/init/data')}>Verify Data</Nav.Link>
                    <Nav.Link onClick={()=> goTo('https://documenter.getpostman.com/view/8069940/TzzBpFQp')}>API Documentation</Nav.Link>
                    <Nav.Link onClick={()=> goTo('/search')} className="searchNavLink">Search</Nav.Link>
                </Nav>
                <div className="navbarAcccountDiv">
                    {!isAuthenticated ? 
                        <Nav className="navbar-account">
                            <img src={user} alt="Greenformed User Account" className="accountImg" onClick={() => goTo('/user/profile/unknown')} />
                            <Nav.Link onClick={()=> goTo('/user/signup')}>Sign-Up</Nav.Link>
                            <p className="deviderLine">|</p>
                            <Nav.Link eventKey={2} onClick={()=> goTo('/user/login')}>
                                Log-In
                            </Nav.Link>
                        </Nav>
                        :   
                        <Nav className="navbar-account">
                            <img src={user} alt="Greenformed User Account" className="accountImg" onClick={() => goTo('/user/profile/' + userId)} />
                            <Nav.Link onClick={addTokenToBlacklist}>Logout</Nav.Link>
                        </Nav>
                    }
                </div>
            </Navbar.Collapse>
        </Navbar>
        </div>
        
        <div className="navBottom">
            <div className="textDivNav" onClick={()=> goTo('/')}>
                <h2 className="textDivNavHeadline">Greenformed</h2>
                <h3 className="textDivNavSubHead">The global Greenhouse Gas database</h3>
            </div>
            <div className="searchbarNav">
            <Search 
                event={() => goTo('/search/' + search + "/notapproved")} 
                bg={"#0B1B04"}
                border={"#1E3D0E"}
                icon={"red"}
                placeholder={"Search"}
                fontColor={"white"}
                borderColor={"#1E3D0E"}
                width="300px"
                changeEvent={(e: any) => setSearch(e.target.value)}
            >
            Search
            </Search>
            </div>
        </div>
    </header>
    );
};

export default Navigation;

