import React, {useState} from 'react';
import Search from "../../../BuildingBlocks/tempalteSearchBar";
import { Navbar, Nav } from 'react-bootstrap';
import logo from "../../../../assets/LogoMock.png";
import user from "../../../../assets/user.png";
import goTo from "../../../../helper/goTo";

const Footer: React.FC = () => {
    const [search, setSearch] = useState<string>('');
    return (
    <footer className="footerDiv">
            <Navbar bg="transparent" expand="lg" className="navbar">
                <Navbar.Brand href="/" className='footerBrand'>
                    <div className="footerLogoDiv">
                        <img src={logo} alt="GreenFormed Logo" className="logoFooter" />
                    </div>
                    <h2 className="footerHeadline">Greenformed</h2>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse className="navbar-justify" id="basic-navbar-nav">
                    <div className="searchbarFooter">
                        <Search 
                            event={() => goTo('/search/' + search + "/notapproved")} 
                            bg={"#0B1B04"}
                            border={"#1E3D0E"}
                            icon={"red"}
                            placeholder={"Search"}
                            fontColor={"white"}
                            borderColor={"#1E3D0E"}
                            width="300px"
                            changeEvent={(e: any) => setSearch(e.target.value)}
                        >
                        Search
                        </Search>
                    </div>
                    <Nav className="navbar-account">
                        <img src={user} alt="Greenformed User Account" className="accountImg" onClick={()=> goTo('/user/profile')}/>
                        <Nav.Link onClick={()=> goTo('/user/signup')}>Sign Up</Nav.Link>
                        <p className="deviderLine">|</p>
                        <Nav.Link eventKey={2} onClick={()=> goTo('/user/login')}>
                            Log In
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        
        <div className="footerBottom">
            <div className="navFooterDivText">
                <div className="navBottom">
                    <div className="textDivNavFooter" onClick={()=> goTo('/')}>
                        <h2 className="textDivNavHeadline">Greenformed</h2>
                        <h3 className="textDivNavSubHead">The global Greenhouse Gas database</h3>
                        <br></br>
                        <p className="footerParagraph">Made by the community, for the community.</p>
                    </div>
                </div>
            </div>
            <div className="footerLeft">
                <div className="listOfPages">
                    <Nav.Link onClick={()=> goTo('/')} className="footerLink">
                        Start
                    </Nav.Link>
                    <Nav.Link onClick={()=> goTo('/new/product')} className="footerLink" id="addDataFooter">
                        Add Data
                    </Nav.Link>
                    <Nav.Link onClick={()=> goTo('/approval/init/data')} className="footerLink">
                        Verify Data
                    </Nav.Link>
                    <Nav.Link href="https://documenter.getpostman.com/view/8069940/TzzBpFQp" className="footerLink">
                        API Documentation
                    </Nav.Link>
                    <Nav.Link onClick={()=> goTo('/search')} className="footerLink">
                        Search
                    </Nav.Link>
                </div>
                <div className="listOfPages">
                    <Nav.Link onClick={()=> goTo('/imprint')} className="footerLink">
                        Imprint
                    </Nav.Link>
                    <Nav.Link onClick={()=> goTo('/legal/notice')} className="footerLink">
                        Legal Notive
                    </Nav.Link>
                    <Nav.Link onClick={()=> goTo('/data/security')} className="footerLink">
                        Data Security Agreement 
                    </Nav.Link>
                </div>
                </div>
        </div>
        <div className="footerLeiste">
            <h4 className="footerLeisteText">Copyright 2021 greenformed.com | All rights reserved</h4>
        </div>
    </footer>
    );
};

export default Footer;

