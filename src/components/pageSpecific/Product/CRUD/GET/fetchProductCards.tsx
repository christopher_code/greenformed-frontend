import { useState, useEffect } from 'react';
import ProductCard from "../../UI/FullComponents/productCard";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import goTo from "../../../../../helper/goTo";

type ProductCardProps = {
    url: string
}

const ProductCardsFetched = (props: ProductCardProps) => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []); 

    const loadData = async () => {
        fetch(props.url, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }   

    const cards = !fError && fData && fData.data.map((item: any) => 
        <ProductCard
            key={item.product.dataValues.id}
            date={item.product.dataValues.createdAt}
            productId={item.product.dataValues.id}
            productName={item.product.dataValues.name}
            status={item.product.dataValues.approvalStatus}
            totalGHG={item.productTotalEmission[0] ? item.productTotalEmission[0].ghgPerKG : 0}
            ghgSingle1={item.productSingleEmission[0] ? item.productSingleEmission[0].ghgName : "No data yet"}
            ghgSingle2={item.productSingleEmission[1] ? item.productSingleEmission[1].ghgName : "No data yet"}
            ghgSingle3={item.productSingleEmission[2] ? item.productSingleEmission[2].ghgName : "No data yet"}
            ghgSingle4={item.productSingleEmission[3] ? item.productSingleEmission[3].ghgName : "No data yet"}
            ghgReason1={item.productEmissionReason[0] ? item.productEmissionReason[0].ghgName : "No data yet"}
            ghgReason2={item.productEmissionReason[1] ? item.productEmissionReason[1].ghgName : "No data yet"}
            ghgReason3={item.productEmissionReason[2] ? item.productEmissionReason[2].ghgName : "No data yet"}
            category={item.productCategory[0] ? item.productCategory[0].typeOfCategory : "Not provided"}
            description={item.product.dataValues.description}
            originallyAddedBy={item.addedBy[0] ? item.addedBy[0].name : ''}
            getMoreEvent={() => goTo("/product/" + item.product.dataValues.id)}
            editInfoEvent={() => goTo("/edit/product/" + item.product.dataValues.id)}
        ></ProductCard>)

    return (
        <> { !fError && cards && fData ? <div>{cards}</div> 
            : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>
            : <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            } 
        </>
    )
};

export default ProductCardsFetched;
