import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import ProductMainInfo from "../../UI/FullComponents/productMainInfo";
import ProductGHGData from '../../UI/FullComponents/productGHGData';
import ProductAlternatives from "../../UI/FullComponents/productAlternatives";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import goTo from "../../../../../helper/goTo";

type FullProductProds = {
    url: string,
    type: string,
}

const FullProduct = (props: FullProductProds) => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(props.url + id.name, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          })
          .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    function handleGoWindow(link: string) {
        if (link.includes('https://') || link.includes('http://')) {
            window.open(link)
        } else {
            window.open('https://' + link)
        }
    }

    const cards = fData && !fError ? 
        <>
            <ProductMainInfo
                productId={fData.data.product.id}
                productName={fData.data.product.name ? fData.data.product.name : "no name set for that Id"}
                productCode={fData.data.product.id ? fData.data.product.id : "No Id found"}
                seller={fData.data.seller.length > 0 ? fData.data.seller[0].name : "Not provided"}
                sellerId={fData.data.seller.length > 0 ? fData.data.seller[0].id : "Not provided"}
                totalGHG={fData.data.totalEmission.length > 0 ? fData.data.totalEmission[0].ghgPerKG : "Not provided"}
                category={fData.data.catgeory[0] ? fData.data.catgeory[0].typeOfCategory : "Not provided"}
                description={fData.data.product.description}
                eventBuy={() => handleGoWindow(fData.data.product.linkToBuy)}
                eventEditProduct={() => goTo('/edit/product/' + fData.data.product.id)}
                eventSource={() => goTo('/source/' + fData.data.source[0].id + '/' + fData.data.product.id)}
                eventEditSource={() => goTo('/verify/source/' + fData.data.source[0].id + '/' + fData.data.product.id + '/product/0/new/1')}
                needsSourceEdit={true}
                status={fData.data.product.approvalStatus}
                sellerSourceHostoryEvent={fData.data.seller[0] ? () => goTo('/source/' + fData.data.seller[0].sourceId + '/' + fData.data.product.id) : console.log('no')}
                sellerSourceApprove={fData.data.seller[0] ? () => goTo('/verify/source/' + fData.data.seller[0].sourceId + '/' + fData.data.product.id + '/seller/0/new/' + fData.data.seller[0].id) : console.log("test")}
                sellerSourceStatus={fData.data.seller[0] ? fData.data.seller[0].approvalStatus : 'not deifined'}
                eventEditSeller={fData.data.seller.length !== 0 ? () => goTo('/edit/seller/' + fData.data.product.id) : () => goTo('/add/seller/to/product/' + fData.data.product.id)}
                eventAddSeller={fData.data.seller.length === 0 ? () => goTo('/add/seller/to/product/' + fData.data.product.id) : () => goTo('/edit/seller/' + fData.data.product.id)}
                sellerSourceId={fData.data.seller[0] ? fData.data.seller[0].sourceId : undefined}
                productSourceId={fData.data.source[0] !== undefined ? fData.data.source[0].id : undefined}
                date={fData.data.product.createdAt}
                originallyAddedBy={fData.data.addedBy[0] ? fData.data.addedBy[0].name : 'undefined'}
                costs={fData.data.product.costPerProduct}
                sourceId={fData.data.product.sourceId}
            ></ProductMainInfo>
            <ProductGHGData
                productName={fData.data.product.name}
                totalGHG={fData.data.totalEmission.length > 0 ? fData.data.totalEmission[0].ghgPerKG : "Not provided"}
                totalGHGSourceId={fData.data.totalEmission.length > 0 ? fData.data.totalEmission[0].sourceId : undefined}
                totalGHGEdit={() => goTo('/verify/source/' + fData.data.totalEmission[0].sourceId + '/' + fData.data.product.id + '/total/0/new/' + fData.data.totalEmission[0].id)}
                totalGHGStatus={fData.data.totalEmission[0]}
                GHGSingle={fData.data.singleEmission}
                EmissionReason={fData.data.emissionReason}
                eventEditProduct={() => goTo('/edit/product/' + fData.data.totalEmission[0].id + '/' + fData.data.product.id)}
                eventEditTotal={() => goTo('/edit/total/' + fData.data.totalEmission[0].id + '/' + fData.data.product.id)}
                eventHistorySourceSingle={fData.data.singleEmission[id.index] ? () => goTo('/source/' + fData.data.singleEmission[id.index].sourceId + '/' + fData.data.product.id) : console.log("test")}
                eventHistorySoueceReason={fData.data.emissionReason[id.index] ? () => goTo('/source/' + fData.data.emissionReason[id.index].sourceId + '/' + fData.data.product.id) : console.log("test")}
                data={fData}
                productId={fData.data.product.id}
            ></ProductGHGData>

            <ProductAlternatives
                productId={fData.data.product.id}
                name={fData.data.product.name}
                alternativeProducts={fData.data.altternativeProduct}
                rawMaterials={fData.data.productMadeOfRawMaterial}
            ></ProductAlternatives>
        </>
        : <p>...Loading</p>

    return (
        <> { cards && fData ? cards
        : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default FullProduct;
