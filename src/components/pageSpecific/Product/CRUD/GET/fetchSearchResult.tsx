import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import SearchResult from "../../UI/FullComponents/searchResult";
import Button from "../../../../BuildingBlocks/templateButton";
import goTo from "../../../../../helper/goTo";
import { urlBE } from "../../../../../App";

const FetchedSearchResult = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, [fData, id.name, id.approved]);

    const loadData = async () => {
        fetch(`${urlBE}/api/o/search/name/${id.name}/${id.approved}`, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }   

    return (
        <> { fData && fData.data.responseObj.length !== 0 ? 
            <SearchResult
                productId={fData.data.responseObj[0].dataValues.id}
                productName={fData.data.responseObj[0].dataValues.name}
                description={fData.data.responseObj[0].dataValues.description}
                approvalStatus={fData.data.responseObj[0].dataValues.approvalStatus}
                createdAt={fData.data.responseObj[0].dataValues.createdAt}
                searchs={id.name}
            />
            : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>
            : <MainCard marginTop={'0'} marginBottom={'20px'}>
                <p>We could not find any entry for {id.name}</p>
                <p>If you have matching data, feel free to add it to Greenformed.</p>
                <Button
                    event={() => goTo('/new/product')}
                    bg={"white"}
                    border={'blue'}
                    width={'250px'}
                    color={'black'}
                    margin={'0'}
                >Add new data</Button>
            </MainCard>
            } 
            
        </>
    )
};

export default FetchedSearchResult;
