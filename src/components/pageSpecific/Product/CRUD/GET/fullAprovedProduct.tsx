import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import ProductMainInfo from "../../UI/ApprovedComponents/mainInfo";
import Categroy from "../../UI/ApprovedComponents/category";
import Seller from "../../UI/ApprovedComponents/sellerCard";
import Total from "../../UI/ApprovedComponents/totalEmission";
import Single from "../../UI/ApprovedComponents/singleEmission";
import Reason from "../../UI/ApprovedComponents/reasonOfEmission";

type ApprovedFullProductProds = {
    url: string,
    type: string,
}

const ApprovedFullProduct = (props: ApprovedFullProductProds) => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch('https://greenformedsstbe-jnzm36w3ca-ew.a.run.app/api/o/product/get/' + id.name, { 
        method: 'get', 
        headers: {
            "X-Content-Type-Options": "nosniff"
        },
      }).then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res;
        })
        .then(resData => setFData(resData))
        .catch(err => setFError(err));
}

    let cards: any;
    
    if (fData && !fError) {
    cards = <div>
                <div>
                    <div>
                        <ProductMainInfo
                            productId={fData.data.product.id}
                            productName= {fData.data.product.name}
                            description={fData.data.product.description}
                            link={fData.data.product.linkToBuy}
                            costPerProduct={fData.data.product.costPerProduct}
                            createdAt={fData.data.product.createdAt}
                            transferredFristAt={fData.data.product.transferredFirst}
                            lastUpdateAt={fData.data.product.lastUpdate}
                        />
                    </div>
                </div>
            { fData.data.catgeory[0] !== undefined ?
                <Categroy
                    productName= {fData.data.product.name}
                    typeOfCategory={fData.data.category[0].typeOfCategory}/> : <></>
            }
            { fData.data.seller[0] !== undefined ?
                <Seller
                    productName={fData.data.product.name}
                    sellerId={fData.data.seller[0].id} 
                    sellerName={fData.data.seller[0].name} 
                    sellerLink={fData.data.seller[0].link} 
                    createdAt={fData.data.seller[0].createdAt} 
                    transferredFristAt={fData.data.seller[0].transferredFirst} 
                    lastUpdateAt={fData.data.seller[0].lastUpdate} 
                    />
                     : <></>
            }
            { fData.data.totalEmission[0] !== undefined ?
                <Total
                    productName={fData.data.product.name}
                    id={fData.data.totalEmission[0].id}
                    ghgPerKG={fData.data.totalEmission[0].ghgPerKG}
                    createdAt={fData.data.totalEmission[0].createdAt} 
                    transferredFristAt={fData.data.totalEmission[0].transferredFirst} 
                    lastUpdateAt={fData.data.totalEmission[0].lastUpdate} 
                    /> : <></>
            }
            { fData.data.singleEmission[0] !== undefined ?  
            <Single
                key={fData.data.singleEmission[0].id}
                productName={fData.data.product.name}
                id={fData.data.singleEmission[0].id}
                ghgName={fData.data.singleEmission[0].ghgName}
                emissionPerKG={fData.data.singleEmission[0].emissionPerKG}
                createdAt={fData.data.singleEmission[0].createdAt} 
                transferredFristAt={fData.data.singleEmission[0].transferredFirst} 
                lastUpdateAt={fData.data.singleEmission[0].lastUpdate} 
            /> : <div></div> }
            { fData.data.emissionReason[0] !== undefined ? 
            <Reason
                key={fData.data.emissionReason[0].id}
                productName={fData.data.product.name}
                id={fData.data.emissionReason[0].id}
                ghgName={fData.data.emissionReason[0].ghgName}
                reasonTitle={fData.data.emissionReason[0].reasonTitle}
                description={fData.data.emissionReason[0].description}
                createdAt={fData.data.emissionReason[0].createdAt} 
                transferredFristAt={fData.data.emissionReason[0].transferredFirst} 
                lastUpdateAt={fData.data.emissionReason[0].lastUpdate} 
            /> : <></> }
            { fData.data.altternativeProduct[0] !== undefined ? 
            <ProductMainInfo
                key={fData.data.altternativeProduct[0].id}
                productId={fData.data.altternativeProduct[0].id}
                productName= {fData.data.altternativeProduct[0].name}
                description={fData.data.altternativeProduct[0].description}
                link={fData.data.altternativeProduct[0].linkToBuy}
                costPerProduct={fData.data.altternativeProduct[0].costPerProduct}
                createdAt={fData.data.altternativeProduct[0].createdAt}
                transferredFristAt={fData.data.altternativeProduct[0].transferredFirst}
                lastUpdateAt={fData.data.altternativeProduct[0].lastUpdate}
            /> : <></> }
            { fData.data.productMadeOfRawMaterial[0] !== undefined ?  
            <ProductMainInfo
                key={fData.data.productMadeOfRawMaterial[0].id}
                productId={fData.data.productMadeOfRawMaterial[0].id}
                productName= {fData.data.productMadeOfRawMaterial[0].name}
                description={fData.data.productMadeOfRawMaterial[0].description}
                link={fData.data.productMadeOfRawMaterial[0].linkToBuy}
                costPerProduct={fData.data.productMadeOfRawMaterial[0].costPerProduct}
                createdAt={fData.data.productMadeOfRawMaterial[0].createdAt}
                transferredFristAt={fData.data.productMadeOfRawMaterial[0].transferredFirst}
                lastUpdateAt={fData.data.productMadeOfRawMaterial[0].lastUpdate}
            /> : <></> }
        </div>
    } else {
        cards=<p>...loading</p>
    }

    return (
        <> { cards && fData ? <div>{cards}</div>
            : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>
            : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default ApprovedFullProduct;
