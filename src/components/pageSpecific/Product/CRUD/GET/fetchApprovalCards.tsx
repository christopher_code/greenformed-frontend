import { useState, useEffect } from 'react';
import ApprovalCards from "../../UI/ApprovedComponents/approvedProductCard";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import goTo from "../../../../../helper/goTo";
import Button from "../../../../BuildingBlocks/templateButton";
import Search from "../../../../BuildingBlocks/tempalteSearchBar";

type ProductApprovalCardProps = {
    url: string
}

const ProductpprovalCardsFetched = (props: ProductApprovalCardProps) => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();
    const [searchApproved, setSearchApproved] = useState<string>('');

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(props.url, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          })
          .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }  

    const cards = !fError && fData && fData.data.map((item: any) => 
        <ApprovalCards
            key={item.product.dataValues.id}
            productName={item.product.dataValues.name}
            description={item.product.dataValues.description}
            linkToBuy={item.product.dataValues.linkToBuy}
            costPerProduct={item.product.dataValues.costPerProduct}
            createdAt={item.product.dataValues.createdAt}
            transferredAt={item.product.dataValues.transferredFirst}
            lastUpdateAt={item.product.dataValues.lastUpdate}
            getMoreEvent={() => goTo("/approved/product/" + item.product.dataValues.id)}
            getFullDataEvent={() => goTo("/product/" + item.product.dataValues.id)}
        ></ApprovalCards>)

    return (
        <> { !fError && cards && fData ? 
        <div>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                <div className="bottomBarFlex">
                    <h3 className="bottomBarHeadline">Here you see approved data.</h3>
                </div>
            </MainCard>
            {cards}
            <MainCard marginTop={'20px'} marginBottom={'0'}>
                <div className="bottomBarFlex">
                    <h3 className="bottomBarHeadline">Get more approved data.</h3>
                    <Button 
                        event={loadData}
                        bg={"#A3A9C3"}
                        border={'#A3A9C3'}
                        width={"215px"}
                        color={"#36354A"}
                        margin={"20px 0 0 0"}
                    >
                        Load more data
                    </Button>
                </div>
            </MainCard>
            <MainCard marginTop={'20px'} marginBottom={'-20px'}>
                <div className="bottomBarFlex">
                <h3 className="bottomBarHeadline">Search for approved products on the Greenformed platform.</h3>
                    <Search 
                        event={() => goTo('/search/' + searchApproved + "/approved")} 
                        bg={"white"}
                        border={"#41475E"}
                        icon={"red"}
                        placeholder={"Search"}
                        fontColor={"#41475E"}
                        borderColor={"#41475E"}
                        width="130px"
                        changeEvent={(e: any) => setSearchApproved(e.target.value)}
                    >
                    Search
                    </Search>
                </div>
            </MainCard>
        </div> 
            : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>
            : 
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            } 
        </>
    )
};

export default ProductpprovalCardsFetched;
