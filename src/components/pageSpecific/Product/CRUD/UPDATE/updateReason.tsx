import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import UpdateForm from "../../../../BuildingBlocks/templateFormUpdate";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import RequestDeleteForm from "../../../../BuildingBlocks/templateFormDelete";
import Modal from "../../../../BuildingBlocks/templateModal";
import goTo from "../../../../../helper/goTo";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";

const UpdateReasonData = () => {
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDesc, setSourceDesc] = useState<string>('');
    const [reasonGHGChanged, setReasonGHGChanged] = useState<string>('');
    const [reasonGHGDescriptionChanged, setReasonGHGDescriptionChanged] = useState<string>('');
    const [GHGNameChanged, setGHGNameChanged] = useState<string>('');
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();
    const [sError, setSError] = useState<any>();
    const [selectedField, setSelectedField] = useState<string>('');
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');
    let id: any = useParams();

    useEffect(() => {
        loadLocalStorage();
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    const loadData = async () => {
        fetch(urlBE + '/api/o/product/get/full/' + id.id, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .then(()=> setFError(undefined))
            .catch(err => setFError(err));
    } 

    const updateExistingData = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/ghg/reason/update/fields/${fData.data.emissionReason[0].id}/${userId}/${id.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff" 
                },
                body: JSON.stringify({
                    reasonTitle: reasonGHGChanged,
                    description: reasonGHGDescriptionChanged,
                    ghgName: GHGNameChanged,
                    sourceTitle: 'Update of reason of GHG emission fields',
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then(() => {
                setReasonGHGChanged('')
                setReasonGHGDescriptionChanged('')
                setGHGNameChanged('')
                setSourceLink('')
                setSourceDesc('')
                setSError(undefined)
                goTo('/product/' + id.id)
              })
              .catch(error => {
                setSError(error);
              }
            );
      }

      const deleteData = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/ghg/reason/request/deletion/${fData.data.emissionReason[0].id}/${userId}/${id.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"    
                },
                body: JSON.stringify({
                    sourceTitle: 'Source of the deletion request of reason of emssion data',
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then(() => {
                setSourceLink('')
                setSourceDesc('')
                setSError(undefined)
                goTo('/product/' + id.id)
              })
              .catch(error => {
                setSError(error);
              }
            );
      }

    return (
        <MainCard
            marginTop={"0"}
            marginBottom={'20px'}
        >
        { fData ?
            <div className="containerFormUpdateProduct">
                    <div>
                        <h2>Reason of {fData.data.emissionReason[0].ghgName} Emission: {fData.data.emissionReason[0].reasonTitle}</h2>
                        <p>Choose the field that you want to update: </p>
                        <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                            <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                            <p className="modalButtonPre">Get more information</p>
                        </div>
                    { showModalData ?
                    <Modal 
                        info={'Select the type of data you want to update: request to change the reason title, the description or the name of the GHG. Or request to delete it.'}
                        eventClose={() => setShowModalData(false)}
                        show={showModalData}
                    /> : <></> }

                        <div className='updateSelctButtonsDiv'>
                            <div className='updateSelectDivSelect' id='title' onClick={() => setSelectedField('title')}>Emission Reason Title</div>
                            <div className='updateSelectDivSelect' id='description' onClick={() => setSelectedField('description')}>Reason Description</div>
                            <div className='updateSelectDivSelect' id='name' onClick={() => setSelectedField('name')}>GHG Name</div>
                            <div className='updateSelectDivSelect' id='delete' onClick={() => setSelectedField('delete')}>Delete this reason</div>
                        </div>

                        {
                            selectedField === 'title' ?
                            <form onSubmit={updateExistingData}>
                                <UpdateForm
                                    eventUpdateField={setReasonGHGChanged}
                                    label={'Update the title of the Reason:'}
                                    typeUpdate={'text'}
                                    value={reasonGHGChanged}
                                    originalValue={fData.data.emissionReason[0].reasonTitle}
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                />
                                <div className="buttonDivSignUp">
                                    <input type="submit" value="Submit" className="formButtonGeneral" />
                                </div>
                            </form>
                            : selectedField === 'description' ?
                            <form onSubmit={updateExistingData}>
                                <UpdateForm
                                    eventUpdateField={setReasonGHGDescriptionChanged}
                                    label={'Update the description of the emission reason:'}
                                    typeUpdate={'text'}
                                    value={reasonGHGDescriptionChanged}
                                    originalValue={fData.data.emissionReason[0].description}
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                />
                                <div className="buttonDivSignUp">
                                    <input type="submit" value="Submit" className="formButtonGeneral" />
                                </div>
                            </form>
                            : selectedField === 'name' ?
                            <form onSubmit={updateExistingData}>
                                <UpdateForm
                                    eventUpdateField={setGHGNameChanged}
                                    label={'Update the name of the GHG:'}
                                    typeUpdate={"text"}
                                    value={GHGNameChanged}
                                    originalValue={fData.data.emissionReason[0].ghgName}
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                />
                               <div className="buttonDivSignUp">
                                    <input type="submit" value="Submit" className="formButtonGeneral" />
                                </div>
                            </form>
                            : selectedField === 'delete' ?
                            <form onSubmit={deleteData}>
                                <RequestDeleteForm
                                    eventSourceLink={setSourceLink}
                                    sourceLink={sourceLink}
                                    eventSourceDescription={setSourceDesc}
                                    sourceDescr={sourceDesc}
                                    name={fData.data.emissionReason[0].reasonTitle + ' (listed as a reason of GHG Emission in Greenformed) '}
                                />
                                <div className="buttonDivSignUp">
                                    <input type="submit" value="Submit" className="formButtonGeneral" />
                                </div>
                            </form>
                            : <></>
                        }


                    </div>
                </div> :
                    fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                    : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                    : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                    : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>  
                    : sError && sError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                    : sError && sError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                    : sError && sError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                    : sError && sError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                    : sError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
                    :  
                <div className="skeletonWidth">
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    </div> 
            }
        </MainCard>
    )
};

export default UpdateReasonData;
