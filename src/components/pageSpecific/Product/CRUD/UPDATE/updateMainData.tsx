import {useState, useEffect} from 'react';
import MainBar from "../../../../BuildingBlocks/templateMainBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import UpdateForm from "../../../../BuildingBlocks/templateFormUpdate";
import UpdateFormNumber from "../../../../BuildingBlocks/templateFormUpdateNumner";
import Modal from "../../../../BuildingBlocks/templateModal";
import goTo from "../../../../../helper/goTo";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";
import RequestDeleteForm from "../../../../BuildingBlocks/templateFormDelete";

type ProductCardProps = {
    name: string, 
    description: string,
    linkToBuy: string,
    costPerProduct: number,
    productId: string,
};

const UpdateMainData = (props: ProductCardProps) => {
    const [fError, setFError] = useState<any>();
    const [description] = useState<string>(props.description);
    const [linkToBuy] = useState<string>(props.linkToBuy);
    const [costs] = useState<number>(props.costPerProduct);
    const [descriptionChanged, setDescription] = useState<string>();
    const [linkToBuyChanged, setLinkToBuy] = useState<string>();
    const [costsCahnged, setCostsChanged] = useState<any>();
    const [selectedField, setSelectedField] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDesc, setSourceDesc] = useState<string>('');
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    const updateExistingData = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/product/update/fields/${props.productId}/${userId}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"  
                },
                body: JSON.stringify({
                    description: descriptionChanged,
                    linkToBuy: linkToBuyChanged,
                    costPerProduct: costsCahnged,
                    sourceTitle: "Source of updating the product fields",
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then(() => {
                setSourceLink('')
                setSourceDesc('')
                setDescription('')
                setLinkToBuy('')
                setCostsChanged(null)
                setFError(undefined)
                goTo('/product/' + props.productId)
              })
              .catch(error => {
                setFError(error);
              }
            );
      }

      const deleteData = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/product/request/deletion/${props.productId}/${userId}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"  
                },
                body: JSON.stringify({
                    sourceTitle: 'Source for the request to delete the product and its relationships',
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then(() => {
                setSourceLink('')
                setSourceDesc('')
                setFError(undefined)
                goTo('/product/' + props.productId)
              })
              .catch(error => {
                setFError(error);
              }
            );
      }

    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'-20px'}
        >
            <div className="containerFormUpdateProduct">
                <div>
                    <h2 className="headlineUpdateForm">Name of the product: {props.name}</h2>
                    <p>Choose the field that you want to update: </p>
                    <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                        <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                        <p className="modalButtonPre">Get more information</p>
                    </div>
                    { showModalData ?
                    <Modal 
                        info={'Select the type of data you watn to update: request to change the description, the link or the cost of the product. Or request to delete the proudct.'}
                        eventClose={() => setShowModalData(false)}
                        show={showModalData}
                    /> : <></> }
                    <div className='updateSelctButtonsDiv'>
                        <div className='updateSelectDivSelect' id='description' onClick={() => setSelectedField('description')}>Product Description</div>
                        <div className='updateSelectDivSelect' id='link' onClick={() => setSelectedField('link')}>Product Link To Buy</div>
                        <div className='updateSelectDivSelect' id='costs' onClick={() => setSelectedField('costs')}>Product Costs</div>
                        <div className='updateSelectDivSelect' id='delete' onClick={() => setSelectedField('delete')}>Deletion Request</div>
                    </div>

                    {
                        selectedField === 'description' ?
                        <form onSubmit={updateExistingData}>
                            <p className='updateNameType'>Update the Description of that product</p>
                            <UpdateForm
                                eventUpdateField={setDescription}
                                label={'Update the product description:'}
                                typeUpdate={'text'}
                                value={descriptionChanged}
                                originalValue={description}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                           <div className="buttonDivSignUp">
                                <input type="submit" value="Submit" className="formButtonGeneral" />
                            </div>
                        </form>
                        : selectedField === 'link' ?
                        <form onSubmit={updateExistingData}>
                        <p className='updateNameType'>Update the Link to buy of the product</p>
                            <UpdateForm
                                eventUpdateField={setLinkToBuy}
                                label={'Update the link to buy the product:'}
                                typeUpdate={'text'}
                                value={linkToBuyChanged}
                                originalValue={linkToBuy}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                            <div className="buttonDivSignUp">
                                <input type="submit" value="Submit" className="formButtonGeneral" />
                            </div>
                        </form>
                        : selectedField === 'costs' ?
                        <form onSubmit={updateExistingData}>
                        <p className='updateNameType'>Update the costs per unit of that product</p>
                            <UpdateFormNumber
                                eventUpdateField={setCostsChanged}
                                label={'Update the costs of the product:'}
                                typeUpdate={"number"}
                                value={costsCahnged}
                                originalValue={costs}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                            <div className="buttonDivSignUp">
                                <input type="submit" value="Submit" className="formButtonGeneral" />
                            </div>
                        </form>
                        : selectedField === 'delete' ?
                        <form onSubmit={deleteData}>
                        <p className='updateNameType'>Request to delete that product</p>
                            <RequestDeleteForm
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                                name={props.name + ' (listed as a product in Greenformed) '}
                            />
                            <div className="buttonDivSignUp">
                                <input type="submit" value="Submit" className="formButtonGeneral" />
                            </div>
                        </form>
                        : <></>
                    }
                    { fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                    : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                    : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                    : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                    : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
                    : <></>}
                </div>
                
            </div>
        </MainBar>
    )
};

export default UpdateMainData;