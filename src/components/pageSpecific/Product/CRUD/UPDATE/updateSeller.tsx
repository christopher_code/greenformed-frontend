import {useState, useEffect} from 'react';
import { useParams } from "react-router-dom";
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import UpdateForm from "../../../../BuildingBlocks/templateFormUpdate";
import RequestDeleteForm from "../../../../BuildingBlocks/templateFormDelete";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import Modal from "../../../../BuildingBlocks/templateModal";
import goTo from "../../../../../helper/goTo";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";

const UpdateSellerData = () => {
    const [selectedField, setSelectedField] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDesc, setSourceDesc] = useState<string>('');
    const [sellerLinkChanged, setSellerLinkChanged] = useState<string>('');
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();
    const [sError, setSError] = useState<any>();
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    let id: any = useParams();

    useEffect(() => {
        loadLocalStorage();
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    const loadData = async () => {
        fetch(urlBE + '/api/o/product/get/full/' + id.id, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    const updateExistingData = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/seller/update/fields/${fData.data.seller[0].id}/${userId}/${id.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"    
                },
                body: JSON.stringify({
                    link: sellerLinkChanged,
                    sourceTitle: 'Source of updating the seller link',
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then(() => {
                setSourceLink('')
                setSourceDesc('')
                setSError(undefined)
                goTo('/product/' + id.id)
              })
              .catch(error => {
                setSError(error);
              }
            );
      }

      const deleteData = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/seller/request/deletion/${fData.data.seller[0].id}/${userId}/${id.id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"    
                },
                body: JSON.stringify({
                    sourceTitle: 'Source of requesting to delete this seller',
                    sourceLink: sourceLink,
                    sourceDescription: sourceDesc, 
                })
            }).then(res => {
                console.log(res)
                return res.json();
              })
              .then(() => {
                setSError(undefined)
                setSourceLink('')
                setSourceDesc('')
                goTo('/product/' + id.id)
              })
              .catch(error => {
                setSError(error);
              }
            );
      }

    return (
        <MainCard
            marginTop={"0"}
            marginBottom={'20px'}
        >
        { fData && !fError ?
            <div className="containerFormUpdateProduct">
                <div>
                    { selectedField === '' ?
                        <>
                        <h2>Update the data of {fData.data.seller[0].name} (Seller)</h2>
                        <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                            <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                            <p className="modalButtonPre">Get more information</p>
                        </div>
                        { showModalData ?
                            <Modal 
                                info={'You can change the link of the seller'}
                                eventClose={() => setShowModalData(false)}
                                show={showModalData}
                            /> : <></> }

                        <form onSubmit={updateExistingData}>
                            <UpdateForm
                                eventUpdateField={setSellerLinkChanged}
                                label={'Update the link of the seller:'}
                                typeUpdate={'text'}
                                value={sellerLinkChanged}
                                originalValue={fData.data.seller[0].link}
                                eventSourceLink={setSourceLink}
                                sourceLink={sourceLink}
                                eventSourceDescription={setSourceDesc}
                                sourceDescr={sourceDesc}
                            />
                            <div className="buttonDivSignUp">
                                <input type="submit" value="Submit" className="formButtonGeneral" />
                            </div>
                        </form>
                        <div onClick={() => setSelectedField('delete')}>Request to delete the product.</div>
                        </>
                        : selectedField === 'delete' ? 
                            <>
                                <form onSubmit={deleteData}>
                                    <RequestDeleteForm
                                        eventSourceLink={setSourceLink}
                                        sourceLink={sourceLink}
                                        eventSourceDescription={setSourceDesc}
                                        sourceDescr={sourceDesc}
                                        name={fData.data.seller[0].name + ' (listed as a seller in Greenformed) '}
                                    />
                                    <div className="buttonDivSignUp">
                                        <input type="submit" value="Submit" className="formButtonGeneral" />
                                    </div>
                                </form>
                                <div onClick={() => setSelectedField('')}>Change seller fields.</div>
                            </> : <></>
                        }
                </div>
            </div> : 
            fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
            : sError && sError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : sError && sError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : sError && sError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
            : sError && sError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : sError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
            :
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        }
        </MainCard>
    )
};

export default UpdateSellerData;
