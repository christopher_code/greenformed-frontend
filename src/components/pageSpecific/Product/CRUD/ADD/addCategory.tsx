import {useState, useEffect } from 'react';
import Button from "../../../../BuildingBlocks/templateButton";
import Search from "../../../../BuildingBlocks/tempalteSearchBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import { NewProductProps } from "../../UI/FullComponents/newProduct";
import { urlBE } from "../../../../../App";

const AddCategory = (props: NewProductProps) => {
    const [category, setCategory] = useState<any>();
    const [isSet, setIsSet] = useState<boolean>(false);
    const [fError, setFError] = useState<any>();
    const [fErrorCat, setFErrorCat] = useState<any>();
    const [searchParam, setSearchParam] = useState<string>('');
    const [searchResult, setSearchResult] = useState<any>();
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    let skipHandle: any = () => {
        props.func();
    }

    const searchForCategory = async () => {
        fetch(`${urlBE}/api/o/category/get/${searchParam}`, { 
            method: 'GET', 
            headers: {
                "X-Content-Type-Options": "nosniff",
                "Content-Type": "application/json",  
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setSearchResult(resData))
            .then(() => {setFErrorCat(undefined);})
            .catch(err => setFErrorCat(err));
    }    

    const postAddCategory = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/category/new/${userId}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"
                },
                body: JSON.stringify({
                    typeOfCategory: category 
                })
            }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((res) => {
                setIsSet(true)
                setFError(undefined);
                props.func()
            })
            .catch(error => {
                setIsSet(false);
                setFError(error);
            }
        );
      }

      const putExistingCategory = (e: any) => {
        fetch(`${urlBE}/api/product/update/first/relationship/category/${props.productId}/${e}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token
                 },
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })  
              .then((res) => {
                setIsSet(true);
                props.func();
              })
      }

    return (
        <>  
        {
            isSet === false ?
            <>
                <h2 className="addProductCategoryHeadline">Add a category to the product.</h2>
                <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                    <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                    <p className="modalButtonPre">Get more information</p>
                </div>
                { showModalData ?
                <Modal 
                    info={'Search for an existing category or add a new one.'}
                    eventClose={() => setShowModalData(false)}
                    show={showModalData}
                /> : <></> }
                    <label>Search for an existing category: </label>
                    <div className="categorySelectDivFlex">
                        <div>
                            <Search
                                event={searchForCategory}
                                bg='white'
                                fontColor='grey'
                                borderColor='grey'
                                border='grey'
                                icon=''
                                placeholder='search existing categories'
                                width='300px'
                                changeEvent={(e: any) => setSearchParam(e.target.value)}
                            >Search</Search>
                            {
                                searchResult && !fErrorCat && searchResult.data.category !== false ? 
                                <div>
                                    <p>We found this category: {searchResult.data.category.dataValues.typeOfCategory}</p>
                                    <Button
                                        event={() => putExistingCategory(searchResult.data.category.dataValues.typeOfCategory)}
                                        bg={"#41475E"}
                                        border={'transparaent'}
                                        width={'300px'}
                                        color={'white'}
                                        margin={'10px 0 0 0'}
                                    >Set as category</Button> 
                                    : <></>
                                </div> 
                                :
                                searchResult && searchResult.data.category === false ?
                                <p>{searchParam} does not exist in our database. Feel free to add it below.</p> :
                                <></>
                            }
                            <div className="deviderDivAddFields"></div>
                        </div>
                        <div className="addProductCategoryAddDiv">
                            <form onSubmit={postAddCategory}>
                                <h2 className="addProductCategoryHeadline">Or add a new category: </h2>
                                <label>Add a new Category</label> 
                                <input className="form-control me-2"
                                    id="inputNewCategory" 
                                    type="text" 
                                    placeholder="Category"
                                    aria-label="inputDescriptionProduct"
                                    value={category}
                                    onChange={(e) => setCategory(e.target.value)}
                                    minLength={3}
                                    required
                                    ></input>
                                <div className="buttonDivSignUp">
                                    <input type="submit" value="Submit" className="formButtonGeneral" />
                                </div>
                            </form>
                            { fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                                : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                                : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >This category already exists please search for it above and select it.</MessageCard>
                                : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
                                : <></>}
                            {   fErrorCat && fErrorCat.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                                : fErrorCat && fErrorCat.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                                : fErrorCat && fErrorCat.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                                : fErrorCat && fErrorCat.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                                : fErrorCat ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>                             
                                : <></>
                            }
                        </div>
                    </div>
                    { props.skipHandle ?
                    <>
                        <div className="deviderDivAddFields"></div>
                        <h2 className="addProductCategoryHeadline">Or skip this field: </h2>
                        <Button
                            event={skipHandle}
                            bg={"#41475E"}
                            border={'transparaent'}
                            width={'250px'}
                            color={'white'}
                            margin={'10px 0 0 0'}
                        >
                            Skip this field
                    </Button>
                    </> : <></>
                    }
                </>
                : category && isSet ? 
                    <div>
                        <p>The category is set to {category}.</p>
                    </div>
                : <div>
                    <p>The category is set.</p>
                </div>
                }
        </>
    )
};

export default AddCategory;