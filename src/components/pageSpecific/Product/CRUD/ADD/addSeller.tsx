import {useState, useEffect } from 'react';
import Button from "../../../../BuildingBlocks/templateButton";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { NewProductProps } from "../../UI/FullComponents/newProduct";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";

const ProductCard = (props: NewProductProps) => {
    const [seller, setSeller] = useState<string>('');
    const [fError, setFError] = useState<any>();
    const [link, setLink] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [isSet, setIsSet] = useState<boolean>(false);
    const [showModalSource, setShowModalSource] = useState<boolean>(false);
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    let skipHandle: any = () => {
        props.func();
    }  

    const postAddNewSeller = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/seller/new/${userId}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"
                },
                body: JSON.stringify({
                    sourceTitle: "Seller Source of " + seller,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    name: seller, 
                    link: link,
                })
            }).then(res => {
                  return res.json();
              })
              .then(() => {
                props.func();
                setIsSet(true);
                setFError(undefined);
              })
              .catch(error => {
                setIsSet(false);
                setFError(error);
              }
            );
      }

    return (
        <>
        {
            isSet === false ?
            <>
            <h2 className="addProductCategoryHeadline">Add the Company that sells this product</h2>
                <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                    <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                    <p className="modalButtonPre">Get more information</p>
                </div>
                { showModalData ?
                <Modal 
                    info={'To add a new one please add a name and a link. If the seller is already in our database, we link them together.'}
                    eventClose={() => setShowModalData(false)}
                    show={showModalData}
                /> : <></> }
                <div className="categorySelectDivFlex">
                    <div className="addProductCategoryAddDiv">
                        <form onSubmit={postAddNewSeller}>
                            <h2 className="addProductCategoryHeadline">Add a new seller: </h2>
                            <label className="labelNewProduct">Seller Name *</label> 
                            <input className="form-control me-2"
                                id="inputNewSeller" 
                                type="text" 
                                placeholder="Seller Name"
                                aria-label="inputNewSeller"
                                value={seller}
                                onChange={(e) => setSeller(e.target.value)}
                                minLength={3}
                                required
                                ></input>
                            <label className="labelNewProduct">Seller Link *</label> 
                            <input className="form-control me-2"
                                id="inputNewSellerSourceLink" 
                                type="url" 
                                placeholder="Seller Link"
                                aria-label="inputNewSellerSourceLink"
                                value={link}
                                onChange={(e) => setLink(e.target.value)}
                                minLength={4}
                                required
                                ></input>
                            <h2 className="newProductDescriptionStepSeller">In order to save the data you need to proof that it is valid. Therefore, a source needs to be added.</h2>
                            <div className="modalDivRow" onClick={() => setShowModalSource(true)}>
                                <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                                <p className="modalButtonPre">Get more information</p>
                            </div>
                            { showModalSource ?
                                <Modal 
                                    info={'The source`s link should display a url that leads to a valid data source confirming you information. In the description you can specify where the data is in the source link (at least 20 characters).'}
                                    eventClose={() => setShowModalSource(false)}
                                    show={showModalSource}
                                /> : <></> }
                            <label className="labelNewProduct">Source Description *</label> 
                            <textarea className="form-control me-2"
                                id="inputNewSellerSourceDescription" 
                                placeholder="Source Description"
                                aria-label="inputNewSellerSourceDescription"
                                value={sourceDescription}
                                onChange={(e) => setSourceDescription(e.target.value)}
                                minLength={20}
                                required
                                ></textarea>
                            <label className="labelNewProduct">Add a Source Link that backs your description *</label> 
                            <input className="form-control me-2"
                                id="inputNewSellerSourceLink" 
                                type="url" 
                                placeholder="Source Link"
                                aria-label="inputDescriptionProduct"
                                value={sourceLink}
                                onChange={(e) => setSourceLink(e.target.value)}
                                minLength={4}
                                required
                                ></input>
                            <div className="buttonDivSignUp">
                                <input type="submit" value="Submit" className="formButtonGeneral" />
                            </div>
                        </form>
                    </div>
                </div>
                { fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                    : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                    : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                    : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                    : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
                    : <></>}
                { props.skipHandle ?
                <>
                    <div className="deviderDivAddFields"></div>
                    <h2 className="addProductCategoryHeadline">Or skip this field: </h2>
                    <Button
                            event={skipHandle}
                            bg={"#41475E"}
                            border={'transparaent'}
                            width={'250px'}
                            color={'white'}
                            margin={'10px 0 0 0'}
                        >
                            Skip this field
                        </Button> </> : <></>
                }
        </> : seller && isSet ?
            <p>The seller is set to {seller}.</p>
        : <p>The seller is set.</p>
        }
       </>
    )
};

export default ProductCard;