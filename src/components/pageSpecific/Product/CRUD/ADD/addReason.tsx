import {useState, useEffect} from 'react';
import { NewProductProps } from "../../UI/FullComponents/newProduct";
import Button from "../../../../BuildingBlocks/templateButton";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";

const ProductCard = (props: NewProductProps) => {
    const [fError, setFError] = useState<any>();
    let [timesSend, setTimesSend] = useState<number>(0);
    const [ghgName, setGHGName] = useState<string>('');
    const [reasonTitle, setReasonTitle] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [sourceTitle, setSourceTitle] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [showModalSource, setShowModalSource] = useState<boolean>(false);
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

      const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    let skipHandle: any = () => {
        props.func();
    }

    const postAddReasonGHG = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/ghg/reason/new/${userId}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"
                },
                body: JSON.stringify({
                    sourceTitle: sourceTitle,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    ghgName: ghgName,
                    reasonTitle: reasonTitle,
                    description: description,
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then((res) => {
                setSourceTitle("")
                setSourceLink("")
                setSourceDescription("")
                setTimesSend(1);
                setGHGName('')
                setReasonTitle('')
                setDescription('')
                props.func();
                setFError(undefined);
              })
              .catch(error => {
                setFError(error);
              }
            );
      }

    return (
        <>
            {
                timesSend === 0 ? <h2 className="newProductDescriptionStepSeller">Add a reason of Green House Gases Emission: </h2> 
                : <h2 className="newProductDescriptionStepSeller">Add another reason for Green House Gases Emission: </h2>
            }
            <div className="addProductCategoryAddDiv">
                <form onSubmit={postAddReasonGHG}>
                    <h2 className="addProductCategoryHeadline">Add a new GHG Reason: </h2>
                    <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                        <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                        <p className="modalButtonPre">Get more information</p>
                    </div>
                    { showModalData ?
                    <Modal 
                        info={'In order to add a reason of GHG Emission, add the name of the GHG, the Reason and a description.'}
                        eventClose={() => setShowModalData(false)}
                        show={showModalData}
                    /> : <></> }
                    <label className="labelNewProduct">GHG Name *</label> 
                    <input className="form-control me-2"
                        id="inoutReasonFieldGHGName" 
                        type="text" 
                        placeholder="GHG Name"
                        aria-label="inoutReasonFieldGHGName"
                        value={ghgName}
                        onChange={(e) => setGHGName(e.target.value)}
                        maxLength={30}
                        minLength={2}
                        required
                        ></input>
                    <label className="labelNewProduct">Reason *</label> 
                    <input className="form-control me-2"
                        id="inoutReasonFieldReasonname" 
                        type="text" 
                        placeholder="Reason ABC"
                        aria-label="inoutReasonFieldReasonname"
                        value={reasonTitle}
                        onChange={(e) => setReasonTitle(e.target.value)}
                        minLength={3}
                        required
                        ></input>
                    <label className="labelNewProduct">Reason Description *</label> 
                    <textarea className="form-control me-2"
                        id="inoutReasonFieldDescRes" 
                        placeholder="Reason Description"
                        aria-label="inoutReasonFieldDescRes"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        minLength={10}
                        required
                        ></textarea>
                    <h2 className="newProductDescriptionStepSeller">In order to save the data you need to proof that it is valid. Therefore, a source needs to be added.</h2>
                    <div className="modalDivRow" onClick={() => setShowModalSource(true)}>
                        <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                        <p className="modalButtonPre">Get more information</p>
                    </div>
                    { showModalSource ?
                        <Modal 
                            info={'The source`s link should display a url that leads to a valid data source confirming you information. In the description you can specify where the data is in the source link (at least 20 characters).'}
                            eventClose={() => setShowModalSource(false)}
                            show={showModalSource}
                        /> : <></> }
                    <label className="labelNewProduct">Source Link *</label> 
                    <input className="form-control me-2"
                        id="inoutReasonFieldSoucrceLink" 
                        type="url" 
                        placeholder="Source Link"
                        aria-label="inoutReasonFieldSoucrceLink"
                        value={sourceLink}
                        onChange={(e) => setSourceLink(e.target.value)}
                        minLength={4}
                        required
                        ></input>
                    <label className="labelNewProduct">Source Description *</label> 
                    <textarea className="form-control me-2"
                        id="inoutReasonFieldDescrSource" 
                        placeholder="Source Description"
                        aria-label="inoutReasonFieldDescrSource"
                        value={sourceDescription}
                        onChange={(e) => setSourceDescription(e.target.value)}
                        minLength={10}
                        required
                        ></textarea>
                    <div className="buttonDivSignUp">
                        <input type="submit" value="Submit" className="formButtonGeneral" />
                    </div>
                </form>
            </div>
            { fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>                    
                : <></> }
            { props.skipHandle ?
            <>
                <div className="deviderDivAddFields"></div>
                <h2 className="addProductCategoryHeadline">Or skip this field: </h2>
                <Button
                    event={skipHandle}
                    bg={"#41475E"}
                    border={'transparaent'}
                    width={'250px'}
                    color={'white'}
                    margin={'10px 0 0 0'}
                >
                    Skip this field
                </Button> </> : <></>
            }
        </>
    )
};

export default ProductCard;