import {useState, useEffect } from 'react';
import Button from "../../../../BuildingBlocks/templateButton";
import Search from "../../../../BuildingBlocks/tempalteSearchBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import SelectOptions from "../../../../BuildingBlocks/templateSelectOptions";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import goTo from "../../../../../helper/goTo";
import { urlBE } from "../../../../../App";

import { NewProductProps } from "../../UI/FullComponents/newProduct";

const AddRawMaterials = (props: NewProductProps) => {
    const [fError, setFError] = useState<any>();
    const [existingCategory, setExistingCategory] = useState<any>();
    let [timesSend, setTimesSend] = useState<number>(0);
    const [searchParam, setSearchParam] = useState<string>('');
    const [searchResult, setSearchResult] = useState<any>();
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    const searchForCategory = async () => {
        fetch(`${urlBE}/api/o/search/name/${searchParam}/approved`, { 
            method: 'GET', 
            headers: {
                "Content-Type": "application/json",   
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setSearchResult(resData))
    }    

    const putExistingCategory = (e: any) => {

    fetch(`${urlBE}/api/product/update/relationship/rawmaterial/${props.productId}/${e}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                'authorization': 'Bearer ' + token,
                "X-Content-Type-Options": "nosniff"
                },
        }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((res) => {
            props.func();
            setTimesSend(1);
            setFError(undefined);
            })
            .catch(error => {
                setFError(error)
            }
            );
      }

    let skipHandle: any = () => {
        goTo('/product/' + props.productId)
    }

    return (
            <>
                {
                    timesSend === 0 ? <h2 className="addProductCategoryHeadline">Add raw materials of the product: </h2> 
                    : <h2 className="addProductCategoryHeadline">Add more raw materials of the product: </h2>
                }
                <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                    <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                    <p className="modalButtonPre">Get more information</p>
                </div>
                { showModalData ?
                <Modal 
                    info={'Search for an existing product that is a raw material of that very product.'}
                    eventClose={() => setShowModalData(false)}
                    show={showModalData}
                /> : <></> }
                    <label>Search for the product as raw material:</label>
                    <div className="categorySelectDivFlex">
                        <div>
                            <Search
                                event={searchForCategory}
                                bg='white'
                                fontColor='grey'
                                borderColor='grey'
                                border='grey'
                                icon=''
                                placeholder='search existing categories'
                                width='300px'
                                changeEvent={(e: any) => setSearchParam(e.target.value)}
                            >Search Raw Material</Search>
                            {
                                searchResult && !fError && searchResult.data.responseObj[0] !== undefined ? 
                                <div>
                                    <SelectOptions
                                        event={() => setExistingCategory(searchResult.data.responseObj.dataValues.id)}
                                        text={searchResult.data.responseObj.dataValues.name}
                                    />
                                    { existingCategory ? 
                                        <Button
                                            event={putExistingCategory}
                                            bg={"#41475E"}
                                            border={'transparaent'}
                                            width={'250px'}
                                            color={'white'}
                                            margin={'10px 0 0 0'}
                                        >Set as raw material</Button> 
                                        : <></>
                                    }
                                    
                                </div> 
                                : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                                : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                                : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>                    
                                :
                                searchResult && searchResult.data.responseObj[0] === undefined ?
                                <p>{searchParam} does not exist in our approved database.</p> :
                                <></>
                            }
                        </div>
                        <div className="deviderDivAddFields"></div>
                        <h2 className="addProductCategoryHeadline">Or skip this field: </h2>
                        <Button
                            event={skipHandle}
                            bg={"#41475E"}
                            border={'transparaent'}
                            width={'250px'}
                            color={'white'}
                            margin={'10px 0 0 0'}
                        >
                            Finish product creation
                        </Button>
                    </div>
                </>
    )
};

export default AddRawMaterials;