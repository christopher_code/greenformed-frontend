import {useState, useEffect} from 'react';
import Button from "../../../../BuildingBlocks/templateButton";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { NewProductProps } from "../../UI/FullComponents/newProduct";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";

const ProductCard = (props: NewProductProps) => {
    const [fError, setFError] = useState<any>();
    let [timesSend, setTimesSend] = useState<number>(0);
    const [singleGHG, setSingleGHG] = useState<number>(0);
    const [ghgName, setGHGName] = useState<string>('')
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [showModalSource, setShowModalSource] = useState<boolean>(false);
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    let skipHandle: any = () => {
        props.func();
    }

    const postAddNewSingleGHG = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/ghg/single/new/${userId}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"
                },
                body: JSON.stringify({
                    sourceTitle: 'Source of ' + ghgName + 'Emission (' + singleGHG + ')',
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    ghgName: ghgName,
                    emissionPerKG: singleGHG,
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then(() => {
                setSourceLink("")
                setSourceDescription("")
                setGHGName('')
                setSingleGHG(0)
                setTimesSend(1);
                props.func();
                setFError(undefined);
              })
              .catch(error => {
                setFError(error)
              }
            );
      }

    return (
        <>
            {
                timesSend === 0 ? <h2 className="newProductDescriptionStepSeller">Add a single Green House Gases Emission Data: </h2> 
                : <h2 className="addProductCategoryHeadline">Add another single Green House Gases Emission Data: </h2>
            }
            <div className="addProductCategoryAddDiv">
                <h2 className="addProductCategoryHeadline">Add new GHG Data: </h2>
                <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                    <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                    <p className="modalButtonPre">Get more information</p>
                </div>
                { showModalData ?
                <Modal 
                    info={'Add the amount of a specific Green House Gas that is emitted for 1KG of the product to be sold. This includes all processes they are emitted. Please add the Greenhous Gas`s name and the amount in kg.'}
                    eventClose={() => setShowModalData(false)}
                    show={showModalData}
                /> : <></> }
                <form onSubmit={postAddNewSingleGHG}>
                    <label className="labelNewProduct">GHG Name *</label> 
                    <input className="form-control me-2"
                        id="inputGHGSingleName" 
                        type="text" 
                        placeholder="GHG Name"
                        aria-label="inputGHGSingleName"
                        value={ghgName}
                        onChange={(e) => setGHGName(e.target.value)}
                        minLength={2}
                        maxLength={30}
                        required
                        ></input>
                    <label className="labelNewProduct">Amout of GHG in KG per 1 KG of product *</label> 
                    <input className="form-control me-2"
                        id="inputGHGSingleKG" 
                        type="number" 
                        placeholder="0"
                        aria-label="inputGHGSingleKG"
                        value={singleGHG}
                        onChange={(e: any) => setSingleGHG(+e.target.value)}
                        ></input>
                    <h2 className="newProductDescriptionStepSeller">In order to save the data you need to proof that it is valid. Therefore, a source needs to be added.</h2>
                    <div className="modalDivRow" onClick={() => setShowModalSource(true)}>
                        <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                        <p className="modalButtonPre">Get more information</p>
                    </div>
                    { showModalSource ?
                        <Modal 
                            info={'The source`s link should display a url that leads to a valid data source confirming you information. In the description you can specify where the data is in the source link (at least 20 characters).'}
                            eventClose={() => setShowModalSource(false)}
                            show={showModalSource}
                        /> : <></> }
                    <label className="labelNewProduct">Source Description *</label> 
                    <textarea className="form-control me-2"
                        id="inputGHGSingleSourceDesc" 
                        placeholder="Source Description"
                        aria-label="inputGHGSingleSourceDesc"
                        value={sourceDescription}
                        onChange={(e) => setSourceDescription(e.target.value)}
                        minLength={20}
                        required
                        ></textarea>
                    <label className="labelNewProduct">Add a Source Link that backs your description *</label> 
                    <input className="form-control me-2"
                        id="inputGHGSingleSourceLink" 
                        type="url" 
                        placeholder="Source Link"
                        aria-label="inputGHGSingleSourceLink"
                        value={sourceLink}
                        onChange={(e) => setSourceLink(e.target.value)}
                        minLength={3}
                        required
                        ></input>
                    <div className="buttonDivSignUp">
                        <input type="submit" value="Submit" className="formButtonGeneral" />
                    </div>
                </form>
            </div>
            { fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
                : <></>}
            { props.skipHandle ?
             <>
                <div className="deviderDivAddFields"></div>
                <h2 className="addProductCategoryHeadline">Or skip this field: </h2>
                <Button
                        event={skipHandle}
                        bg={"#41475E"}
                        border={'transparaent'}
                        width={'250px'}
                        color={'white'}
                        margin={'10px 0 0 0'}
                    >
                        Skip this field
                </Button> </> : <></>
            }
        </>
    )
};

export default ProductCard;