import {useState, useEffect} from 'react';
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { NewProductProps } from "../../UI/FullComponents/newProduct";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";

const MainNewProd = (props: NewProductProps) => {
    const [fError, setFError] = useState<any>();
    const [name, setName] = useState<string>('');
    const [linkToBuy, setLinkToBuy] = useState<string>('');
    const [costPerProduct, setCostPerProduct] = useState<number>(0);
    const [description, setDescription] = useState<string>('');
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [isSet, setIsSet] = useState<boolean>(false);
    const [showModalSource, setShowModalSource] = useState<boolean>(false);
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    const postNewProduct = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/product/new/${userId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"
                },
                body: JSON.stringify({
                    sourceTitle: "Source of " + name + "as a product",
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    name: name,
                    description: description,
                    linkToBuy: linkToBuy,
                    costPerProduct: costPerProduct,
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then((res) => {
                props.setIdFunc(res.data.id);
                props.func();
                setIsSet(true);
                setFError(undefined);
              })
              .catch(error => {
                setIsSet(false);
                setFError(error);
              }
            );
      }

    return (
        <>
        {
            isSet === false ?
            <>
            <h2 className="newProductDescriptionStep">Add the main information of the product before you add any Green House Gas related data.</h2>
            <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                <p className="modalButtonPre">Get more information</p>
            </div>
            { showModalData ?
            <Modal 
                info={'The product name displays the name of the product (at least 3 characters). The link to buy is the url that leads to the online shop where you can buy the product (min 6 characters). The description gives a short summery of the product (at least 20 characters). The cost per unit is the price of the product per unit.'}
                eventClose={() => setShowModalData(false)}
                show={showModalData}
            /> : <></> }
            <div className="newProductTopDiv">
                    <div className="newProductNameDiv">
                        <div className="newProductDescriptionDiv">
                            <label>Product Name * </label> 
                            <input className="form-control me-2"
                                id="inputMainProduName" 
                                type="text" 
                                placeholder="Product Name"
                                aria-label="inputMainProduName"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                minLength={2}
                                required
                                ></input>
                        </div>
                        <div className="newProductDescriptionDiv">
                            <label>Link to buy</label> 
                            <input className="form-control me-2"
                                id="inputMainProduLinkToBuy" 
                                type="url" 
                                placeholder="Product Link"
                                aria-label="inputMainProduLinkToBuy"
                                value={linkToBuy}
                                onChange={(e) => setLinkToBuy(e.target.value)}
                                minLength={4}
                                required
                                ></input>
                        </div>
                        <div className="newProductDescriptionDiv">
                            <label>Description *</label> 
                            <textarea className="form-control me-2"
                                id="inputMainProduDesc" 
                                placeholder="Product Description"
                                aria-label="inputMainProduDesc"
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                                minLength={20}
                                required
                                ></textarea>
                        </div>
                        <div className="newProductDescriptionDiv">
                                <label>Cost Per Product in EUR *</label> 
                                <input className="form-control me-2"
                                    id="inputMainProduCost" 
                                    type="number" 
                                    pattern='[0-9]{0,5}'
                                    aria-label="inputMainProduCost"
                                    value={costPerProduct}
                                    onChange={(e: any) => setCostPerProduct(+e.target.value)}
                                    min={0}
                                    required
                                    ></input>
                        </div>
                </div>
            </div>
            <form onSubmit={postNewProduct}>
                <div className="sourceDivNewProduct">
                    <div className="sourceDivInputDiv">
                        <h2 className="newProductDescriptionStep">In order to save the data you need to proof that it is valid. Therefore, a source needs to be added.</h2>
                        <div className="modalDivRow" onClick={() => setShowModalSource(true)}>
                            <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                            <p className="modalButtonPre">Get more information</p>
                        </div>
                        { showModalSource ?
                            <Modal 
                                info={'The source`s link should display a url that leads to a valid data source confirming you information. In the description you can specify where the data is in the source link (at least 20 characters).'}
                                eventClose={() => setShowModalSource(false)}
                                show={showModalSource}
                            /> : <></> }
                        <div className="newProductDescriptionDiv">
                                <label>Source Link *</label> 
                                <input className="form-control me-2"
                                    id="inputMainProduSourceLinj" 
                                    type="text" 
                                    placeholder="Source Link"
                                    aria-label="inputMainProduSourceLinj"
                                    value={sourceLink}
                                    onChange={(e) => setSourceLink(e.target.value)}
                                    minLength={4}
                                    required
                                    ></input>
                        </div>
                        <div className="newProductDescriptionDiv">
                                <label>Source Description *</label> 
                                <textarea className="form-control me-2"
                                    id="inputMainProduSourceDesc" 
                                    placeholder="Description"
                                    aria-label="inputMainProduSourceDesc"
                                    value={sourceDescription}
                                    onChange={(e) => setSourceDescription(e.target.value)}
                                    minLength={20}
                                    required
                                    ></textarea>
                        </div>
                    </div>
                </div>
                <div className="buttonDivSignUp">
                    <input type="submit" value="Submit" className="formButtonGeneral" />
                </div>
            </form>
            </>     
            : 
            <div>
                <h3>{name} was successfully created!</h3>
                <p>Description: {description}</p>
                <p>{name} can be bought from: </p>
                <a href={linkToBuy} target="_blank" rel="noreferrer">{linkToBuy}</a>
                <p>{name} costs {costPerProduct} EUR.</p>
            </div>
        }
        {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>
            : <></>      }
        </>
    )
};

export default MainNewProd;
