import {useState} from 'react';
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import Search from "../../../../BuildingBlocks/tempalteSearchBar";
import goTo from "../../../../../helper/goTo";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";

const SetSearch = () => {
    let [search, setSearch] = useState<any>();
    const [showModalData, setShowModalData] = useState<boolean>(false);

    return (
        <MainCard
            marginTop={'0'}
            marginBottom={'20px'}
        >
            <h1 className='headlineSearchPage'>Search any data by name</h1>
            <p>Search for any product that you are interested in or add the data if it does not exist. Get approved and other Greenhouse Gas data from the Greenformed platform.</p>
            <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                <p className="modalButtonPre">Get more information</p>
            </div>
                    { showModalData ?
                    <Modal 
                        info={'Use the search field to search any data in the Greenformed database.'}
                        eventClose={() => setShowModalData(false)}
                        show={showModalData}
                    /> : <></> }
            <Search 
                event={() => goTo('/search/' + search + "/notapproved")} 
                bg={"#0B1B04"}
                border={"#1E3D0E"}
                icon={"red"}
                placeholder={"Search"}
                fontColor={"white"}
                borderColor={"#1E3D0E"}
                width="300px"
                changeEvent={(e: any) => setSearch(e.target.value)}
            >
            Search Data
            </Search>
        </MainCard>
    )
};

export default SetSearch;