import {useState, useEffect } from 'react';
import { urlBE } from "../../../../../App";
import Button from "../../../../BuildingBlocks/templateButton";
import Search from "../../../../BuildingBlocks/tempalteSearchBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { NewProductProps } from "../../UI/FullComponents/newProduct";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";

const AddAlternative = (props: NewProductProps) => {
    const [fError, setFError] = useState<any>();
    let [timesSend, setTimesSend] = useState<number>(0);
    const [searchParam, setSearchParam] = useState<string>('');
    const [searchResult, setSearchResult] = useState<any>();
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    let skipHandle: any = () => {
        props.func();
    }

    const loadLocalStorage = async () => {
        let data = localStorage.getItem('token');
        if (data) {setToken(data)}
    }   

    const searchForAlternative = async () => {
        fetch(`${urlBE}/api/o/search/name/${searchParam}/approved`, { 
            method: 'GET', 
            headers: {
                "Content-Type": "application/json",  
                "X-Content-Type-Options": "nosniff" 
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setSearchResult(resData))
            .then(resp => setSearchParam(''))
    }    

    const putExistingCategory = (e: any) => {

    fetch(`${urlBE}/api/product/update/relationship/alternative/${props.productId}/${e}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                'authorization': 'Bearer ' + token,
                "X-Content-Type-Options": "nosniff"
                },
        }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(() => {
            setTimesSend(1);
            props.func();
            setFError(undefined);
            })
            .catch(error => {
                setFError(error);
              }
            );
      }

    return (
            <>
                {
                    timesSend === 0 ? <h2 className="addProductCategoryHeadline">Add an alternative Product:</h2> 
                    : <h2 className="addProductCategoryHeadline">Add another alternative product: </h2>
                }
                <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                    <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                    <p className="modalButtonPre">Get more information</p>
                </div>
                { showModalData ?
                <Modal 
                    info={'Search if an approved alternative exists. If it exists you can add it as an alternative for that very product.'}
                    eventClose={() => setShowModalData(false)}
                    show={showModalData}
                /> : <></> }
                    <label>The alternative is:</label>
                    <div className="categorySelectDivFlex">
                        <div>
                            <Search
                                event={searchForAlternative}
                                bg='white'
                                fontColor='grey'
                                borderColor='grey'
                                border='grey'
                                icon=''
                                placeholder='search existing categories'
                                width='300px'
                                changeEvent={(e: any) => setSearchParam(e.target.value)}
                            >Search</Search>
                            {
                                searchResult && searchResult.data.responseObj[0] !== undefined ? 
                                <div>
                                    <>
                                        <p>We found {searchResult.data.responseObj.dataValues.name}.</p>
                                        <Button
                                            event={() => putExistingCategory(searchResult.data.responseObj.dataValues.id)}
                                            bg={"#41475E"}
                                            border={'transparaent'}
                                            width={'250px'}
                                            color={'white'}
                                            margin={'10px 0 0 0'}
                                        >Set as alternative</Button> 
                                    </> : <></>
                                </div> 
                                :
                                searchResult && searchResult.data.responseObj[0] === undefined ?
                                <p>{searchParam} does not exist in our approved database.</p> :
                                <></>
                            }
                        </div>
                    </div>
                    {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                        : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>                    
                        :<></>}
                    { props.skipHandle ?
                    <>
                    <div className="deviderDivAddFields"></div>
                    <h2 className="addProductCategoryHeadline">Or skip this field: </h2>
                        <Button
                            event={skipHandle}
                            bg={"#41475E"}
                            border={'transparaent'}
                            width={'250px'}
                            color={'white'}
                            margin={'10px 0 0 0'}
                        >
                            Skip this field
                        </Button> </> : <></>
                    }
                </> 
    )
};

export default AddAlternative;