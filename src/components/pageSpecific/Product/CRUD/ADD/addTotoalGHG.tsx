import {useState, useEffect} from 'react';
import Button from "../../../../BuildingBlocks/templateButton";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { NewProductProps } from "../../UI/FullComponents/newProduct";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import { urlBE } from "../../../../../App";

const ProductCard = (props: NewProductProps) => {
    const [fError, setFError] = useState<any>();
    const [totalGHG, setTotalGHG] = useState<number>(0);
    const [sourceLink, setSourceLink] = useState<string>('');
    const [sourceDescription, setSourceDescription] = useState<string>('');
    const [isSet, setIsSet] = useState<boolean>(false);
    const [showModalSource, setShowModalSource] = useState<boolean>(false);
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    let skipHandle: any = () => {
        props.func();
    }

    const postAddNewTotoalGHG = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/ghg/total/new/${userId}/${props.productId}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"
                },
                body: JSON.stringify({
                    sourceTitle: 'Source of total Emission in kg: ' + totalGHG,
                    sourceLink: sourceLink,
                    sourceDescription: sourceDescription,
                    ghgPerKG: totalGHG,
                })
            }).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
              .then(() => {
                setSourceLink("")
                setSourceDescription("")
                props.func();
                setIsSet(true);
                setFError(undefined);
              })
              .catch(error => {
                setIsSet(false);
                setFError(error);
              }
            );
      }

    return (
        <>
        {
            isSet === false ?
            <>
            <h2 className="addProductCategoryHeadline">Add the Total amount og Green House Gases emitted through this product: </h2>
            <div className="addProductCategoryAddDiv">
                <h2 className="addProductCategoryHeadline">Add new GHG Data: </h2>
                <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                    <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                    <p className="modalButtonPre">Get more information</p>
                </div>
                { showModalData ?
                <Modal 
                    info={'Add the amount of Greenhouse Gases that is emitted for 1KG of the product to be sold. This includes all Greenhouse Gases and all processes they are emitted.'}
                    eventClose={() => setShowModalData(false)}
                    show={showModalData}
                /> : <></> }
                <form onSubmit={postAddNewTotoalGHG}>
                    <label>Total GHG *</label> 
                    <input className="form-control me-2"
                        id="inputAddTotalNumber" 
                        type="number" 
                        pattern='[0-9]{0,5}'
                        aria-label="inputAddTotalNumber"
                        value={totalGHG}
                        onChange={(e: any) => setTotalGHG(+e.target.value)}
                        required
                        ></input>
                    <h2 className="newProductDescriptionStepSeller">In order to save the data you need to proof that it is valid. Therefore, a source needs to be added.</h2>
                    <div className="modalDivRow" onClick={() => setShowModalSource(true)}>
                        <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                        <p className="modalButtonPre">Get more information</p>
                    </div>
                    { showModalSource ?
                        <Modal 
                            info={'The source`s link should display a url that leads to a valid data source confirming you information. In the description you can specify where the data is in the source link (at least 20 characters).'}
                            eventClose={() => setShowModalSource(false)}
                            show={showModalSource}
                        /> : <></> }
                    <label className="labelNewProduct">Source Description *</label> 
                    <textarea className="form-control me-2"
                        id="inputAddTotalSourceDescription" 
                        placeholder="Source Description"
                        aria-label="inputAddTotalSourceDescription"
                        value={sourceDescription}
                        onChange={(e) => setSourceDescription(e.target.value)}
                        minLength={20}
                        required
                        ></textarea>
                    <label className="labelNewProduct">Add a Source Link that backs your description *</label> 
                    <input className="form-control mbe-2"
                        id="inputAddTotalSourceLink" 
                        type="url" 
                        placeholder="Source Link"
                        aria-label="inputAddTotalSourceLink"
                        value={sourceLink}
                        onChange={(e) => setSourceLink(e.target.value)}
                        minLength={4}
                        required
                        ></input>
                    <div className="buttonDivSignUp">
                        <input type="submit" value="Submit" className="formButtonGeneral" />
                    </div>
                </form>
            </div>
            { fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
                : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >SSomething went wrong - please also check your connection</MessageCard>           
                : <></>}
            { props.skipHandle ?
             <>
                <div className="deviderDivAddFields"></div>
                <h2 className="addProductCategoryHeadline">Or skip this field: </h2>
                <Button
                    event={skipHandle}
                    bg={"#41475E"}
                    border={'transparaent'}
                    width={'250px'}
                    color={'white'}
                    margin={'10px 0 0 0'}
                >
                    Skip this field
                </Button> </> : <></>
            }
            </>
            : <p>The total Emission of Green House Gases is set to {totalGHG} per KG produced.</p>
            }
        </>
    )
};

export default ProductCard;