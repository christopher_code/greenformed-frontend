import {useState, useEffect} from 'react';
import { useParams } from "react-router-dom";
import MainBar from "../../../../BuildingBlocks/templateMainBar";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import CategorySelection from "./addCategory";
import SellerSelection from './addSeller';
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import TotalGHG from "./addTotoalGHG";
import SingleGHG from "./addSingleGHG";
import Reason from "./addReason";
import Alternative from "./addAlternative";
import RawMaterial from "./addRawMaterial";
import goTo from '../../../../../helper/goTo';
import { urlBE } from "../../../../../App";

const AddToExsitingProduct = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(urlBE + '/api/o/product/get/core/' + id.productId, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    } 

    let typeAdd: string = '';
    if (id.type === 'category') {typeAdd = 'a category'}
    else if (id.type === 'seller') {typeAdd = 'a seller'}
    else if (id.type === 'total') {typeAdd = 'the total Emission of GHG in KG'}
    else if (id.type === 'single') {typeAdd = 'a single GHG Emission in KG'}
    else if (id.type === 'reason') {typeAdd = 'a reason of GHG Emission'}
    else if (id.type === 'alternative') {typeAdd = 'an alternative product'}
    else if (id.type === 'rawmaterial') {typeAdd = 'a raw material'}

    return (

        <>
        { !fError && fData ?
            <>
            <MainBar
                marginTop={'0'}
                marginBottom={'20px'}
            >
                <div className="topBarFlex">
                    <div>
                        { fData ? <h1 className="topBarHeadline">Add {typeAdd} to {fData.data.product.name}.</h1> : <></> }
                        <h3 className="topBarSubheadline">The data you add to Greenformed is being processed to be fully approved. All data that comes into our system is directly forwarded into the approval mechansim. The approval mechanism is build to make sure every data in the system is accurate. At least two users need to give their approval to verify any data in the system. For denying data at least two users are needed to deny the data, as well.</h3>
                    </div>
                </div>
            </MainBar>

            {
                id.type === 'category' ?
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <CategorySelection
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></CategorySelection>
                </MainBar>
                : <></>
            }

            {
                id.type === 'seller' ?
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <SellerSelection
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></SellerSelection>
                </MainBar>
                : <></>
            }

            {
                id.type === 'total' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <TotalGHG
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></TotalGHG>
                </MainBar>
                : <></>
            }

            {
                id.type === 'single' ?
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <SingleGHG
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></SingleGHG>
                </MainBar>
                : <></>
            }

            {
                id.type === 'reason' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <Reason
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></Reason>
                </MainBar> 
                : <></>
            } 

            {
                id.type === 'alternative' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <Alternative
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></Alternative>
                </MainBar> 
                : <></>
            }

            {
                id.type === 'rawmaterial' ? 
                <MainBar marginTop={'20px'} marginBottom={'0'}>
                    <RawMaterial
                        func={() => goTo('/product/' + id.productId)}
                        productId={id.productId}
                        setIdFunc={''}
                        skipHandle={false}
                    ></RawMaterial>
                </MainBar> 
                : <></>
            }
            </>
        : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
        :
        <div className="skeletonWidth">
            <MainBar marginTop={'20px'} marginBottom={'0'}>
                <SkeletonTemplate numberOfLines={6} /> 
            </MainBar>
            <MainBar marginTop={'20px'} marginBottom={'0'}>
                <SkeletonTemplate numberOfLines={6} /> 
            </MainBar>
            <MainBar marginTop={'20px'} marginBottom={'0'}>
                <SkeletonTemplate numberOfLines={6} /> 
            </MainBar>
            </div>
        }
        </>
    )
};

export default AddToExsitingProduct;
