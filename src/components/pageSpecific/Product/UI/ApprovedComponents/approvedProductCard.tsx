import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Button from "../../../../BuildingBlocks/templateButton";
import converteDate from "../../../../../helper/convertDate";

type ApprovedProductCardProps = {
    productName: string, 
    description: string, 
    linkToBuy: string,
    costPerProduct: number,
    createdAt: number,
    transferredAt: number,
    lastUpdateAt: number,
    getMoreEvent: any,
    getFullDataEvent: any,
};

const ApprovedProductCard = (props: ApprovedProductCardProps) => {
    let createdAt: string[] = converteDate(props.createdAt);
    let transfered: string[] = converteDate(props.transferredAt);
    let update: string[] = converteDate(props.transferredAt);
    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="approvedProductWidth">
                <div className="approvedProductContainer">
                    <div className="approvedProductMainInfoDiv">
                        <h3 className="productNameLong">{props.productName}</h3>
                        <p>Approval Status: fully Approved</p>
                        <p>Cost per Unit: {props.costPerProduct} EUR</p>
                        <p>Link to buy: <a href={props.linkToBuy} target="_blank" rel="noreferrer">Click here</a></p>
                    </div>
                    <div className="deviderProductInfo"></div>
                    <div className="approvedProductDescDiv">
                        <h4 className="descriptionApprovedProd">Description: {props.description}</h4>
                    </div>
                    <div className="approvedProductDivButtons">
                        <div>
                            <p className="addedByProductcard">Creation Date: {createdAt[0]} {createdAt[1]}</p>
                            <p className="addedByProductcard">Approval Date: {transfered[0]} {transfered[1]}</p>
                            <p className="addedByProductcard">Latest Update Date: {update[0]} {update[1]}</p>
                        </div>
                        <Button 
                            event={props.getFullDataEvent}
                            bg={"#A3A9C3"}
                            border={'transparent'}
                            width={"250px"}
                            color={"#36354A"}
                            margin={"10px 10px 0 0"}
                        >
                            Get Full Data
                        </Button>  
                        <Button 
                            event={props.getMoreEvent}
                            bg={"#A3A9C3"}
                            border={'transparent'}
                            width={"250px"}
                            color={"#36354A"}
                            margin={"10px 10px 0 0"}
                        >
                            Approved Data
                        </Button> 
                    </div>
                </div>
            </div>
        </MainBar>
    )
};

export default ApprovedProductCard;
