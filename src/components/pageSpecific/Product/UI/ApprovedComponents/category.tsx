import MainBar from "../../../../BuildingBlocks/templateMainBar";

type ApprovedCategoryProps = {
    productName: string
    typeOfCategory: string;
};

const ApprovedCategory = (props: ApprovedCategoryProps) => {
    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div>
                <h2 className="productNameLong">{props.productName} belongs to this category: {props.typeOfCategory}</h2>
            </div>
        </MainBar>
    )
};

export default ApprovedCategory;
