import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Button from "../../../../BuildingBlocks/templateButton";
import goTo from "../../../../../helper/goTo";
import convertDate from "../../../../../helper/convertDate";
import Tag from "../../../../BuildingBlocks/templateTag";

type ApprovedProductMainInfoProps = {
    productId: string,
    productName: string,  
    description: string, 
    link: string,
    costPerProduct: number,
    createdAt: number,
    transferredFristAt: number,
    lastUpdateAt: number,
};

const ApprovedProductMainInfo = (props: ApprovedProductMainInfoProps) => {
    let createdAt: string[] = convertDate(props.createdAt);
    let transfered: string[] = convertDate(props.transferredFristAt);
    let update: string[] = convertDate(props.transferredFristAt);

    function handleGoWindow(link: string) {
        if (link.includes('https://') || link.includes('http://')) {
            window.open(link)
        } else {
            window.open('https://' + link)
        }
    }

    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className='approvedDataMainContainer'>
                <div className='approvedDataMainDivInfo'>
                    <h2 className='productNameLong'>Product: {props.productName}</h2>
                    <Tag bg={"green"} width={'120px'} height={'35px'} color={"white"} fontSize={"10px"}>Fully approved</Tag>
                    <p className='approvedDataMainText'>All information that you see are fully approved.</p>
                    <p className='approvedDataMainText'>Description: {props.description}</p>
                    <p className='approvedDataMainText'>Cost per unit: {props.costPerProduct} EUR</p>
                </div>
                <div className='approvedDataMainButtonDiv'>
                    <Button
                        event={() => goTo('/product/' + props.productId)}
                        bg={"#A3A9C3"}
                        border={'transparent'}
                        width={'250px'}
                        color={'white'}
                        margin={'10px'}
                    >Full product</Button>
                    <Button
                        event={() => handleGoWindow(props.link)}
                        bg={"#A3A9C3"}
                        border={'transparent'}
                        width={'250px'}
                        color={'white'}
                        margin={'10px'}
                    >Buy Product</Button>
                </div>
                <div className='approvedDataMainDivUnder'>
                    <div>
                        <p className="addedByProductcard">Created Originally at: {createdAt[0]} {createdAt[1]}</p>
                        <p className="addedByProductcard">Approved at: {transfered[0]} {transfered[1]}</p>
                        <p className="addedByProductcard">Last Update at: {update[0]} {update[1]}</p>
                    </div>
                    <div>
                        <Button
                            event={() => goTo('/change/history/product/' + props.productId)}
                            bg={"#A3A9C3"}
                            border={'white'}
                            width={'250px'}
                            color={'white'}
                            margin={'0'}
                        >Change history</Button>
                    </div>
                </div>
            </div>
        </MainBar>
    )
};

export default ApprovedProductMainInfo;
