import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Button from "../../../../BuildingBlocks/templateButton";
import goTo from "../../../../../helper/goTo";
import convertDate from "../../../../../helper/convertDate";
import Tag from "../../../../BuildingBlocks/templateTag";

type ApprovedSellerProps = {
    productName: string, 
    sellerId: string 
    sellerName: string,
    sellerLink:string,
    createdAt: number,
    transferredFristAt: number,
    lastUpdateAt: number,
};

const ApprovedSeller = (props: ApprovedSellerProps) => {
    let createdAt: string[] = convertDate(props.createdAt);
    let transfered: string[] = convertDate(props.transferredFristAt);
    let update: string[] = convertDate(props.transferredFristAt);

    function handleGoWindow(link: string) {
        if (link.includes('https://') || link.includes('http://')) {
            window.open(link)
        } else {
            window.open('https://' + link)
        }
    }

    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className='approvedDataMainContainer'>
                <div className='approvedDataMainDivInfo'>
                    <h2 className='productNameLong'>{props.productName} can be bought from: {props.sellerLink}</h2>
                    <Tag bg={"green"} width={'120px'} height={'35px'} color={"white"} fontSize={"10px"}>Fully approved</Tag>
                    <p>All information that you see are fully approved.</p>
                </div>
                <div>
                <Button
                    event={() => handleGoWindow(props.sellerLink)}
                    bg={"white"}
                    border={'blue'}
                    width={'250px'}
                    color={'black'}
                    margin={'0'}
                >Seller Link</Button>
                </div>
                <div className='approvedDataMainDivUnder'>
                    <div>
                        <p className="addedByProductcard">Created Originally at: {createdAt[0]} {createdAt[1]}</p>
                        <p className="addedByProductcard">Approved at: {transfered[0]} {transfered[1]}</p>
                        <p className="addedByProductcard">Last Update at: {update[0]} {update[1]}</p>
                    </div>
                    <div>
                        <Button
                            event={() => goTo('/change/history/seller/' + props.sellerId)}
                            bg={"white"}
                            border={'blue'}
                            width={'250px'}
                            color={'black'}
                            margin={'0'}
                        >See change history</Button>
                    </div>
                </div>
            </div>
        </MainBar>
    )
};

export default ApprovedSeller;
