import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Button from "../../../../BuildingBlocks/templateButton";
import goTo from "../../../../../helper/goTo";
import convertDate from "../../../../../helper/convertDate";
import Tag from "../../../../BuildingBlocks/templateTag";

type TotalEmissionProps = {
    productName: string, 
    id: string,
    ghgPerKG: number,
    createdAt: number,
    transferredFristAt: number,
    lastUpdateAt: number,
};

const TotalEmission = (props: TotalEmissionProps) => {
    let createdAt: string[] = convertDate(props.createdAt);
    let transfered: string[] = convertDate(props.transferredFristAt);
    let update: string[] = convertDate(props.transferredFristAt);

    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className='approvedDataMainContainer'>
                <div className='approvedDataMainDivInfo'>
                    <h2 className='productNameLong'>{props.productName} emmits {props.ghgPerKG} kg of GHG per 1 kg sold.</h2>
                    <Tag bg={"green"} width={'120px'} height={'35px'} color={"white"} fontSize={"10px"}>Fully approved</Tag>
                    <p>All information that you see are fully approved.</p>
                </div>
                <div className='approvedDataMainDivUnder'>
                    <div>
                        <p className="addedByProductcard">Created Originally at: {createdAt[0]} {createdAt[1]}</p>
                        <p className="addedByProductcard">Approved at: {transfered[0]} {transfered[1]}</p>
                        <p className="addedByProductcard">Last Update at: {update[0]} {update[1]}</p>
                    </div>
                    <div>
                        <Button
                            event={() => goTo('/change/history/total/' + props.id)}
                            bg={"white"}
                            border={'blue'}
                            width={'250px'}
                            color={'black'}
                            margin={'0'}
                        >See change history</Button>
                    </div>
                </div>
            </div>
        </MainBar>
    )
};

export default TotalEmission;