import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Tag from "../../../../BuildingBlocks/templateTag";
import Button from "../../../../BuildingBlocks/templateButton";

import convertDate from "../../../../../helper/convertDate";

import arrow from "../../../../../assets/Arrow.svg";
import pie from "../../../../../assets/pie-chart.png";
import devide from "../../../../../assets/alternate.png";

type ProductCardProps = {
    productId: string,
    productName: string, 
    totalGHG: number, 
    ghgSingle1: string,
    ghgSingle2: string,
    ghgSingle3: string,
    ghgSingle4: string,
    ghgReason1: string,
    ghgReason2: string,
    ghgReason3: string,
    category: string, 
    description: string, 
    getMoreEvent: any,
    editInfoEvent: any,
    originallyAddedBy: string,  
    status: string,
    date: number,
};

const ProductCard = (props: ProductCardProps) => {

    let date: string[] = convertDate(props.date);

    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="productDiv">
                <div className="productMainInfoDiv">
                    <h3 className="productName">{props.productName}</h3>
                    <p className="infoP">Category: {props.category}</p>
                    <div className="tagDivProductCard">
                        {
                            props.status === 'newSource' ? <Tag bg={"#FAFCB9"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Not approved yet</Tag>
                            : props.status === 'approvedFirst' || props.status === 'approvedByNewUser' ? <Tag bg={"#C2FCB9"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Approved Once</Tag>
                            : props.status === 'notApprovedOnce' || props.status === 'notApprovedNewUser' ? <Tag bg={"#F5C0C0"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Not approved once</Tag>
                            : props.status === 'approvedFinal' ? <Tag bg={"green"} width={'120px'} height={'20px'} color={"white"} fontSize={"10px"}>Fully approved</Tag>
                            : props.status === 'notApprovedAgain' ? <Tag bg={"red"} width={'120px'} height={'20px'} color={"white"} fontSize={"10px"}>Not approved</Tag>
                            : <Tag bg={"#F5C0C0"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Undefined Status</Tag>
                        }
                        {
                            props.totalGHG < 0 ? <Tag bg={"green"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>GHG Negative Product</Tag>
                            : props.totalGHG === 0 ? <Tag bg={"yellow"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>GHG Neutral Product</Tag>
                            : <Tag bg={"#F3C3C3"} width={'120px'} height={'20px'} color={"#571529"} fontSize={"10px"}>GHG High Product</Tag>
                        }
                    </div>
                    <div className="divButtonMoreTop">
                        <Button 
                            event={props.getMoreEvent}
                            bg={"#A3A9C3"}
                            border={'transparent'}
                            width={"250px"}
                            color={"#36354A"}
                            margin={"10px 0 0 0"}
                        >
                            More Information
                        </Button>
                    </div>
                </div>
                <div className="deviderProductInfo"></div>
                <div className="productGhgMainDiv">
                    <div className="productGhgDataTop">
                        <div className='totalGhgDiv'>
                            <h2 className="productGHGDAtaHeadlineTop">1 kg sold of {props.productName} produces {props.totalGHG} kg of GHGs.</h2>
                            <div className="arrowProductDiv">
                                <img src={arrow} alt={"Emission data of " + props.productName} className='arrowProductEmissionImg' />
                                <p>Reasons of Emissions: {props.ghgReason1}</p>
                            </div>
                        </div>
                    </div>
                    <div className="productGhgDataMiddle">
                        <h3 className="productGHGDAtaHeadlineTop">Read the Green House Gases seperated</h3>
                        <div className="arrowProductDiv">
                            <img src={pie} alt={props.productName + "emits" + props.totalGHG + "KG of Green House Data"} className="imgProductInfoMiddleBottom" />
                            <p>single GHGs: {props.ghgSingle1}</p>
                        </div>
                    </div>
                    <div className="productGhgDataBottom">
                        <div className="productAlternativeProductsDiv">
                            <h3 className="productGHGDAtaHeadlineTop">Alternative Products</h3>
                            <div className="arrowProductDiv">
                                <img src={devide} alt={"Green House Gas Free Product Alternative for products that emit Green House Gases."} className="imgProductInfoMiddleBottom" />
                                <p>Discover the alternative products of {props.productName}</p>
                            </div>
                        </div>
                    </div>
                    <h4 className="productDescription">{props.description}</h4>
                </div>
                <div className="divEditAndAddedByProductCard">
                    <div className="ProductEditDiv">
                            <Button 
                                event={props.editInfoEvent}
                                bg={"#A3A9C3"}
                                border={'transparent'}
                                width={"200px"}
                                color={"#36354A"}
                                margin={"10px 0 0 0"}
                            >
                                Edit Information
                            </Button>   
                    </div>
                    <div className="addedByDivCard">
                        <p className="addedByProductcard">Originally added by {props.originallyAddedBy}</p> 
                        <p className="addedByProductcard">Originally added at {date[0]}</p>
                        <p className="addedByProductcard">Time: {date[1]}</p>
                    </div>
                    <div className="divButtonMoreBottom">
                        <Button 
                            event={props.getMoreEvent}
                            bg={"#A3A9C3"}
                            border={'transparent'}
                            width={"250px"}
                            color={"#36354A"}
                            margin={"10px 0 0 0"}
                        >
                            More Information
                        </Button>
                    </div>
                </div>
            </div>
        </MainBar>
    )
};

export default ProductCard;
