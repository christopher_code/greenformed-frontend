import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Button from "../../../../BuildingBlocks/templateButton";
import goTo from "../../../../../helper/goTo";
import converteDate from "../../../../../helper/convertDate";

type SearchResultProps = {
    productId: string,
    productName: string, 
    description: string,
    approvalStatus: string,
    createdAt: number,
    searchs: string,
};

const SearchResult = (props: SearchResultProps) => {
    let date: string[] = converteDate(props.createdAt)
    return (
        <>
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        > 
            <h1 className="searchHead1">Here is your search result for your search on: {props.searchs}</h1>
        </MainBar>
        <MainBar
            marginTop={"0"}
            marginBottom={'-20px'}
        >
            <h2 className='searchresultHeadine'>We found the following product: {props.productName}</h2> 
            <p className='searchresultDescr'>Description: {props.description}</p>
            <p className='searchresultDescr'>Approval Status: {props.approvalStatus}</p>
            <p className='searchresultDescr'>Created At: {date[0]} {date[1]}</p>
            <Button
                event={() => goTo('/product/' + props.productId)}
                bg={"#41475E"}
                border={'transparent'}
                width={'300px'}
                color={'white'}
                margin={'0'}
            >All information</Button>
            {
                props.approvalStatus === 'approvedFinal' ?
                <Button
                    event={() => goTo('/approved/product/' + props.productId)}
                    bg={"#41475E"}
                    border={'transparent'}
                    width={'250px'}
                    color={'white'}
                    margin={'0'}
                >See approved data</Button> : <></>
            }
        </MainBar>
        </>
    )
};

export default SearchResult;
