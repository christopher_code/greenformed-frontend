import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Tag from "../../../../BuildingBlocks/templateTag";
import SimpleEdit from "../../../../BuildingBlocks/templateSimpleEdit";
import SourceAndEdit from "../../../../BuildingBlocks/templateSourceEdit";
import Button from "../../../../BuildingBlocks/templateButton";
import goTo from "../../../../../helper/goTo";
import convertDate from "../../../../../helper/convertDate";

type ProductMainInfoProps = {
    productId: string,
    productName: string, 
    productCode: string,
    seller: string,
    sellerId: string,
    totalGHG: number, 
    category: string, 
    description: string, 
    eventBuy: any,
    eventEditProduct: any,
    eventSource: any,
    eventEditSource: any,
    needsSourceEdit: boolean,
    status: string,
    sellerSourceHostoryEvent: any,
    sellerSourceApprove: any,
    sellerSourceStatus: string,
    eventEditSeller: any,
    eventAddSeller: any,
    sellerSourceId: string,
    productSourceId: string,
    date: number,
    originallyAddedBy: string,
    costs: number,
    sourceId: string,
};

const ProductCard = (props: ProductMainInfoProps) => {
    let date: string[] = convertDate(props.date);
    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="mainCarProdContainer">
                <div className="mainCarProdDivLeft">
                    <h3 className="productName">{props.productName}</h3>
                    <SimpleEdit
                        event={props.eventEditProduct}
                        editText="Edit Prodcut Data"
                    ></SimpleEdit>
                    {
                        props.status !== 'approvedFinal' && props.status !== 'notApprovedAgain' ?
                        <SimpleEdit
                            event={() => goTo('/approved/product/' + props.productId)}
                            editText="Approve Source"
                        ></SimpleEdit> : <></>
                    }
                    
                    {
                        props.status === 'newSource' ? <Tag bg={"#FAFCB9"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Not approved yet</Tag>
                        : props.status === 'approvedFirst' || props.status === 'approvedByNewUser' ? <Tag bg={"#C2FCB9"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Approved Once</Tag>
                        : props.status === 'notApprovedOnce' || props.status === 'notApprovedNewUser' ? <Tag bg={"#F5C0C0"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Not approved once</Tag>
                        : props.status === 'approvedFinal' ? <Tag bg={"green"} width={'120px'} height={'20px'} color={"white"} fontSize={"10px"}>Fully approved</Tag>
                        : props.status === 'notApprovedAgain' ? <Tag bg={"red"} width={'120px'} height={'20px'} color={"white"} fontSize={"10px"}>Not approved</Tag>
                        : <Tag bg={"#F5C0C0"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>Undefined Status</Tag>
                    }
                    {
                        props.status === 'approvedFinal' ?
                        <Button 
                            event={() => goTo('/change/history/product/' + props.productId)}
                            bg={"#41475E"}
                            width={"170px"}
                            color={"white"}
                            margin={"0 0 10px 0"}
                            border={'transparent'}
                            >
                            See approved data
                        </Button> : <div></div>
                    }
                    {
                        props.totalGHG < 0 ? <Tag bg={"green"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>GHG Negative Product</Tag>
                        : props.totalGHG === 0 ? <Tag bg={"yellow"} width={'120px'} height={'20px'} color={"black"} fontSize={"10px"}>GHG Neutral Product</Tag>
                        : props.totalGHG > 0 ? <Tag bg={"#F3C3C3"} width={'120px'} height={'20px'} color={"#571529"} fontSize={"10px"}>GHG High Product</Tag>
                        : <></>
                    }
                    <div className="divButtonProduMain">
                            <Button 
                            event={() => goTo('/change/history/product/' + props.productId)}
                            bg={"#41475E"}
                            width={"170px"}
                            color={"white"}
                            margin={"0 0 10px 0"}
                            border={'transparent'}
                            >
                            Change History
                        </Button>
                        <Button 
                            event={() => goTo('/source/' + props.sourceId + "/product/" + props.productName )}
                            bg={"#616574"}
                            width={"170px"}
                            color={"white"}
                            margin={"0 0 10px 0"}
                            border={'transparent'}
                            >
                            Approval History
                        </Button>
                        <Button 
                            event={props.eventBuy}
                            bg={"#404248"}
                            width={"170px"}
                            color={"white"}
                            margin={"0 0 10px 0"}
                            border={'transparent'}
                            >
                            Buy product
                        </Button>
                    </div>
                </div>
                <div className="mainCarProdDivMiddle">
                    <p className="mainCarProdP">Cost per Unit: {props.costs} EUR</p>
                    <div className="mainCarProdPDiv">
                        <p className="mainCarProdP">Category: {props.category}</p>
                        { props.category === 'Not provided' ?
                            <SimpleEdit
                                event={() => goTo('/add/category/to/product/' + props.productId)}
                                editText="Add a category"
                            ></SimpleEdit> : <></> }
                    </div>
                    <div className="mainCarProdPDiv">
                        <p className="mainCarProdP">Seller: {props.seller}</p>
                        { props.seller === 'Not provided' ?
                            <SimpleEdit
                                event={props.eventAddSeller}
                                editText="Add a seller"
                            ></SimpleEdit>
                            : 
                            <></>}
                    </div>
                    { props.seller === 'Not provided' ?
                        <></>
                        : <>
                        <SourceAndEdit
                            eventSource={() => goTo('/source/' + props.sellerSourceId + '/seller/' + props.seller)}
                            eventEdit={props.sellerSourceApprove}
                            status={props.sellerSourceStatus}
                        ></SourceAndEdit>
                        <SimpleEdit
                            event={() => goTo('/change/history/seller/' + props.sellerId)}
                            editText="Change History of the Seller"
                        ></SimpleEdit>
                    </>}
                    <p className="mainCarProdDesc">Description: {props.description}</p>
                </div>
                <div className="mainCarProdRight">
                    <p className="addedByProductcard">Originally added by {props.originallyAddedBy}</p> 
                    <p className="addedByProductcard">Originally added at {date[0]}</p>
                    <p className="addedByProductcard">Time: {date[1]}</p>
                </div>
            </div>
        </MainBar>
    )
};

export default ProductCard;
