import MainBar from "../../../../BuildingBlocks/templateMainBar";
import EditSourceStatus from "../../../../BuildingBlocks/templateSourceEdit";
import Edit from "../../../../BuildingBlocks/templateSimpleEdit";
import Productdateils from "./productDetails";
import goTo from "../../../../../helper/goTo";

type PRoductGHGProps = {
    productName: string, 
    totalGHG: number | string, 
    totalGHGSourceId: string,
    totalGHGEdit: any,
    totalGHGStatus: string,
    GHGSingle: any,
    EmissionReason: any,
    eventEditProduct: any,
    eventEditTotal: any,
    eventHistorySourceSingle: any,
    eventHistorySoueceReason: any,
    data: any,
    productId: string,
};

const ProductGreenHouseGasData = (props: PRoductGHGProps) => {
    const ghgDetails = props.GHGSingle && props.GHGSingle.map((item: any, i: number) => 
                <Productdateils
                    key={i} 
                    ghgName={item.ghgName}
                    ghgEmission={item.emissionPerKG + "kg"}
                    GHGSourceEvent={() => goTo('/source/' + props.data.data.singleEmission[i].sourceId + '/single/' + props.data.data.singleEmission[i].ghgName)}
                    GHGSoucreEventApprove={() => goTo('/verify/source/' + props.data.data.singleEmission[i].sourceId + '/' + props.productId + '/single/' + i + '/new/' + props.data.data.singleEmission[i].id)}
                    GHGEdit={item.GHGEdit}
                    GHGStatus={item.GHGStatus}
                    eventEdit={() => goTo('/edit/ghg/single/' + props.data.data.product.id)}
                    type={'single'}
                    id={props.data.data.singleEmission[i] !== undefined ? props.data.data.singleEmission[i].id : ''}
                ></Productdateils> )

    const ghgReasons = props.EmissionReason && props.EmissionReason.map((item: any, j: number) => 
                <Productdateils
                    key={item.id} 
                    ghgName={item.reasonTitle}
                    ghgEmission={"Greenhouse Gas: "+item.ghgName}
                    GHGSourceEvent={() => goTo('/source/' + props.data.data.emissionReason[j].sourceId + '/single/' + props.data.data.emissionReason[j].reasonTitle)}
                    GHGSoucreEventApprove={() => goTo('/verify/source/' + props.data.data.emissionReason[j].sourceId + '/' + props.productId + '/reason/' + j + '/new/' + props.data.data.emissionReason[j].id)}
                    GHGEdit={item.GHGEdit}
                    GHGStatus={item.GHGStatus}
                    eventEdit={() => goTo('/edit/ghg/reason/' + props.data.data.product.id)}
                    type={'reason'}
                    id={props.data.data.emissionReason[j] !== undefined ? props.data.data.emissionReason[j].id : ''}
                ></Productdateils> )

    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="ProductGHGDataContainer">
                <h3 className="ProductGHGDataTitle">{props.productName} - Greenhouse Gas Data</h3>
                <div className="EmissionMainStatementDiv">
                        <p className="EmissionDataTitle">Total Emission: </p>
                        <div className='emssionTotoalDivFlex'>
                        { props.totalGHG !== 'Not provided' ?
                        <>
                            <h1 className="EmissionDataText">1 KG of {props.productName} emits {props.totalGHG} KG of Green House Gases until it is sold.</h1>
                            <EditSourceStatus 
                                eventSource={() => goTo('/source/' + props.totalGHGSourceId + '/total/' + props.totalGHG + ' KG')}
                                eventEdit={props.totalGHGEdit}
                                status={props.totalGHGStatus}
                            ></EditSourceStatus>
                            <Edit
                                event={() => goTo('/edit/ghg/total/' + props.data.data.product.id)}
                                editText="Request to delete"
                            ></Edit>
                        </>
                        : <>
                            <h1 className="EmissionDataText">There is not data yet provided how much KG of Green House Gases are emited for 1 KG of {props.productName} sold.</h1>
                            <Edit
                                event={() => goTo('/add/total/to/product/' + props.data.data.product.id)}
                                editText="Add total GHG data"
                            ></Edit>
                        </>
                        }
                        </div>
                </div>
                <div className="EmissionMainStatementDiv">
                    <h5 className="EmissionDataTitle">Emission Details</h5>
                    <div className="emissionsDetailDataDiv">
                    { props.GHGSingle && props.GHGSingle[0] ?
                        <>
                            {ghgDetails}
                            <Edit
                                event={() => goTo('/add/single/to/product/' + props.data.data.product.id)}
                                editText="Add additional single GHG data"
                            ></Edit>
                        </>
                        : <div className="emissionDetailsDivWithEdit">
                            <p className='emssissionDataTextCond'>Single Green House Gas Emissions are just not provided for {props.productName}</p>
                            <Edit
                                event={() => goTo('/add/single/to/product/' + props.data.data.product.id)}
                                editText="Add single GHG data"
                            ></Edit>
                        </div>
                    }
                    </div>
                </div>
                <div className="EmissionMainStatementDiv">
                    <h5 className="EmissionDataTitle">Emission Reasons</h5>
                    <div className="emissionsDetailDataDiv">
                    { props.EmissionReason && props.EmissionReason[0] ?
                        <>
                            {ghgReasons}
                            <Edit
                                event={() => goTo('/add/reason/to/product/' + props.data.data.product.id)}
                                editText="Add additional GHG Reasons"
                            ></Edit>
                        </>
                        : <div className="emissionDetailsDivWithEdit">
                        <p className='emssissionDataTextCond'>Reasons for Green House Gas Emissions are not provided for {props.productName}</p>
                        <Edit
                            event={() => goTo('/add/reason/to/product/' + props.data.data.product.id)}
                            editText="Add GHG Reasons"
                        ></Edit>
                    </div>
                    }
                    </div>
                </div>
            </div>
        </MainBar>
    )
};

export default ProductGreenHouseGasData;
