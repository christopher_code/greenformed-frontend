import goTo from "../../../../../helper/goTo";

type AlternativeProductProps = {
    productId: string,
    productName: string,
    totalGHGEmissions: number,
};

const AlternativeProduct = (props: AlternativeProductProps) => {
    return (
        <div className="alternativeProduct" onClick={() => goTo('/product/' + props.productId)}>
            <div className="alternativeProductsHeadlineDiv">
                <h3 className="alternativeProductName">
                    {props.productName}
                </h3>
            </div>
            <div className="alternativeProductsHeadlineDiv">
                <h3 className="alternativeProductP">
                    Emits {props.totalGHGEmissions} KG per produced KG
                </h3>
            </div>
        </div>
    )
};

export default AlternativeProduct;
