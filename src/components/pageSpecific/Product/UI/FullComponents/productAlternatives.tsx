import goTo from "../../../../../helper/goTo";
import MainBar from "../../../../BuildingBlocks/templateMainBar";
import Edit from "../../../../BuildingBlocks/templateSimpleEdit";
import AlternativeProducts from "./alternativeCard";

type ProductCardProps = {
    productId: string,
    name: string, 
    alternativeProducts: any,
    rawMaterials: any,
};

const ProductCard = (props: ProductCardProps) => {
    const alternativeProduct = props.alternativeProducts && props.alternativeProducts.map((item: any) => 
                <AlternativeProducts
                    key={item.id} 
                    productId={item.id}
                    productName={item.name}
                    totalGHGEmissions={item.totalGHGEmissions}
                ></AlternativeProducts> )
    
    const rawMaterialsProduct = props.rawMaterials && props.rawMaterials.map((item: any) => 
                <AlternativeProducts
                    key={item.id} 
                    productId={item.id}
                    productName={item.name}
                    totalGHGEmissions={item.totalGHGEmissions}
                ></AlternativeProducts> )


    return (
        <MainBar
            marginTop={"0"}
            marginBottom={'20px'}
        >
            <div className="productAltHeadlineDiv">
                <div className="productAltHeadlineDivFlex">
                    <h2 className="productAltHeadline">Alternative Products of {props.name}: </h2>
                </div>
                { props.alternativeProducts && props.alternativeProducts[0] ?
                <div className="alternativeDivFlex">
                    {alternativeProduct}
                    <Edit
                        event={() => goTo('/add/alternative/to/product/' + props.productId)}
                        editText="Add additional Alternatives"
                    ></Edit>
                </div> : <>
                <h3 className="productAltHeadlineNotProv">No alternative products selected yet for {props.name}.</h3>
                    <Edit
                        event={() => goTo('/add/alternative/to/product/' + props.productId)}
                        editText="Add Alternatives"
                    ></Edit>
                </>
                }
            </div>
            <div className="productAltHeadlineDiv">
                <div className="productAltHeadlineDivFlex">
                    <h2 className="productAltHeadline">Raw materials of {props.name}:</h2>
                </div>
                { props.rawMaterials && props.rawMaterials[0] ?
                <div className="alternativeDivFlex">
                    {rawMaterialsProduct}
                    <Edit
                        event={() => goTo('/add/rawmaterial/to/product/' + props.productId)}
                        editText="Add raw materials of the product"
                    ></Edit>
                </div> : <>
                <h3 className="productAltHeadlineNotProv">No raw materials are provided yet {props.name}.</h3>
                    <Edit
                        event={() => goTo('/add/rawmaterial/to/product/' + props.productId)}
                        editText="Add Raw Materials"
                    ></Edit>
                </>
                }
            </div>
            
        </MainBar>
    )
};

export default ProductCard;
