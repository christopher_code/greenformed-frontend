import EditSourceStatus from "../../../../BuildingBlocks/templateSourceEdit";
import EditSimple from "../../../../BuildingBlocks/templateSimpleEdit";
import goTo from "../../../../../helper/goTo";

type ProductDetailsProps = {
    ghgName: string, 
    ghgEmission: string,
    GHGSourceEvent: any,
    GHGSoucreEventApprove: any,
    GHGEdit: any,
    GHGStatus: string,
    eventEdit: any,
    id: string,
    type: string,
    // index: number,
};

const ProductDetails = (props: ProductDetailsProps) => {

    return (
        <div className="ghgDetailContainer">
            <div className="ghgDetailPDiv">
                <p className="ghgDetailParagraph">{props.ghgName}</p>
                <p className="ghgDetailParagraphBrackets">({props.ghgEmission})</p>
            </div>
            <EditSourceStatus 
                eventSource={props.GHGSourceEvent}
                eventEdit={props.GHGSoucreEventApprove}
                status={props.GHGStatus}
                // index={props.index}
            ></EditSourceStatus>
            <EditSimple
                event={props.eventEdit}
                editText={"Edit GHG Data"}
            ></EditSimple>
            <EditSimple
                event={()=> goTo("/change/history/"+ props.type +"/" + props.id)}
                editText={"Change History"}
            ></EditSimple>

        </div>
    )
};

export default ProductDetails;
