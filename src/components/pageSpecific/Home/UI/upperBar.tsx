import React from 'react';
import MainBar from "../../../BuildingBlocks/templateMainBar";
import Button from "../../../BuildingBlocks/templateButton";
import goTo from "../../../../helper/goTo";

const Navigation: React.FC = () => {

    return (
    <MainBar
        marginTop={'0'}
        marginBottom={'20px'}
    >
        <div className="topBarFlex">
            <div>
                <h1 className="topBarHeadline">Read all data from Greenformed: approved and not-approved GHG data </h1>
                <h3 className="topBarSubheadline">Greenformed is a platform for you to get approved Greenhouse Gas Data. In order to receive confirmed data, we need you and the whole Greenformed community. Add actual data, confirm or deny other data and get all data from our databases. Get only approved data, or as on this page all data (approved and not-approved).</h3>
                <div className="upperBarDivButtons">
                    <div className='divUpperBarButton'>
                        <Button 
                            event={() => goTo('/approved/data')}
                            bg={"#A3A9C3"}
                            border={'#A3A9C3'}
                            width={"215px"}
                            color={"#36354A"}
                            margin={"20px 0 0 0"}
                        >
                            Approved data
                        </Button>
                        <Button 
                            event={() => goTo('/approval/init/data')}
                            bg={"#A3A9C3"}
                            border={'#A3A9C3'}
                            width={"215px"}
                            color={"#36354A"}
                            margin={"20px 0 0 0"}
                        >
                            Verify Data
                        </Button>
                        <Button 
                            event={() => goTo('/new/product')}
                            bg={"#A3A9C3"}
                            border={'#A3A9C3'}
                            width={"215px"}
                            color={"#36354A"}
                            margin={"20px 0 0 0"}
                        >
                            Add Data
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    </MainBar>
    );
};

export default Navigation;
