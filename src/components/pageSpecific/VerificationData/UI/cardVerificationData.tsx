import MainBar from "../../../BuildingBlocks/templateMainBar";
import { Link } from 'react-router-dom';

type VerificationCardProps = {
    sourceFor: string,
    sourceTitle: string,
    sourceLink: string,
    date: number, 
    status: string,
    descriptionSource: string, 
    sourceId: string,
    productsId: string,
    typeSource: string,
    index: number,
    typeRel: string,
    modelId: string,
};

const CardVerificationData = (props: VerificationCardProps) => {
    let pathVerify: string = "/verify/source/" + props.sourceId + '/' + props.productsId + '/' + props.typeRel + '/' + props.index + '/' + props.typeSource + '/' + props.modelId;
    let pathProduct: string = "/product/" + props.productsId;
    let type: string ='';
    if (props.typeRel === 'product') {type='Product'}
    else if (props.typeRel === 'seller') {type='Seller'}
    else if (props.typeRel === 'single') {type='Single GHG'}
    else if (props.typeRel === 'total') {type='Total GHG'}
    else if (props.typeRel === 'reason') {type='Reason of GHG Emission'}
    else if (props.typeRel === 'alternative') {type='Alternative Product'}

    return (
        <MainBar
            marginTop={'20px'}
            marginBottom={'0'}
        >
            <div className="cardApprovalContainer">
                <h2 className="cardApprovalTitle">{props.typeSource} Source for {type}: {props.sourceFor}.</h2>
                <div className="cardApprovalUpperDiv">
                    <div className="cardApprovalTextDiv">
                        <p className="cardApprovalP">Source Title: {props.sourceTitle}</p>
                        <p className="cardApprovalP">Source Link: {props.sourceLink}</p>
                        <p className="cardApprovalP">Date of Creation: {props.date}</p>
                        <p className="cardApprovalP">Status: {props.status}</p>
                    </div>
                    <div className="cardApprovalRightDiv">
                        <p className="cardApprovalPDesc">Source Description: {props.descriptionSource}</p>
                    </div>
                </div>
            </div>
            <div className="cardApprovalButtonDiv">
                <div className="cardApprovalButtonFlexDiv">
                    <Link to={pathVerify}>Approve Source</Link>
                    <Link to={pathProduct}>See full data</Link>
                </div>
            </div>
        </MainBar>
    )
};

export default CardVerificationData;
