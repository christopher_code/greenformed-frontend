import { useState, useEffect } from 'react';
import SkeletonTemplate from "../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../BuildingBlocks/templateMainBar';
import VerificationCard from "../UI/cardVerificationData";
import MessageCard from "../../../BuildingBlocks/templateFeedback";
import { urlBE } from "../../../../App";

const SourceApprovalReasonGHG = () => {
    const [typeRel, setTypeRel] = useState<string>('product');
    const [typeSource, setTypeSource] = useState<string>('new');
    const [roles, setRoles] = useState<string>('user');
    const [pData, setPData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let role: string;
    if (roles === 'superuser') {role = 'superuser'} else {role = 'user'}

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, [typeSource, typeRel]);

    const loadData = async () => {
            let roleStroage: any = await localStorage.getItem('role');
            if (roleStroage) {
                roleStroage=roleStroage.replace(/['"]+/g, '')
                setRoles(roleStroage)}

            fetch(urlBE + '/api/o/source/get/random/'+ typeRel + '/' + role + '/' + typeSource, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(res => {
                if (res.ok) {
                    return res.json();
                }
                throw res;
            })
            .then(resData => setPData(resData))
            .catch(err => setFError(err));
    }

    const cards = pData && pData.data.map((item: any) => 
        <VerificationCard
            key={item.source.id}
            typeRel={typeRel}
            sourceFor = {item.relationNode.dataValues.name ? item.relationNode.dataValues.name 
                : item.relationNode.dataValues.ghgPerKG ? item.relationNode.dataValues.ghgPerKG + " KG per 1KG sold"
                : item.relationNode.dataValues.reasonTitle ? item.relationNode.dataValues.reasonTitle
                : item.relationNode.dataValues.ghgName ? item.relationNode.dataValues.ghgName
                : item.relationNode.dataValues.name}
            sourceTitle = {item.source.title}
            sourceLink = {item.source.link}
            date = {item.source.createdAt}
            status = {item.source.status}
            descriptionSource = {item.source.description}
            sourceId = {item.source.id}
            productsId = {item.source.productId}
            typeSource = {typeSource}
            index = {0}
            modelId = {item.source.relId}
        ></VerificationCard>
    )

    const changeTypeRel = async (typeRelI: string) => {
        setFError(undefined);
        setTypeRel(typeRelI);
    }

    const changeTypeSource = async (typeSourceI: string) => {
        setFError(undefined);
        setTypeSource(typeSourceI);
    }

    return (
        <> { cards && pData ? <div>
                <MainCard marginTop={'0'} marginBottom={'20px'}>
                    <h1 className='headlineApproveSourcesFetch'>Approve sources of Green House Gas Data</h1>
                </MainCard>
                { role !== 'user' ?
                <MainCard marginTop={'0'} marginBottom={'0'}>
                    <h2 className='headlineApproveSourcesFetch'>Select what you want to approve</h2>
                    <div className="chooseTypeDivFlex">
                        <div className='chooseTypeSouerceDiv' onClick={() => changeTypeSource('new')}>Added Data</div>
                        <div className='chooseTypeSouerceDiv' onClick={() => changeTypeSource('update')}>Requested to update</div>
                        <div className='chooseTypeSouerceDiv' onClick={() => changeTypeSource('delete')}>Requested to delete</div>
                    </div>
                    { typeSource === 'new' ?
                    <>
                        <h2 className='headlineApproveSourcesFetch'>Select the type of data you want to approve:</h2>
                        <div className="chooseTypeDivFlex">
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('product')}>Product</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('seller')}>Seller</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('total')}>Total Emission Data</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('single')}>Single Emission Data</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('reason')}>Reason of Emission Data</div>
                        </div> 
                    </>
                    : typeSource === 'update' ?
                    <>
                        <h2 className='headlineApproveSourcesFetch'>Select the type of update data you want to approve:</h2>
                        <div className="chooseTypeDivFlex">
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('product')}>Product</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('seller')}>Seller</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('single')}>Single Emission Data</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('reason')}>Reason of Emission Data</div>
                        </div> 
                    </> 
                    : typeSource === 'delete' ?
                    <>
                        <h2 className='headlineApproveSourcesFetch'>Select the type of deletion data you want to approve:</h2>
                        <div className="chooseTypeDivFlex">
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('product')}>Product</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('seller')}>Seller</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('total')}>Total Emission Data</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('single')}>Single Emission Data</div>
                            <div className='chooseRelTypeDiv' onClick={() => changeTypeRel('reason')}>Reason of Emission Data</div>
                        </div> 
                    </> 
                    :<></>
                    }
                    {
                        fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >No data for your requested category provided any more - approve the other sources first</MessageCard>
                        : <></>
                    }
                </MainCard> : <></>
            }
                
                {cards}
            </div>
        : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
        : 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalReasonGHG;