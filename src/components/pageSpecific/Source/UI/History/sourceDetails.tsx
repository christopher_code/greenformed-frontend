type SourceDetailProps = {
    type: string,
    name: string,
    titel: string, 
    description: string,
    link: string, 
    status: string,
};

const SourceDetails = (props: SourceDetailProps) => {
    return (
        <div className='containerSourceDetails'>
            <h2 className='headlineSourceDetails'>Source for the {props.type} {props.name}</h2>
            <div className='divSourceDetails'>
            <p className='descSourceDetails'>Source Title: {props.titel}</p>
                <p className='descSourceStatus'>Current Source Status: {props.status}</p>
            </div>
            <p className='descSourceDetails'>Source Description: {props.description}</p>
            <p className='descSourceDetails'>Source Link: <a href={props.link} target="_blank" rel="noreferrer">click here</a></p>
        </div>
    )
};

export default SourceDetails;