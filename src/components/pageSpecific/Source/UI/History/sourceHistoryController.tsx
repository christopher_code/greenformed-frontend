import { useParams } from "react-router-dom";
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import SourceHistory from "../../CRUD/approvalSourceHistory";

const SourceHistoryController = () => {

    let id: any = useParams();

    return (
        <>
            {
                id.type === 'product' ? <SourceHistory name={id.name} type={'product'} />
                : id.type === 'seller' ? <SourceHistory name={id.name} type={'seller'} />
                : id.type === 'reason' ? <SourceHistory name={id.name} type={'reason of GHG Emission'} />
                : id.type === 'total' ? <SourceHistory name={id.name} type={'total GHG Emission'} />
                : id.type === 'single' ? <SourceHistory name={id.name} type={'single GHG Emission'} />
                :
                <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    Please Provide correct data
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            }
        </>
    )
};

export default SourceHistoryController;