import convertDate from "../../../../../helper/convertDate";

type SourceDotProps = {
    approvalState: string,
    date: number, 
    description?: string,
    user: string, 
};

const SourceDot = (props: SourceDotProps) => {
    let date: string[] = convertDate(props.date);
    return (
        <div className='containerSourceDot'>
        <p className='dateSourceDot' >{date[0] + ", " + date[1]}</p> 
            {
                props.approvalState === 'Frist approved by ' ? <div className="dotSourceDotsLG"></div>
                : props.approvalState === 'Approval by new user ' ? <div className="dotSourceDotsLG" ></div>
                : props.approvalState === 'Final approval by ' ? <div className="dotSourceDotsG"></div>
                : props.approvalState === 'Final denial by ' ? <div className="dotSourceDotsR"></div>
                : props.approvalState === 'Denial by new user ' ? <div className="dotSourceDotsLR"></div>
                : props.approvalState === 'First denial by ' ? <div className="dotSourceDotsLR"></div>
                : props.approvalState === 'Created by ' ? <div className="dotSourceDotsLB"></div>
                : <></>
            }
            
            <div className="dotsTextDiv">
                <div className="dotsDataDiv">
                    <div className="dotsDataUpperDiv">
                        <p className='statusSourceDot'>{props.approvalState}</p>
                        <p className='userSourceDot'>{props.user}</p>

                    </div>
                    <p className='descriptionSourceDot'>Description: {props.description}</p>
                </div>
            </div>
        </div>
    )
};

export default SourceDot;
