import MainBar from "../../../../BuildingBlocks/templateMainBar";
import SourceDots from "./sourceDot";
import SortApprovalArray from "../../../../../helper/sortApprovalSources";

export type SourceMainInfoProps = {
    dataSource: string,
    dataName: string
    createdAt: number,
    addedBy: any,
    createdDescr: string,
    firstApprovalData?: any,
    finalApprovalData?: any,
    newUserApprovalData?: any,
    firstDenialData?: any,
    finalDenialData?: any,
    newUserDenialData?: any,
};

const SourceHistory = (props: SourceMainInfoProps) => {

    let sortedArray = SortApprovalArray(props);

    return (
        <>
            <MainBar
                marginTop={"0"}
                marginBottom={'20px'}
            >
                <h2 className="sourceApprovalHistoryHeadline">Source Approval History for {props.dataSource} {props.dataName}</h2>
                <div className='sourceHistoryContainer' >
                    <SourceDots
                        date={props.createdAt}
                        approvalState='Created by '
                        user={props.addedBy}
                        description={props.createdDescr}
                    ></SourceDots>
                    {sortedArray}
                </div>
            </MainBar>
        </>
    )
};

export default SourceHistory;
