import { useState } from "react";
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import Modal from "../../../../BuildingBlocks/templateModal";

import Info from "../../../../../assets/information.png";

import SourceAddApprovalProduct from "../../CRUD/Added/prodApproveSource";
import SourceAddApprovalTotal from "../../CRUD/Added/totalGHGAppSource";
import SourceAddApprovalSingle from "../../CRUD/Added/singleGHGApproveSource";
import SourceAddApprocalReason from "../../CRUD/Added/reasonSourceApproval";
import SourceAddApprovalSeller from "../../CRUD/Added/sellerSourceApproval";

import SourceUpdateApprovalProduct from "../../CRUD/Updated/approveProductUpdate";
import SourceUpdateApprovalSeller from "../../CRUD/Updated/approvesellerUpdateSource";
import SourceUpdateApprovalSingle from "../../CRUD/Updated/approveSingleUpdateSource";
import SourceUpdateApprovalReason from "../../CRUD/Updated/reasonGHGApprovalSource";

import SourceDeletionApproveProduct from "../../CRUD/Deleted/prodApproveDeletionSource";
import SourceDeletionApproveSeller from "../../CRUD/Deleted/sellerApproveDeletionSource";
import SourceDeletionApproveTotal from "../../CRUD/Deleted/totalApproveDeletionSource";
import SourceDeletionApproveSingle from "../../CRUD/Deleted/singleApproveDeletionSource";
import SourceDeletionApproveReason from "../../CRUD/Deleted/reasonApproveDeletionSource";

const SourceApprovalService = () => {
    let id: any = useParams();
    const [showModalData, setShowModalData] = useState<boolean>(false);

    return (
        <> 
        <MainCard marginTop={'0'} marginBottom={'20px'}>
        <h2 className='headlineApprovalSource'>Approve or deny the data that is displayed</h2>
        <div className="modalDivRow" onClick={() => setShowModalData(true)}>
            <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
            <p className="modalButtonPre">Get more information</p>
        </div>
        { showModalData ?
        <Modal 
            info={'The data that is the subject of the approval mechansim is displayed below. To verify the source please select the checkbox and fill the data.'}
            eventClose={() => setShowModalData(false)}
            show={showModalData}
        /> : <></> }
        </MainCard>
        {
            id.typeSource === 'new' ? 
            <>
                { 
                    id.type === "product" ? <SourceAddApprovalProduct idOri={true}/>
                    : id.type === 'total' ? <SourceAddApprovalTotal/>
                    : id.type === 'single' ? <SourceAddApprovalSingle/>
                    : id.type === 'reason' ? <SourceAddApprocalReason/>
                    : id.type === 'seller' ? <SourceAddApprovalSeller/>
                    : <></>
                } 
            </>
        : id.typeSource === 'update' ?
            <>
                { 
                    id.type === "product" ? <SourceUpdateApprovalProduct/>
                    : id.type === 'single' ? <SourceUpdateApprovalSingle/>
                    : id.type === 'reason' ? <SourceUpdateApprovalReason/>
                    : id.type === 'seller' ? <SourceUpdateApprovalSeller/>
                    : <></>
                } 
            </>
        : id.typeSource === 'delete' ?
        <>
            { 
                id.type === "product" ? <SourceDeletionApproveProduct/>
                : id.type === 'total' ? <SourceDeletionApproveTotal/>
                : id.type === 'single' ? <SourceDeletionApproveSingle/>
                : id.type === 'reason' ? <SourceDeletionApproveReason/>
                : id.type === 'seller' ? <SourceDeletionApproveSeller/>
                : <></>
            } 
        </>
        : <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } 
        </>
    )
};

export default SourceApprovalService;
