import {useState, useEffect} from 'react';
import goTo from '../../../../helper/goTo';
import MessageCard from "../../../BuildingBlocks/templateFeedback";
import Modal from "../../../BuildingBlocks/templateModal";
import { urlBE } from "../../../../App";
import Info from "../../../../assets/information.png";

type SourceApprovalProps = {
    sourceId: string, 
    productId: string,
    typeSource: string,
    modelId: string,
};

const VerifySource = (props: SourceApprovalProps) => {
    const [description, setDescription] = useState<string>('');
    const [approvalStatus, setApprovalStatus] = useState<boolean>(false);
    const [fError, setFError] = useState<any>();
    const [response, setResonse] = useState<any>();
    const [showModalData, setShowModalData] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [userId, setUserId] = useState<string>('');
    const [role, setRole] = useState<string>('');

    useEffect(() => {
        loadLocalStorage();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        let dataId: any = await localStorage.getItem('id');
        let dataRole: any = await localStorage.getItem('role');
        if (dataId) {
            dataId=dataId.replace(/['"]+/g, '')
            setUserId(dataId)}
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
        if (dataRole) {
            dataRole=dataRole.replace(/['"]+/g, '')
            setRole(dataRole)}
    }

    const putSourceStatus = (e: any) => {
        e.preventDefault();
    
        fetch(`${urlBE}/api/approval/source/${userId}/${props.modelId}/${props.sourceId}/${role}/${props.typeSource}/${props.productId}/0`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'authorization': 'Bearer ' + token,
                    "X-Content-Type-Options": "nosniff"
                },
                body: JSON.stringify({
                    description: description,
                    approval: approvalStatus,
                })
            }).then(res => {
                if (res.ok) {
                    setDescription('');
                    setResonse(true)
                    return res.json();
                }
                throw res;
              }).then(() => {setFError(undefined)
                goTo('/product/' + props.productId)
            })
              .catch(error => {
                setFError(error);
              }
            );
      }

    return (
        <>
        { !fError ?
            <div className="appro">
                <h2 className='headlineApprovalSource'>The approval is done here:</h2>
            <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                <p className="modalButtonPre">Get more information</p>
            </div>
            { showModalData ?
            <Modal 
                info={'To verify the source please select the checkbox and fill the data. If you want to deny the source please keep the checkbox unchecked.'}
                eventClose={() => setShowModalData(false)}
                show={showModalData}
            /> : <></> }
                <form onSubmit={putSourceStatus}>
                    <p className="labelNewProduct">To verify the source select the checkbox. To deny the source please leabe the checkbox unselected.</p> 
                    <div className="checkDivApproveSource">
                        <label className="verifiedBoxLabel">Verified:</label>
                        <input type="checkbox"
                            id="verified" 
                            name="verified" 
                            value="verified"
                            onChange={() => setApprovalStatus(!approvalStatus)}
                            />
                    </div>
                    <label className="labelNewProduct">Description of the Decision *</label> 
                    <textarea className="form-control me-2"
                        id="inputNewSellerSourceDescription" 
                        placeholder="Description of De"
                        aria-label="inputDescriptionProduct"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        minLength={20}
                        required
                        ></textarea>
                    <div className="buttonDivSignUp">
                        <input type="submit" value="Submit" className="formButtonGeneral" />
                    </div>
                </form>
            </div>
            : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Same user is not allowed to apporve data twice.</MessageCard>
            : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
        }
        { response ? <MessageCard bg={'#CBE5C8'} color={'#1F4A1A'} border={'2px solid #1F4A1A'} >You successfully approved the source</MessageCard> : <></>}
        </>
    )
};

export default VerifySource;