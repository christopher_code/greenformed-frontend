import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import SourceDetails from "../../UI/History/sourceDetails";
import ApprovalForm from "../approveSourceForm";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { urlBE } from "../../../../../App";

const SourceApprovalUpdatedProduct = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(urlBE + '/api/o/seller/get/source/' + id.modelId + '/' + id.id, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }

    const sources = fData ? 
        <>
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h2 className='headlineApprovalSource'>The provided data of the updated seller: {fData.data.seller.name}</h2>
                <p className="approveSourceItemHeadline">Link of seller: <a href={fData.data.seller.link} rel="noreferrer" target='_blank'>{fData.data.seller.link}</a></p>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
            <h2 className='headlineApprovalSource'>The data was updated as following:</h2>
            <p className="approveSourceItemHeadline">Changed Field: Link</p>
            <p className="approveSourceItemHeadline">Original Value: {fData.data.updateSource[0].properties.OriginalValue}</p>
            <p className="approveSourceItemHeadline">New Value: {fData.data.updateSource[0].properties.NewValue}</p>
                <SourceDetails
                    type={'updated seller'}
                    name={fData.data.seller.name}
                    titel={fData.data.updateSource[0].source.title}
                    description={fData.data.updateSource[0].source.description}
                    link={fData.data.updateSource[0].source.link}
                    status={fData.data.updateSource[0].source.status}
                ></SourceDetails>
            </MainCard>
            { fData.data.updateSource[0].source.status !== 'approvedFinal' && fData.data.updateSource[0].source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={fData.data.updateSource[0].source.id}
                        productId={id.product}
                        typeSource='seller'
                        modelId={fData.data.seller.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && fData ? <div>{sources}
        {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>  : <></>         
        }
        </div>: 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalUpdatedProduct;
