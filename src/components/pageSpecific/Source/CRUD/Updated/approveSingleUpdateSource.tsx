import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import SourceDetails from "../../UI/History/sourceDetails";
import ApprovalForm from "../approveSourceForm";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { urlBE } from "../../../../../App";

const SourceApprovalUpdatedSingle = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();
    const [token, setToken] = useState<string>('');

    let id: any = useParams();

    useEffect(() => {
        loadLocalStorage();
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadLocalStorage = async () => {
        let dataT: any = await localStorage.getItem('token');
        if (dataT) {
            dataT=dataT.replace(/['"]+/g, '')
            setToken(dataT)}
    }

    const loadData = async () => {
        fetch(urlBE + '/api/ghg/single/get/source/' + id.modelId + '/' + id.id, { 
            method: 'get', 
            headers: {
                "Content-Type": "application/json",
                'authorization': 'Bearer ' + token,
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }

    const sources = fData ? 
        <>
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h2 className='headlineApprovalSource'>The name of the updated ghg emission: {fData.data.single.ghgName}</h2>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
            <h2 className='headlineApprovalSource'>The data was updated as following:</h2>
            <p className="approveSourceItemHeadline">Changed Field: Emission Per KG</p>
            <p className="approveSourceItemHeadline">Original Value: {fData.data.updateSource[0].properties.OriginalValue} KG</p>
            <p className="approveSourceItemHeadline">New Value: {fData.data.updateSource[0].properties.NewValue} KG</p>
                <SourceDetails
                    type={'updated single GHG emission'}
                    name={fData.data.single.ghgName}
                    titel={fData.data.updateSource[0].source.title}
                    description={fData.data.updateSource[0].source.description}
                    link={fData.data.updateSource[0].source.link}
                    status={fData.data.updateSource[0].source.status}
                ></SourceDetails>
            </MainCard>
            { fData.data.updateSource[0].source.status !== 'approvedFinal' && fData.data.updateSource[0].source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={fData.data.updateSource[0].source.id}
                        productId={id.product}
                        typeSource='single'
                        modelId={fData.data.single.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : <p>...Loading</p>

    return (
        <> { sources  && fData ? <div>{sources}
        {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>  : <></>         
        }
        </div>: 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalUpdatedSingle;
