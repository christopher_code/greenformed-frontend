import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import SourceDetails from "../../UI/History/sourceDetails";
import ApprovalForm from "../approveSourceForm";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { urlBE } from "../../../../../App";

const SourceApprovalDeletedSeller = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(urlBE + '/api/o/seller/get/source/' + id.modelId + '/' + id.id, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }

    const sources = fData && !fError ? 
        <>
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h4 className="headlineApprovalSource">The following data displays the seller as it was as it was requested to be deleted:</h4>
                <h2 className="approveSourceItemHeadline">The provided data of the seller requested to be deleted: {fData.data.deletionSource[0].properties.OriginalName}</h2>
                <p className="approveSourceItemHeadline">The seller's link is: <a href={fData.data.deletionSource[0].properties.OriginalLink} rel="noreferrer" target='_blank'>Click here</a></p>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
            <h2 className="headlineApprovalSource">The data should be deleted due to the following: </h2>
                <SourceDetails
                    type={'seller requested to be deleted'}
                    name={fData.data.seller.name}
                    titel={fData.data.deletionSource[0].source.title}
                    description={fData.data.deletionSource[0].source.description}
                    link={fData.data.deletionSource[0].source.link}
                    status={fData.data.deletionSource[0].source.status}
                ></SourceDetails>
            </MainCard>
            { fData.data.deletionSource[0].source.status !== 'approvedFinal' && fData.data.deletionSource[0].source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={fData.data.deletionSource[0].source.id}
                        productId={id.product}
                        typeSource='seller'
                        modelId={fData.data.seller.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && fData ? <div>{sources}
        {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>  : <></>         
        }
        </div>: 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalDeletedSeller;
