import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import SourceDetails from "../../UI/History/sourceDetails";
import ApprovalForm from "../approveSourceForm";
import ApproveProduct from "./prodApproveSource";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { urlBE } from "../../../../../App";

const SourceApprovalTotalGHG = () => {
    const [pData, setPData] = useState<any>();
    const [sData, setSData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadTotalData();
        loadSourceData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadTotalData = async () => {
        fetch(urlBE + '/api/o/product/get/full/' + id.product, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setPData(resData))
            .catch(err => setFError(err));
    }

    const loadSourceData = async () => {
        fetch(urlBE + '/api/o/source/get/id/' + id.id, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setSData(resData))
            .catch(err => setFError(err));
    } 

    const sources = pData && sData && !fError ? 
        <>
            { 
            pData.data.product.approvalStatus !== 'approvedFinal' ?
                <>
                    <MainCard marginTop={"0"}
                        marginBottom={'20px'}>
                        <h2 className="approveSourceItemHeadline">Before updating the Emission reason, please approve the product first.</h2>
                    </MainCard>
                    <ApproveProduct idOri={false}></ApproveProduct>
                </>
            : <>
            <MainCard marginTop={'0'} marginBottom={'20px'}>
                   <h2 className='approveSourceItemHeadline'>Total emissions of {pData.data.product.name} per 1kg: {pData.data.totalEmission[0].ghgPerKG} kg</h2>
            </MainCard> 
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
                <SourceDetails
                    type={'total GHG Emission'}
                    name={pData.data.product.name}
                    titel={sData.data.source.title}
                    description={sData.data.source.description}
                    link={sData.data.source.link}
                    status={sData.data.source.status}
                ></SourceDetails>
            </MainCard>
            { sData.data.source.status !== 'approvedFinal' && sData.data.source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={pData.data.totalEmission[0].sourceId}
                        modelId={pData.data.totalEmission[0].id}
                        typeSource='total'
                        productId={pData.data.product.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
            </>
            }
        </>
        : <p>...Loading</p>

    return (
        <> { sources  && pData ? <div>{sources}
        {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>  : <></>         
        }
        </div>: 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalTotalGHG;
