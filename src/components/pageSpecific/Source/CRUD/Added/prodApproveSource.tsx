import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../../BuildingBlocks/templateMainBar';
import SourceDetails from "../../UI/History/sourceDetails";
import ApprovalForm from "../approveSourceForm";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import { urlBE } from "../../../../../App";

type sourceApprovalProductProps = {
    idOri: boolean,
};

const SourceApprovalProduct = (props: sourceApprovalProductProps) => {
    const [pData, setPData] = useState<any>();
    const [sData, setSData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadProdData();
        loadSourceData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, [pData]);

    const loadProdData = async () => {
        fetch(urlBE + '/api/o/product/get/core/' + id.product, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
            }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setPData(resData))
            .catch(err => setFError(err));
    }

    let sourceId: string = id.id;

    if (pData) {
        if (props.idOri) {
            sourceId = id.id
        } else {
            sourceId = pData.data.product.sourceId
        }
    }

    const loadSourceData = async () => {
        fetch(urlBE + '/api/o/source/get/id/' + sourceId, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setSData(resData))
            .catch(err => setFError(err));

    } 

    const sources = pData && sData ? 
        <>
            <MainCard
                marginTop={'0'} marginBottom={'20px'}
            >
                <h2 className='headlineApprovalSource'>The provided data of the product: {pData.data.product.name}</h2>
                <h4 className="approveSourceItemHeadline">The description is: {pData.data.product.description}</h4>
                <p className="approveSourceItemHeadline">You can buy the product at: <a href={pData.data.product.linkToBuy} rel="noreferrer" target='_blank'>{pData.data.product.linkToBuy}</a></p>
                <p className="approveSourceItemHeadline">Costs per Unit: {pData.data.product.costPerProduct} EUR</p>
            </MainCard>
            <MainCard
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
                <SourceDetails
                    type={'product'}
                    name={pData.data.product.name}
                    titel={sData.data.source.title}
                    description={sData.data.source.description}
                    link={sData.data.source.link}
                    status={sData.data.source.status}
                ></SourceDetails>
            </MainCard>
            {
                fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >No more data in this category to approve</MessageCard>
                : <></>
            }
            { sData.data.source.status !== 'approvedFinal' && sData.data.source.status !== 'notApprovedAgain' ? 
                <MainCard
                        marginTop={"0"}
                        marginBottom={'20px'}
                >
                    <ApprovalForm
                        sourceId={sData.data.source.id}
                        productId={pData.data.product.id}
                        typeSource='product'
                        modelId={pData.data.product.id}
                    ></ApprovalForm> 
                </MainCard> : <MainCard marginTop={"0"}  marginBottom={'20px'} >
                    <h2 className='headlineApprovalSource'>Approval process of this source is completed!</h2>
                </MainCard>
            }
        </>
        : fError ? <p>Error</p>
        : <p>...Loading</p>

    return (
        <> { sources  && pData ? <div>{sources}
            {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>  : <></>         
        }
        </div>
        : <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default SourceApprovalProduct;
