import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import SkeletonTemplate from "../../../BuildingBlocks/templateSkeleton";
import MainCard from '../../../BuildingBlocks/templateMainBar';
import SourceHistory from '../UI/History/sourceHistory';
import MainBar from "../../../BuildingBlocks/templateMainBar";
import SourceDetails from "../UI/History/sourceDetails";
import MessageCard from "../../../BuildingBlocks/templateFeedback";
import { urlBE } from "../../../../App";

type ApprovalHistoryProps = {
    name: string,
    type: string,
};

const ApprovalHistory = (props: ApprovalHistoryProps) => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    let id: any = useParams();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(urlBE + '/api/o/source/get/id/' + id.id, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => {setFData(resData)
                setFError(undefined)
            })
            .catch(err => setFError(err));
    } 

    const firstApprovalData: object | null = fData && fData.data.approvedFirstBy.user && !fError ? fData.data.approvedFirstBy : null;
    const finalApprovalData: object | null = fData && fData.data.approvedFinalBy.user && !fError ? fData.data.approvedFinalBy : null;
    const newUserApproval: object | null = fData && fData.data.approvedByNewUser.user && !fError ? fData.data.approvedByNewUser : null;
    const firstDenial: object | null = fData && fData.data.notApprovedOnceBy.user && !fError ? fData.data.notApprovedOnceBy : null;
    const finalDenial: object | null = fData && fData.data.notApprovedAgainBy.user && !fError ? fData.data.notApprovedAgainBy : null;
    const newUserDenial: object | null = fData && fData.data.notApprovedByNewUser.user && !fError ? fData.data.notApprovedByNewUser : null;

    const sources = fData && !fError ? 
        <>
            <MainBar
                    marginTop={"0"}
                    marginBottom={'20px'}
            >
                <SourceDetails
                    key={props.name}
                    type={props.type}
                    name={props.name}
                    titel={fData.data.source.title}
                    description={fData.data.source.description}
                    link={fData.data.source.link}
                    status={fData.data.source.status}
                ></SourceDetails>
            </MainBar>
            <SourceHistory
                dataSource={props.type + ": "}
                dataName={props.name}
                createdAt={fData.data.source.createdAt}
                addedBy={fData.data.addedBy.addedBy}
                createdDescr={fData.data.source.description}
                firstApprovalData={firstApprovalData}
                finalApprovalData={finalApprovalData}
                newUserApprovalData={newUserApproval}
                firstDenialData={firstDenial}
                finalDenialData={finalDenial}
                newUserDenialData={newUserDenial}
            ></SourceHistory>
        </>
        : <p>...Loading</p>

    return (
        <> { sources && fData ? <div>{sources}
        {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>  : <></>         
        }
        </div>: 
            <div className="skeletonWidth">
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </div>
        } </>
    )
};

export default ApprovalHistory;
