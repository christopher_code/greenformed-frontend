type ProductCardProps = {
    productId: string,
    productName: string, 
    description: string,
    linkToBuy: string,
    costPerProduct: number,
    sourceId: string,
};

const ProductCard = (props: ProductCardProps) => {
    return (
            <div className="changeHistoryProCardContainer">
                <h1 className="changeHistoryProHeadline">The change history of the following product (current version): </h1>
                <p className="changeHistoryProCardP">Product Name: {props.productName}</p>
                <p className="changeHistoryProCardP">Description: {props.description}</p>
                <p className="changeHistoryProCardP">Costs Per Unit: {props.costPerProduct}</p>
                <div className="changeHistoryProCardDiv">
                    <p className="changeHistoryProCardP">Buy {props.productName} here: </p>
                    <a className="changeHistoryProCardA" href={props.linkToBuy} target="_blank" rel="noreferrer">Click here</a>
                </div>
                <div className="changeHistoryProCardDiv">
                    <p className="changeHistoryProCardP">More information about {props.productName}: </p>
                    <a className="changeHistoryProCardA" href={`/product/${props.productId}`}>Click here</a>
                </div>
                <div className="changeHistoryProCardDiv">
                    <p className="changeHistoryProCardP">Original Source: </p>
                    <a className="changeHistoryProCardA" href={`/source/${props.sourceId}/${props.productId}`}>Click here</a>
                </div>
            </div>
    )
};

export default ProductCard;
