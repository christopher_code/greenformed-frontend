import converteDate from "../../../../../helper/convertDate";
import goTo from "../../../../../helper/goTo";

type SourcePointProps = {
    productId: string,
    title: string,
    status: string,
    date: number,
    link: string,
    description: string,
    userName: string,
    userId: string,
    sourceId: string,
};

const AddedourcePoint = (props: SourcePointProps) => {
    let date: string[] = converteDate(props.date)
    return (
        <div className="changeHistoryProCardContainer">
            <h2 className="changeHistoryProHeadline">The product was created with the following data: </h2>
            <p className="changeHistoryProCardP">Source Title: {props.title}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Source Link: </p>
                <a className="changeHistoryProCardA" href={props.link} target="_blank" rel="noreferrer">Click here</a>
            </div>
            <p className="changeHistoryProCardP">Source description: {props.description}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">See the source approval history: </p>
                <p className="changeHistoryProCardA" onClick={() => goTo(`/source/${props.sourceId}/${props.productId}`)}>Click here</p>
            </div>
            <p className="changeHistoryProCardP">Date of source creation: {date[0]} {date[1]}</p>
            <p className="changeHistoryProCardP">Approval status: {props.status}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Added by: {props.userName}</p>
                <p className="changeHistoryProCardA" onClick={() => goTo(`o/user/profile/${props.userId}`)}>Visit User Profile</p>
            </div>
        </div>  
    )
};

export default AddedourcePoint;
