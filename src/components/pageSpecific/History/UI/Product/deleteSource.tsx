import converteDate from "../../../../../helper/convertDate";

type SourcePointProps = {
    title: string, 
    status: string, 
    date: number, 
    link: string, 
    description: string, 
    sourceId: string,
    productId: string,
    userId: string,
    originalDescription: string,
    OriginalLinkToBuy: string,
    OriginalName: string, 
};

const DeleteSource = (props: SourcePointProps) => {
    let date: string[] = converteDate(props.date)
    return (
        <div className="changeHistoryProCardContainer">
            <h2 className="changeHistoryProHeadline">This is a request to delete the product: {props.OriginalName}</h2>
            <p className="changeHistoryProCardP">Original Name: {props.OriginalName}</p>
            <p className="changeHistoryProCardP">Original Description: {props.originalDescription}</p>
            <a className="changeHistoryProCardA" href={props.OriginalLinkToBuy} target="_blank" rel="noreferrer" >Original link to buy</a>
            <p className="changeHistoryProCardP">This source was added to prrof that the product needs to be deleted: </p>
            <p className="changeHistoryProCardP">Updated Source Title: {props.title}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Source Link: </p>
                <a className="changeHistoryProCardA" href={props.link} target="_blank" rel="noreferrer">Click here</a>
            </div>
            <p className="changeHistoryProCardP">Source description: {props.description}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">See the source approval history: </p>
                <a className="changeHistoryProCardA" href={`/source/${props.sourceId}/${props.productId}`}>Click here</a>
            </div>
            <p className="changeHistoryProCardP">Date of source creation: {date[0]} {date[1]}</p>
            <p className="changeHistoryProCardP">Approval status: {props.status}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Go to user profile: </p>
                <a className="changeHistoryProCardA" href={`o/user/profile/${props.userId}`}>Visit User Profile</a>
            </div>
            <div className="deviderDivAddFields"></div>
        </div>
    )
};

export default DeleteSource;
