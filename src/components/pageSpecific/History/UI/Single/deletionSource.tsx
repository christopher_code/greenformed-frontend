import converteDate from "../../../../../helper/convertDate";

type DeletionSourceProps = {
    title: string, 
    status: string, 
    date: number, 
    link: string, 
    description: string, 
    userId: string,
    oriGHGName: string,
    oriEmission: number,
};

const DeleteSource = (props: DeletionSourceProps) => {
    let date: string[] = converteDate(props.date)
    return (
        <div className="changeHistoryProCardContainer">
            <h2 className="changeHistoryProHeadline">This is a request to delete the single {props.oriGHGName} emission: </h2>
            <p className="changeHistoryProCardP">Original Single GHG Name: {props.oriGHGName}</p> 
            <p className="changeHistoryProCardP">Original Emission per 1KG sold: {props.oriEmission} KG of {props.oriGHGName}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Source Link: </p>
                <a className="changeHistoryProCardA" href={props.link} target="_blank" rel="noreferrer">Click here</a>
            </div>
            <p className="changeHistoryProCardP">Source description: {props.description}</p>
            <p className="changeHistoryProCardP">Date of source creation: {date[0]} {date[1]}</p>
            <p className="changeHistoryProCardP">Approval status: {props.status}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Go to user profile: </p>
                <a className="changeHistoryProCardA" href={`o/user/profile/${props.userId}`}>Visit User Profile</a>
            </div>
            <div className="deviderDivAddFields"></div>
        </div>
    )
};

export default DeleteSource;
