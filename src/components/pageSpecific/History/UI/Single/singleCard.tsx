type SingleCardProps = {
    ghgName: string, 
    emissionPerKG: number,
};

const SingleCard = (props: SingleCardProps) => {
    return (
            <div className="changeHistoryProCardContainer">
                <h1 className="changeHistoryProHeadline">The change history of the following single GHG Emission (current version): </h1>
                <p className="changeHistoryProCardP">GHG Name: {props.ghgName}</p>
                <p className="changeHistoryProCardP">Per 1 KG produced, {props.emissionPerKG} KG of {props.ghgName} are emitted.</p>
            </div>
    )
};

export default SingleCard;
