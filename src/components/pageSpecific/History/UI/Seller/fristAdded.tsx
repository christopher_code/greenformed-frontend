import converteDate from "../../../../../helper/convertDate";

type SourcePointProps = {
    title: string,
    status: string,
    date: number,
    link: string,
    description: string,
    userName: string,
    userId: string,
    userType: string,
};

const FirstAdded = (props: SourcePointProps) => {
    let date: string[] = converteDate(props.date)
    return (
        <div className="changeHistoryProCardContainer">
            <h2 className="changeHistoryProHeadline">The data was created with the following data: </h2>
            <p className="changeHistoryProCardP">Source Title: {props.title}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Source Link: </p>
                <a className="changeHistoryProCardA" href={props.link} target="_blank" rel="noreferrer">Click here</a>
            </div>
            <p className="changeHistoryProCardP">Source description: {props.description}</p>
            <p className="changeHistoryProCardP">Date of source creation: {date[0]} {date[1]}</p>
            <p className="changeHistoryProCardP">Approval status: {props.status}</p>
            <p className="changeHistoryProCardP">Added by: {props.userName}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Added by: {props.userName}</p>
                <a className="changeHistoryProCardA" href={`o/user/profile/${props.userId}`}>Visit User Profile</a>
            </div>
        </div>
    )
};

export default FirstAdded;
