type SellerCardProps = {
    name: string, 
    link: string,
};

const SellerCard = (props: SellerCardProps) => {
    return (
            <div className="changeHistoryProCardContainer">
                <h1 className="changeHistoryProHeadline">The change history of the following seller (current version): </h1>
                <div className="changeHistoryProCardDiv">
                    <p className="changeHistoryProCardP">Seller Name: {props.name}</p>
                    <a className="changeHistoryProCardA" href={props.link} target="_blank" rel="noreferrer">More Information</a>
                </div>
            </div>
    )
};

export default SellerCard;
