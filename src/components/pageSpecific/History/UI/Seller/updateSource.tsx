import converteDate from "../../../../../helper/convertDate";

type SourcePointProps = {
    typeChange: string,
    title: string, 
    status: string, 
    date: number, 
    link: string, 
    description: string, 
    originalValue: any,
    newValue: any,
    field: string,
    userId: string,
};

const UpdatedSource = (props: SourcePointProps) => {
    let date: string[] = converteDate(props.date)
    return (
        <div className="changeHistoryProCardContainer">
            <h2 className="changeHistoryProHeadline">The {props.typeChange} was updated at the following field: {props.field}</h2>
            <p className="changeHistoryProCardP">Original Value: {props.originalValue}</p>
            <p className="changeHistoryProCardP">New Value: {props.newValue}</p>
            <p className="changeHistoryProCardP">Updated Source Title: {props.title}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Source Link: </p>
                <a className="changeHistoryProCardA" href={props.link} target="_blank" rel="noreferrer">Click here</a>
            </div>
            <p className="changeHistoryProCardP">Source description: {props.description}</p>
            <p className="changeHistoryProCardP">Date of source creation: {date[0]} {date[1]}</p>
            <p className="changeHistoryProCardP">Approval status: {props.status}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Go to user profile: </p>
                <a className="changeHistoryProCardA" href={`o/user/profile/${props.userId}`}>Visit User Profile</a>
            </div> 
            <div className="deviderDivAddFields"></div>
        </div>
    )
};

export default UpdatedSource;
