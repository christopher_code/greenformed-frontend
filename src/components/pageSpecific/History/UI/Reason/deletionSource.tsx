import converteDate from "../../../../../helper/convertDate";

type SourcePointProps = {
    title: string, 
    status: string, 
    date: number, 
    link: string, 
    description: string, 
    userId: string,
    oriReasonTitle: string,
    oriDescription: string,
    oriGhgName: string,
};

const DeleteSource = (props: SourcePointProps) => {
    let date: string[] = converteDate(props.date)
    return (
        <div className="changeHistoryProCardContainer">
            <h2 className="changeHistoryProHeadline">This is a request to delete the reason of GHG Emission: </h2>
            <p className="changeHistoryProCardP">Original Reason of GHG Emission: {props.oriReasonTitle}</p>
            <p className="changeHistoryProCardP">Original GHG emitted druing {props.oriReasonTitle}: {props.oriGhgName}</p>
            <p className="changeHistoryProCardP">Original Description of GHG Emission: {props.oriDescription}</p>
            <p className="changeHistoryProCardP">This source was added to prrof that the product needs to be deleted: </p>
            <p className="changeHistoryProCardP">Updated Source Title: {props.title}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Source Link: </p>
                <a className="changeHistoryProCardA" href={props.link} target="_blank" rel="noreferrer">Click here</a>
            </div>
            <p className="changeHistoryProCardP">Source description: {props.description}</p>
            <p className="changeHistoryProCardP">Date of source creation: {date[0]} {date[1]}</p>
            <p className="changeHistoryProCardP">Approval status: {props.status}</p>
            <div className="changeHistoryProCardDiv">
                <p className="changeHistoryProCardP">Go to user profile: </p>
                <a className="changeHistoryProCardA" href={`o/user/profile/${props.userId}`}>Visit User Profile</a>
            </div>
            <div className="deviderDivAddFields"></div>
        </div>
    )
};

export default DeleteSource;
