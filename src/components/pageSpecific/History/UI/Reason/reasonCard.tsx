type ProductCardProps = {
    ghgName: string, 
    description: string,
    reasonTitle: string,
};

const ProductCard = (props: ProductCardProps) => {
    return (
        <div className="changeHistoryProCardContainer">
            <h1 className="changeHistoryProHeadline">The change history of the reason of GHG Emission (current version): </h1>
            <p className="changeHistoryProCardP">Reason of GHG Emission: {props.reasonTitle}</p>
            <p className="changeHistoryProCardP">GHG Emitted during {props.reasonTitle}: {props.ghgName}</p>
            <p className="changeHistoryProCardP">Description: {props.description}</p>
        </div>
    )
};

export default ProductCard;
