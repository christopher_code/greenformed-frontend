import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import { urlBE } from "../../../../App";
import SkeletonTemplate from "../../../BuildingBlocks/templateSkeleton";
import MainCard from "../../../BuildingBlocks/templateMainBar";
import ReasonCard from "../UI/Reason/reasonCard";
import FirstSource from "../UI/Reason/addedFirstCard";
import UpdatedSource from "../UI/Seller/updateSource";
import DeletionSource from "../UI/Reason/deletionSource";
import MessageCard from "../../../BuildingBlocks/templateFeedback";

const ProductCardsFetched = () => {
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

      let id: any = useParams();

    const loadData = async () => {
        fetch(`${urlBE}/api/o/reason/changehis/get/` + id.id, { 
            method: 'get', 
            headers: {
                "X-Content-Type-Options": "nosniff"
            },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));
    }   

    const updatedSources = fData && fData.data.updateSource[0] !== undefined && fData.data.updateSource[0].length !== 0 && fData.data.updateSource.reverse().map((item: any) => 
        <UpdatedSource
            key={item.source.id}
            typeChange={"reason of GHG emission"}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            originalValue={item.properties.OriginalValue}
            newValue={item.properties.NewValue}
            field={item.properties.ChangedField}
            userId={item.properties.UserId}
        />
    )

    const deleteionSource = fData && fData.data.requestDeleteSource[0] !== undefined && fData.data.requestDeleteSource[0].length !== 0  && fData.data.requestDeleteSource.reverse().map((item: any) => 
        <DeletionSource
            key={item.source.id}
            title={item.source.title}
            status={item.source.status}
            date={item.source.createdAt}
            link={item.source.link}
            description={item.source.description}
            userId={item.properties.UserId}
            oriReasonTitle={item.properties.reasonTitle}
            oriDescription={item.properties.description}
            oriGhgName={item.properties.ghgName}
        />
    )

    return (
        <> { fData && !fError ? 
                <>
                    <MainCard marginTop={'0'} marginBottom={'0'}>
                        <ReasonCard
                            reasonTitle={fData.data.reasonOfEmission.reasonTitle}
                            description={fData.data.reasonOfEmission.description}
                            ghgName={fData.data.reasonOfEmission.ghgName}
                        />
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <FirstSource
                            title={fData.data.source[0].title}
                            status={fData.data.source[0].status}
                            date={fData.data.source[0].createdAt}
                            link={fData.data.source[0].link}
                            description={fData.data.source[0].description}
                            addedBy={fData.data.addedBy[0].description}
                            userName={fData.data.addedBy[0].name}
                            userId={fData.data.addedBy[0].id}
                            userType={fData.data.addedBy[0].description}
                        ></FirstSource>
                    </MainCard>
                    { fData && fData.data.updateSource[0] !== undefined ?
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        {updatedSources}
                    </MainCard> :<></>
                    }
                    { fData && fData.data.requestDeleteSource[0] !== undefined ?
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        {deleteionSource}
                    </MainCard> : <></>
                    }
                </>
            : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
            : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
            : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
            : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
            :
            <>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
                <MainCard marginTop={'20px'} marginBottom={'0'}>
                    <SkeletonTemplate numberOfLines={6} /> 
                </MainCard>
            </>
            } 
        </>
    )
};

export default ProductCardsFetched;
