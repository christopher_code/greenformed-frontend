import {useState} from 'react';
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import goTo from "../../../../../helper/goTo";
import { urlBE } from "../../../../../App";
import Button from "../../../../BuildingBlocks/templateButton";

const SignUpForm = () => {
    const [fError, setFError] = useState<any>();
    const [name, setName] = useState('');
    const [company, setCompany] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showModalData, setShowModalData] = useState<boolean>(false);

    const postSignUp = (e: any) => {
        e.preventDefault();
    
        fetch(urlBE + "/api/signup/user", {
            method: "post",
            headers: {
              "Content-Type": "application/json",
              "X-Content-Type-Options": "nosniff"
            },
            body: JSON.stringify({
              email: email,
              password: password,
              name: name, 
              company: company,
            })
            }).then(res => {
                if (res.ok) {
                    setPassword("");
                    setCompany("");
                    setName("");
                    setEmail("");
                    goTo('/user/login')
                    return res.json();
                }
                throw res;
            }).catch(err => setFError(err));
        
      }

    return (
        <>
        <div className="containerSignUp">
         <div className="modalDivRow" onClick={() => setShowModalData(true)}>
            <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
            <p className="modalButtonPre">Get more information</p>
        </div>
        { showModalData ?
        <Modal 
            info={'Add your name, your company name, a valid email and a secure password (at least 8 characters).'}
            eventClose={() => setShowModalData(false)}
            show={showModalData}
        /> : <></> }
        <form onSubmit={postSignUp}>
            <div className="signupOuterDiv">
                <div className="signupDIvForm">
                    <label>Name *</label> 
                    <input className="form-control me-2"
                        id="inputSignUpName" 
                        type="text" 
                        placeholder="Maxime Musterfrau"
                        aria-label="inputName"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        minLength={2}
                        required
                        ></input>
                </div>
                <div className="signupDIvForm">
                    <label>Company</label> 
                    <input className="form-control me-2"
                        id="inputSignUpCompnay" 
                        type="text" 
                        placeholder="Company GmbH"
                        aria-label="inputCompany"
                        value={company}
                        onChange={(e) => setCompany(e.target.value)}
                        minLength={2}
                        ></input>
                </div>
                <div className="signupDIvForm">
                    <label>Email Address *</label> 
                    <input className="form-control me-2"
                        id="inputSignUpEmail" 
                        type="email" 
                        placeholder="max@musterfrau.de"
                        aria-label="inputEmail"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        minLength={6}
                        required
                        name={'email'}
                        ></input>
                </div>
                <div className="signupDIvForm">
                    <label>Password *</label> 
                    <input className="form-control me-2"
                        id="inputSignUpPassword1" 
                        type="password" 
                        placeholder="Password"
                        aria-label="inputPassword"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        minLength={8}
                        required
                        ></input>
                </div>
                </div>
                <div className="buttonDivSignUp">
                    <input type="submit" value="Sign Up" className="formButtonGeneral"/>
                </div>
                <Button
                    event={() => goTo('/user/login')}
                    bg={"#41475E"}
                    border={'transparaent'}
                    width={'300px'}
                    color={'white'}
                    margin={'10px 0 0 0'}
                >Go to Log In</Button> 
            </form>
        </div>
        {fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >User with this email already exists</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
        : <></>
        }
        </>
    )
};

export default SignUpForm;