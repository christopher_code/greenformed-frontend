import React, {useState} from 'react'
import { useHistory } from 'react-router';
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import { AuthContext } from "../../../../../App";
import { urlBE } from "../../../../../App";
import Button from "../../../../BuildingBlocks/templateButton";
import goTo from '../../../../../helper/goTo';

const LoginForm = () => {
    const routerHistory = useHistory();
    const { dispatch } = React.useContext(AuthContext);
    const [fError, setFError] = useState<any>();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showModalData, setShowModalData] = useState<boolean>(false);

    const postLogin = (e: any) => {
        e.preventDefault();
    
        fetch(urlBE + "/api/login/user", {
            method: "post",
            headers: {
              "Content-Type": "application/json",
              "X-Content-Type-Options": "nosniff"
            },
            body: JSON.stringify({
              email: email,
              password: password
            })
            }).then(res => {
                if (res.ok) {
                    setEmail("");
                    setPassword("");
                    return res.json();
                }
                throw res;
            })
            .then(resJson => {
                dispatch({
                    type: "LOGIN",
                    payload: resJson
                })
                console.log("Loged in: " + resJson.data.token)
                routerHistory.goBack();
            })
            .catch(err => setFError(err));
      }

    return (
        <>
        <div className="containerSignUp">
        <div className="modalDivRow" onClick={() => setShowModalData(true)}>
            <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
            <p className="modalButtonPre">Get more information</p>
        </div>
        { showModalData ?
        <Modal 
            info={'When you have verified your mail, please type in your email and password. In case you don not have verfied the email, please do it before signing in. '}
            eventClose={() => setShowModalData(false)}
            show={showModalData}
        /> : <></> }
            <form onSubmit={postLogin}>
                    <div className="signupDIvForm">
                        <label>Email Address *</label> 
                        <input className="form-control me-2"
                            id="loginEmail" 
                            type="email" 
                            placeholder="max@muysterfrau.de"
                            aria-label="inputEmail"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            minLength={6}
                            required
                            ></input>
                    </div>
                    <div className="signupDIvForm">
                        <label>Password *</label> 
                        <input className="form-control me-2"
                            id="loginPassword" 
                            type="password" 
                            placeholder="Password"
                            aria-label="inputPassword"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            minLength={8}
                            required
                            ></input>
                </div>
                <div className="buttonDivSignUp">
                    <input type="submit" value="Submit" className="formButtonGeneral" />
                </div>
                <Button
                    event={() => goTo('/user/signup')}
                    bg={"#41475E"}
                    border={'transparaent'}
                    width={'250px'}
                    color={'white'}
                    margin={'10px 0 0 0'}
                >Create account</Button> 
            </form>
        </div>
        { fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
        : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
        : fError && fError.status === 406 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >406 - Please check your input</MessageCard>
        : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
        : fError && fError.status === 401 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >401 - Please provide the correct email and password</MessageCard>
        : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
        : <></> } 
        </>
    )
};

export default LoginForm;