import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import { AuthContext } from "../../../../../App";
import SkeletonTemplate from "../../../../BuildingBlocks/templateSkeleton";
import MainCard from "../../../../BuildingBlocks/templateMainBar";
import MessageCard from "../../../../BuildingBlocks/templateFeedback";
import Modal from "../../../../BuildingBlocks/templateModal";
import Info from "../../../../../assets/information.png";
import convertDate from "../../../../../helper/convertDate";
import { urlBE } from "../../../../../App";

const ProfileData = () => {
    const { state: authState } = React.useContext(AuthContext);
    const [fData, setFData] = useState<any>();
    const [fError, setFError] = useState<any>();
    const [showModalData, setShowModalData] = useState<boolean>(false);

    let id: any = useParams();
    let userId: string = '';
     
    if (authState.id) {
        userId = authState.id;
    } else {
        userId = id.id;
    }

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0)
        // eslint-disable-next-line
      }, []);

    const loadData = async () => {
        fetch(`${urlBE}/api/user/data/${userId}`, { 
            method: 'get', 
            headers: {
                "Content-Type": "application/json",
                "X-Content-Type-Options": "nosniff"
              },
          }).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then(resData => setFData(resData))
            .catch(err => setFError(err));;
    }   

    return (
        <>
            {
                fData ?
                <div className="profileDataContainer">
                    <div className="profileCoreDiv">
                        <p>Name: {fData.data.name}</p>
                        <p>Email: {fData.data.email}</p>
                        <p>Prodile Type: {fData.data.role}</p>
                        <p>Profile exists since: {convertDate(fData.data.createdAt)[0]}</p>
                        <p>As a user (not superuser) you have correctly approved {fData.data.approvals} sources.</p>
                        <p>Company: {fData.data.company}</p>
                        <div className="modalDivRow" onClick={() => setShowModalData(true)}>
                            <img src={Info} className='infoButtonModal' alt="information for setting data in greenformed"  />
                            <p className="modalButtonPre">Get more information</p>
                        </div>
                        { showModalData ?
                        <Modal 
                            info={'This is your user profile. If you are a user and not a super user, you need to wait 7 days and correctly approve five source. In case you approve like two other super users, you get 1 point closer to your 10 approval requirements.'}
                            eventClose={() => setShowModalData(false)}
                            show={showModalData}
                        /> : <></> }
                    </div>
                </div> 
                : fError && fError.status === 404 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >404 - URL does not exist</MessageCard>
                : fError && fError.status === 400 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >400 - Please try again and check your input</MessageCard>
                : fError && fError.status === 500 ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >500 - Internal Server error - please try again later</MessageCard>
                : fError ? <MessageCard bg={'#F3A8A8'} color={'#810000'} border={'2px solid #810000'} >Something went wrong - please also check your connection</MessageCard>           
                : 
                <>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                    <MainCard marginTop={'20px'} marginBottom={'0'}>
                        <SkeletonTemplate numberOfLines={6} /> 
                    </MainCard>
                </>
            }
        </>
    )
};

export default ProfileData;
