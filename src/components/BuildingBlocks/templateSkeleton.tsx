import Skeleton from 'react-loading-skeleton';

type SkeletonProps = {
    numberOfLines: number,
};

const templateSkeleton = (props: SkeletonProps) => {
    return (
        <div className="skeletonWidth">
            <Skeleton height={50} />
            <div className="skeletonDevider"></div>
            <Skeleton count={props.numberOfLines} height={25}/>
        </div>
    )
};

export default templateSkeleton;