import { useState } from 'react';
import Search from "./tempalteSearchBar";
import Button from "./templateButton";
import goTo from '../../helper/goTo';

const TemplateSideBar = () => {
    const [search, setSearch] = useState<string>('');
    const [searchApproved, setSearchApproved] = useState<string>('');

    return (
        <div className="sideBarTemplate">
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">Product</h3>
                <p className="sideBarSubText">Search for any product on the Greenformed platform.</p>
                <Search 
                    event={() => goTo('/search/' + search + "/notapproved")} 
                    bg={"white"}
                    border={"#41475E"}
                    icon={"red"}
                    placeholder={"Search"}
                    fontColor={"#41475E"}
                    borderColor={"#41475E"}
                    width="130px"
                    changeEvent={(e: any) => setSearch(e.target.value)}
                >
                Search
                </Search>
                <div className="sideBarButtonProductDiv">
                    <h3 className="sideBarHeadline">Add a Product</h3>
                    <p className="sideBarSubText">Everyone can add data to Greenformed.</p>
                    <Button 
                        event={() => goTo('/new/product')}
                        bg={"#41475E"}
                        border={'#41475E'}
                        width={"215px"}
                        color={"white"}
                        margin={"10px 0 0 0"}
                    >
                        Add Product
                    </Button>
                </div>
            </div>
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">Approved Data</h3>
                <p className="sideBarSubText">Search for approved products on the Greenformed platform.</p>
                <Search 
                    event={() => goTo('/search/' + searchApproved + "/approved")} 
                    bg={"white"}
                    border={"#41475E"}
                    icon={"red"}
                    placeholder={"Search"}
                    fontColor={"#41475E"}
                    borderColor={"#41475E"}
                    width="130px"
                    changeEvent={(e: any) => setSearchApproved(e.target.value)}
                >
                Search
                </Search>
            </div>
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">Participate</h3>
                <p className="sideBarSubText">Add and review new products, add and update information or request to delete information.</p>
                <Button 
                    event={() => goTo('/approval/init/data')}
                    bg={"#41475E"}
                    border={'#41475E'}
                    width={"215px"}
                    color={"white"}
                    margin={"10px 0 0 0"}
                >
                    Verify Data
                </Button>
                <Button 
                        event={() => goTo('/new/product')}
                        bg={"#41475E"}
                        border={'#41475E'}
                        width={"215px"}
                        color={"white"}
                        margin={"10px 0 0 0"}
                    >
                        Add Data
                    </Button>
            </div>
            <div className="sideBarDivPart">
                <h3 className="sideBarHeadline">API Documentation</h3>
                <p className="sideBarSubText">Make use of the Greenformed database and functionality as a developer. See our API documentation.</p>
                <Button 
                    event={() => goTo('https://documenter.getpostman.com/view/8069940/TzzBpFQp')}
                    bg={"#41475E"}
                    border={'#41475E'}
                    width={"215px"}
                    color={"white"}
                    margin={"10px 0 0 0"}
                >
                    Documentation
                </Button>
            </div>
        </div>
    )
};

export default TemplateSideBar;