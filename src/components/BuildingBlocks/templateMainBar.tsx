type MainBarProps = {
    children: any;
    marginTop: string;
    marginBottom: string;
};

const templateMainBar = (props: MainBarProps) => {
    return (
        <div 
            className="MainBarContainer"
            style={{
                marginTop: props.marginTop,
                marginBottom: props.marginBottom
            }}
        >
            {props.children}
        </div>
    )
};

export default templateMainBar;
