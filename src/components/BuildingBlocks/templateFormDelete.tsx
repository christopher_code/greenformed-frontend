type RequestDeleteFormProps = {
    eventSourceLink: any,
    eventSourceDescription: any,
    sourceLink: string,
    sourceDescr: string,
    name: string,
};

const templateFormDelete = (props: RequestDeleteFormProps) => {
    return (
        <div>
            <p>If you want to delete {props.name} please fill fill the following form: </p>
            <label className="labelNewProduct">Add a source description why you request to delete the data</label> 
            <input className="form-control me-2"
                id="inputSourceDeletionDescriptions" 
                placeholder="Source Description"
                type="text" 
                aria-label="inputSourceDeletionDescriptions"
                value={props.sourceDescr}
                onChange={(e) => props.eventSourceDescription(e.target.value)}
                minLength={20}
                required
                ></input>
            <label className="labelNewProduct">Add a link that prroves your source</label> 
            <input className="form-control me-2"
                id="inputSourceDeletionLink" 
                type="url" 
                aria-label="inputSourceDeletionLink"
                placeholder="Source Link"
                value={props.sourceLink}
                onChange={(e) => props.eventSourceLink(e.target.value)}
                minLength={4}
                required
                ></input>
        </div>
    )
};

export default templateFormDelete;