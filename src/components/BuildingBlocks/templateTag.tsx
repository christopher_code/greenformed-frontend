type TagProps = {
    bg: string;
    children: string;
    width: string;
    height: string;
    color: string;
    fontSize: string;
};

const templateTag = (props: TagProps) => {
    return (
        <div className='divTag'
            style={{background: props.bg, 
                    width: props.width, 
                    height: props.height, 
                    color: props.color,
                    fontSize: props.fontSize,
                }} title="Tag Component">
                {props.children}
        </div>
    )
};

export default templateTag;