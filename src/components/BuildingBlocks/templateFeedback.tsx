type MessageProps = {
    bg: string;
    children: string;
    color: string;
    border: string;
};

const templateFeedback = (props: MessageProps) => {
    return (
        <div className="messageComponentContainer"
        style={{
            background: props.bg,
            border: props.border,
        }}>
           <h4 className="messageComponentText"
           style={{
                color: props.color,
            }}>{props.children}</h4> 
        </div>
    )
};

export default templateFeedback;
