type UpdateFormProps = {
    eventUpdateField: any;
    label: string,
    typeUpdate: string, 
    originalValue: any,
    value: any,
    eventSourceLink: any,
    eventSourceDescription: any,
    sourceLink: string,
    sourceDescr: string,
};

const templateFormUpdateNumber = (props: UpdateFormProps) => {
    return (
        <>
            <p>The original values is: {props.originalValue}</p>
            <label className="labelUpdateProduct">{props.label}</label> 
            <input className="form-control me-2"
                id="inputUpdateNumberFieldToUpdate" 
                type={props.typeUpdate} 
                aria-label="inputUpdateNumberFieldToUpdate"
                value={props.value}
                onChange={(e) => props.eventUpdateField(+e.target.value)}
                required
                ></input>
            <label className="labelNewProduct">Add a description why you want to change the data</label> 
            <input className="form-control me-2"
                id="inputUpdateNumberDescrSource" 
                type="text" 
                aria-label="inputUpdateNumberDescrSource"
                value={props.sourceDescr}
                onChange={(e) => props.eventSourceDescription(e.target.value)}
                minLength={20}
                required
                ></input>
            <label className="labelNewProduct">Add a source link that backs your description</label> 
            <input className="form-control me-2"
                id="inputUpdateNumberSourceLink" 
                type="url" 
                aria-label="inputUpdateNumberSourceLink"
                value={props.sourceLink}
                onChange={(e) => props.eventSourceLink(e.target.value)}
                minLength={4}
                required
                ></input>
        </>
    )
};

export default templateFormUpdateNumber;
