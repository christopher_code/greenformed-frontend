type SelectOptionsProps = {
    event: any,
    text: string,
};

const templateSelectOptions = (props: SelectOptionsProps) => {
    return (
        <div className='selectOptiosnDivContainter' onClick={() => props.event()}>
            <p className='selectOptiosText'>{props.text}</p>
        </div>
    )
};

export default templateSelectOptions;