import { Button } from 'react-bootstrap';

type ButtonProps = {
    event: any;
    bg: string;
    children: string;
    border: string;
    width: string;
    color: string;
    margin: string;
};

const templateButton = (props: ButtonProps) => {
    return (
        <Button 
            className='buttonTemplate'
            onClick={props.event} 
            style={{background: props.bg, 
                borderColor: props.border,
                width: props.width,
                color: props.color,
                margin: props.margin
            }} 
            title="Button Component"
            >
            {props.children}
        </Button>
    )
};

export default templateButton;
