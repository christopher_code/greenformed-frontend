import { Button, Modal } from 'react-bootstrap';

type ModalProps = {
    info: string,
    eventClose: any,
    show: boolean
};

const templateModal = (props: ModalProps) => {

    return (
        <Modal show={props.show} onHide={props.eventClose}>
            <Modal.Header>
            <Modal.Title>Information required?</Modal.Title>
            </Modal.Header>
            <Modal.Body>{props.info}</Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={props.eventClose}>
                close
            </Button>
            </Modal.Footer>
        </Modal>
    )
};

export default templateModal;
