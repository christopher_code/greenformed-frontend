type UpdateFormProps = {
    eventUpdateField: any;
    label: string,
    typeUpdate: string, 
    originalValue: any,
    value: any,
    eventSourceLink: any,
    eventSourceDescription: any,
    sourceLink: string,
    sourceDescr: string,
};

const templateUpdateFormNumber = (props: UpdateFormProps) => {
    return (
        <>
            <p className="updateFormOriginalValue">The original values is: {props.originalValue}</p>
            <label className="labelUpdateProduct">{props.label}</label> 
            <input className="form-control me-2"
                id="inputUpdateFormToUpdate" 
                type={props.typeUpdate} 
                aria-label="inputUpdateFormToUpdate"
                value={props.value}
                onChange={(e) => props.eventUpdateField(e.target.value)}
                required
                ></input>
            <label className="labelNewProduct">Add a description why you want to update the data</label> 
            <input className="form-control me-2"
                id="inputUpdateFormSourceDesc" 
                type="text" 
                aria-label="inputUpdateFormSourceDesc"
                value={props.sourceDescr}
                onChange={(e) => props.eventSourceDescription(e.target.value)}
                minLength={20}
                required
                ></input>
            <label className="labelNewProduct">Add a sourcelink that backs your description</label> 
            <input className="form-control me-2"
                id="inputUpdateFormSourceLink" 
                type="url" 
                aria-label="inputUpdateFormSourceLink"
                value={props.sourceLink}
                onChange={(e) => props.eventSourceLink(e.target.value)}
                minLength={4}
                required
                ></input>
        </>
    )
};

export default templateUpdateFormNumber;