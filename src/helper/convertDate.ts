export default function convertDate(input: number): string[] {
    let date: any = new Date(input);
    let dateDate = date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getFullYear();
    let dateTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return [dateDate, dateTime]
}