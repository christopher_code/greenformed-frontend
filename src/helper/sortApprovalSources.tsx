import { SourceMainInfoProps } from "../components/pageSpecific/Source/UI/History/sourceHistory"
import SourceDots from "../components/pageSpecific/Source/UI/History/sourceDot"

const SortApprovalSources = (props: SourceMainInfoProps): any => {
    let firstApproval: any; 
    let finalApproval: any;
    let newUserApproval: any;
    let firstDenial: any;
    let finalDenial: any;
    let newUserDenial: any;

    props.firstApprovalData ? firstApproval = <SourceDots
        key={props.firstApprovalData.approvalProperties.date}
        date={props.firstApprovalData.approvalProperties.date}
        approvalState='Frist approved by '
        user={props.firstApprovalData.user}
        description={props.firstApprovalData.approvalProperties.Description}
    ></SourceDots> : firstApproval = undefined 
    props.finalApprovalData ? finalApproval = <SourceDots
        key={props.finalApprovalData.approvalProperties.date}
        date={props.finalApprovalData.approvalProperties.date}
        approvalState='Final approval by '
        user={props.finalApprovalData.user}
        description={props.finalApprovalData.approvalProperties.Description}
    ></SourceDots> : finalApproval = undefined
    props.newUserApprovalData ? newUserApproval = <SourceDots
        key={props.newUserApprovalData.approvalProperties.date}
        date={props.newUserApprovalData.approvalProperties.date}
        approvalState='Approval by new user '
        user={props.newUserApprovalData.user}
        description={props.newUserApprovalData.approvalProperties.Description}
    ></SourceDots> : newUserApproval = undefined
    props.firstDenialData ? firstDenial = <SourceDots
        key={props.firstDenialData.approvalProperties.date}
        date={props.firstDenialData.approvalProperties.date}
        approvalState='First denial by '
        user={props.firstDenialData.user}
        description={props.firstDenialData.approvalProperties.Description}
    ></SourceDots> : firstDenial = undefined
    props.finalDenialData ? finalDenial = <SourceDots
        key={props.finalDenialData.approvalProperties.date}
        date={props.finalDenialData.approvalProperties.date}
        approvalState='Final denial by '
        user={props.finalDenialData.user}
        description={props.finalDenialData.approvalProperties.Description}
    ></SourceDots> : finalDenial = undefined
    props.newUserDenialData ? newUserDenial = <SourceDots
        key={props.newUserDenialData.approvalProperties.date}
        date={props.newUserDenialData.approvalProperties.date}
        approvalState='Denial by new user '
        user={props.newUserDenialData.user}
        description={props.newUserDenialData.approvalProperties.Description}
    ></SourceDots> : newUserDenial = undefined

    let rawArray: any[]= [];
    if (firstApproval !== undefined) { 
        rawArray.push([firstApproval, props.firstApprovalData.approvalProperties.date])}
    if (finalApproval !== undefined) { 
        rawArray.push([finalApproval, props.finalApprovalData.approvalProperties.date])}
    if (newUserApproval !== undefined) { 
        rawArray.push([newUserApproval, props.newUserApprovalData.approvalProperties.date])}
    if (firstDenial !== undefined) {
        rawArray.push([firstDenial, props.firstDenialData.approvalProperties.date])}
    if (finalDenial !== undefined) {
        rawArray.push([finalDenial, props.finalDenialData.approvalProperties.date])}
    if (newUserDenial !== undefined) {
        rawArray.push([newUserDenial, props.newUserDenialData.approvalProperties.date])}

    let sortedArrayBoth = rawArray.sort(function(a, b) {
        return a[1] - b[1];
      });

    let sortedArray: any[] = [];

    for (let i: number = 0; i < sortedArrayBoth.length; i++) {
        sortedArray.push(sortedArrayBoth[i][0])
    }

    return sortedArray;
}

export default SortApprovalSources;